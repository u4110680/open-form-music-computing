## MSA Conference 2022: Alexander Hunter and Charles Patrick Martin

Open-Form Music Composition for Synchronised and Coordinated Action

## Abstract

This research project utilises electronic touch-screen instruments in a multi-channel performance space to explore a formal language for describing open-ended musical composition. Our system encodes concepts of coordination, opposition, synchronisation, communication, bandwidth and delay. Our software system allows representation of formally specified compositions which can enable musical performances or data sonifications that explore these concepts.

Open-form composition allows for a spectrum of engagement from fully specified to almost completely improvised performance. Our work is novel in that we capture concepts of adversarial action within a shared musical score mediated by a computer system and that we explore reduction and time-shifting of shared information to represent bandwidth and delay. Shared open-form compositions with computer displays have been previously explored (e.g., the work of Lindsay Vickery and Decibel more generally), but our work extends this concept to implement a formal language for generating these scores and software for mediating communication during performance.

We explore the properties of these generated scores through live test performances with human and artificial performers with computer generated sounds. These experiences allow the quality of our system to be measured in terms of the breadth of synchronised and coordinated experiences that can be represented and explored through our compositional language. Our system could also support non-musicians exploring open-form music through game-like performances to learn about collaboration and synchronisation in real-time.

## Presenters:

Dr Alexander Hunter; Dr Charles Martin 

## Bios (100 words each):

Dr Alexander Hunter is a Lecturer at the ANU School of Music where he convenes the Composition Major and Honours Degree. His research focuses on improvised and other "open" musical forms in terms of both notation and performer/composer interaction. These artistic and academic practices develop and explore dynamic systems of interaction and communication between players using novel approaches to music notation and ensemble organisation. His works range in scale from duo improvisations to small ensemble works to pieces for full orchestra.

Dr Charles Martin is a Senior Lecturer in the ANU School of Computing. Charles is an expert in music technology, human-computer interaction, and creative applications of AI/ML. Charles' expertise covers the development of musical apps such as MicroJam and PhaseRings, and creation of novel intelligent musical instruments where a machine-learning model is embedded within a physical musical device. Charles' recent experiences have included developing ensembles of self-playing synchronised embedded musical systems, researching in ML and evolutionary robotics, and performing music on percussion and computer.

## Keywords:

- Improvisation
- Open-form composition 
- Computer music 
- Collaboration 

## Institutional Affiliation:

Australian National University 
