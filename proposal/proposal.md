# DST Artificial Intelligence for Decision Making Initiative 2022 Application: _Open-Form Music Composition for Synchronised and Coordinated Action_

- [Application portal](https://www.dst.defence.gov.au/opportunity/artificial-intelligence-decision-making-initiative-2022)
- [Challenge Text](https://www.dst.defence.gov.au/sites/default/files/opportunities/documents/AI4DM_Challenges.pdf)
- Return code: TT7LCNKC

## Challenge this application addresses: 

**Musical scoring as a language of synchronised action**

- A musical score is a language of synchronised action. 
- We seek to develop a formalism/language for describing synchronised and coordinated actions by teams of actors. 
- This formalism needs to take into account the **uncertainties** of the environment which may be **adversarial** in nature. 
- We presume that actors can **communicate via limited bandwidth** subject to **delay** and yet they are **cooperating** to achieve a common goal. 
- For example, a squad of soccer players work together to kick goals and defend against their **opponents** from scoring goals. 
- How can this be described using a **formalism for synchronised and coordinated** action?

## Application

### Project Title

Open-Form Music Composition for Synchronised and Coordinated Action

### Lead Investigator

- Dr Charles Martin

### Co-Investigators

- Dr Alexander Hunter
- Mr Brent Schuetze

### Budget (Total: 29902.05)

- Salary: Dr Alexander Hunter (3 months 0.2FTE @ ANU Level B-3 + on-costs): 7767.75
- Salary: Mr Brent Schuetze (3 months 0.4FTE @ ANU Level 6-1 + on-costs): 11086.3
- Materials: Networked score performance system for test performances including: 4 tablet computers + stands,  4 monitor speakers, 4 audio interfaces: 11048

Salary and on costs:

Dr Alexander Hunter (3 months 0.2FTE @ ANU Level B-3) and Mr Brent Schuetze (3 months 0.4FTE @ ANU Level 6-1)

Materials: 


### Novelty, quality and feasibility of the approach

We propose to create a formal language to describe synchronised and coordinated actors within open-ended musical performances. Our system will encode concepts of coordination, adversaries/opponents, synchronised action, communication, bandwidth and delay. We propose to create a software system which will allow representation of formally specified performances as text or in graphical form which would (for example) enable musical performances or data sonifications that explore these concepts.

The musical space that will be explored is open-form composition which allow for a spectrum of engagement from fully specified to almost completely improvised performance. While many musical forms can represent synchrony, communication, and coordination, this type of composition allows for adversarial action (performers competing in some sense). Our work will be novel in that we capture the concept of adversarial action within a shared musical score mediated by a computer system and that we explore reduction and time-shifting of shared information to represent bandwidth and delay.

Shared open-score compositions with computer displays have been previously explored (e.g., Lindsay Vickery's Ubahn composition), but our work will be first to implement a formal language for generating these scores, and an automated system for displaying scores to performers through a computer system. 

We will explore the properties of these generated scores through live test performances where humans work against artificial performers with computer generated (sonified) sounds. These experiences will allow the quality of our system to be measured in terms of the breadth of synchronised and coordinated experiences that can be represented and explored through our compositional language. Our proposal is feasible due to the unique skills of the investigation team combining experience with open-form composition and improvisation with technology development, human-computer interaction and AI/ML research.

### Relevance and alignment to the challenge problem

Open-form musical performance is particularly well-aligned and relevant to exploration of synchronised and coordinated actions by teams of actors. Group dynamics in musical settings are complex with multiple simultaneous communications occurring between actors. These communications happen both in-band (through musical sounds) and out-of-band (through auxiliary gestures, visual cues, or even facial expressions). Unlike a team sport match, there is no "time out" in music, and the consequences of miscommunication can mean the failure of a performance.

Within this context, we propose to explore the possibility of generated scores, where the performers will not be aware of the score contents until the start of the performance. Our proposal is particularly aligned to the challenge problem because we will use a network of computers to display different scores to each performer, updating them with information during the performance. In this way, the displayed score can act as a kind of informational bottleneck, where performers can be given complete, limited, or time-delayed information regarding the actions of others in the group. 

Our formalism for synchronised actions will specify musical outcomes in terms of areas of a musical action space for performers to explore. Adversarial behaviour can be expressed through efforts to oppose, cancel or override certain performer actions (e.g., playing loudly in a quiet section or changing tonality).

We will explore the potential of this formalised language for open-form music in performances with the possibility for communication of concepts related to synchronised coordinated action to audiences. In the future, these explorations could also occur with non-musicians exploring our system through game-like performances to learn about collaboration and synchronisation in real-time.

### Expertise in relevant technical field

Dr Charles Martin is a Senior Lecturer in the ANU School of Computing. Charles is an expert in music technology, human-computer interaction, and creative applications of AI/ML. Charles' expertise covers the development of musical apps such as MicroJam and PhaseRings, and creation of novel intelligent musical instruments where a machine-learning model is embedded within a physical musical device. Charles' recent experiences have included developing ensembles of self-playing synchronised embedded musical systems, researching in ML and evolutionary robotics, and performing music on percussion and computer. Charles has a track record of delivering engaging music technology experiences that communicate technical concepts related to AI and collaboration to a wide audience.

Dr Alexander Hunter is a Lecturer at the ANU School of Music where he convenes the Composition Major and Honours Degree. His doctoral thesis _An Investigation into Contemporary Open Musical Forms: Moving beyond practice emanating from the New York School_ focused on improvised and other "open" musical forms in terms of both notation and performer/composer interaction. Alexander's artistic and academic practices develop and explore dynamic systems of interaction and communication between players using novel approaches to music notation and ensemble organisation. These works range in scale from duo improvisations to small ensemble works to pieces for full orchestra. In each of these unique systems of notation, the musical score guides the player in navigating the performance environment while providing space for individual freedom and player communication and interaction.

Mr Brent Schuetze is a software developer and teaching assistant at The Australian National University where he has completed bachelor studies in computer science. Brent has experience in software engineering for the defence sector, and developing software tools for music technology, creative computing, and computing education support.
