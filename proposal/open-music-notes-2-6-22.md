To start:

Each of the 4 players selects their instrument from a set of templates

Each instrument has default settings for:

- Pitch row (set of pitches)
- Amount of randomisation in selection of pitches
- sequence length - from 1 to 20 notes (maybe)
- Sequence speed - the group is given a base tempo (x) and their speed is .25x, .5x, x, 2x, etc.
- Waveform (basic synth sound)
- cutoff frequency
- attack
- release
- amplitude

They then choose 2 of these parameters to ‘equip’ so they can change them in real time

The group also selects how active they want the environmental actor to be (from very hands-off to very hands-on.

As the piece progresses, each player can manipulate their instrument only using their currently equipped parameters. 

At pre-determined intervals players may change instruments or the equipped parameters.

The environmental actor: 

Depending on the settings selected, the environment has jobs:

- At regular (?) intervals, provide choices to players (change equipped parameters, change instruments, etc.)
- At regular (?) intervals, suddenly change values in players’ parameters (change pitch row, change timbre, etc.)
- The environment can also delay, garble or delete messages between players. 

Some of these events might be advertised to the players, and some might come up based on their behaviour, or lack of behaviour - i.e. the environment trying to push the players into new creative situations. Maybe they get a warning that something is going to change soon? Maybe not?

In the GUI the player can see all of their parameters - with their equipped ones highlighted. They can also see a ‘send message’ text box and a log of received messages. The send message are has one box for the player they wish to talk to/about and a list of preset messages to send. The log displays messages with timestamps. 
