# open-form-music-computing

Code related to the Open-Form Music Composition for Synchronised and Coordinated Action

## Running

The backend and frontend components can be run locally with `docker-compose`:

```
docker-compose -f docker-compose.yml up  
```
