import socket
import queue
import time
import datetime as dt
import sys
import threading
import logging as log

# SERVER CONFIG
LISTEN_PORT = 32710

START_DT = dt.datetime.now()
START_TIMESTAMP = START_DT.strftime("%Y-%m-%d_%H-%M-%S")
LOG_FILENAME = f"server-logs_{START_TIMESTAMP}.log"

## Startup Handshake
# Receive a Hello! connection
# 
CONNECTIONS = {}
THREADS = {}
MESSAGE_PIPES = {}


def t_print(*text):
    # if type(text) == list:
    res = ""
    for i, item in enumerate(text):
        if i > 0:
            res = res + " "
        res = res + str(item)
    print(res, flush=True)


def send_message(conn, addr, message):
    conn.send(message.encode("utf-8"))


def receive_message(conn, addr):
    message = conn.recv(4096)
    message = message.decode("utf-8")
    return message


def receiver(id, conn, addr):
    conn.settimeout(60)
    while True:
        try:
            message = receive_message(conn, addr)
        except socket.timeout as e:
            t_print(f"Server timed out waiting for a message from {id}, exiting...")
            conn.close()
            break

        log.info(f"{id} : {message}")
        if message == "":
            # Close
            t_print(f"Ending connection with client-{id} - {addr}")
            CONNECTIONS[id]["status"] = "disconnected"
            conn.close()
            break
        elif message == "HB":
            # Hearbeat message, don't disconnect but don't do anything
            t_print(f"Server got heartbeat from client-{id}")
            t_print(f"Server has received {CONNECTIONS[id]['message_count']} messages from client-{id}")
        else:
            # t_print(f"Server got '{message}' from client-{id}")
            CONNECTIONS[id]["message_count"] = CONNECTIONS[id]["message_count"] + 1
            for mp_id in MESSAGE_PIPES:
                if mp_id != id:
                    MESSAGE_PIPES[mp_id].put(message)


def heartbeat(id, conn, addr):
    message = "HB"
    while True:
        if CONNECTIONS[id]["status"] == "connected":
            try:
                send_message(conn, addr, message)
            except Exception as e:
                t_print(f"Heartbeat for client-{id} got error: {e}")
                CONNECTIONS[id]["error_count"] = CONNECTIONS[id]["error_count"] + 1
                if CONNECTIONS[id]["error_count"] > 10:
                    t_print(f"Too many errors for {id} detected, disconnecting...")
                    CONNECTIONS[id]["status"] = "disconnected"
                    break
            time.sleep(20)
        elif CONNECTIONS[id]["status"] == "disconnected":
            break
    
    t_print(f"Heartbeat for client-{id} exiting")


def sender(id, conn, addr):
    connected = True
    conn.settimeout(60)
    while True:
        if connected:
            message = MESSAGE_PIPES[id].get(block=True)
            try:
                send_message(conn, addr, message)
            except socket.error as e:
                t_print(f"Connection to client-{id} is dead,\n{e}\nreconnecting...")
                connected = False
                continue
            except Exception as e:
                t_print(f"Server sender error with client-{id}\n{e}")

            t_print(f"Server sent '{message}' to {id}")
        else: # not connected
            t_print(f"Server attempting reconnect with client-{id}")
            try:
                conn.connect(addr)
                connected = True
                t_print(f"Reconnected with client-{id}")
            except socket.timeout as e:
                t_print(f"Server timed out waiting for client-{id}, exiting...")
                break




def server_hello():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # host = socket.gethostname()
    host = "localhost"
    host = ""
    port = LISTEN_PORT

    s.bind((host, port))
    s.listen(10)

    # Hello loop
    while True:
        print(f"Waiting for connection on {host}:{port}")
        try:
            conn, addr = s.accept()
        except KeyboardInterrupt:
            t_print(f"\nGot keyboard interrupt, dying...")
            s.shutdown(0)
            s.close()
            break
        except Exception as e:
            t_print(f"Oh shit something went wrong?\n{e}")
            s.close()
            break
        print(f"Connected to {addr}")
        print("Saying Hello")
        message = "Hello!"
        conn.send(message.encode("utf-8"))

        # Handshake initiated, wait for message back
        resp = "EMPTY"
        while resp[0:2] != "ID":
            resp = conn.recv(4096)
            resp = resp.decode("utf-8")
        print(f"Adding {resp} to threads")

        conn.send(f"Ack-{resp.split('-')[1]}".encode("utf-8"))
        
        THREADS.setdefault(resp, [])
        THREADS[resp].append(threading._start_new_thread(sender, (resp, conn, addr))) # SENDER
        THREADS[resp].append(threading._start_new_thread(receiver, (resp, conn, addr))) # RECEIVER
        THREADS[resp].append(threading._start_new_thread(heartbeat, (resp, conn, addr))) # HEARTBEAT

        CONNECTIONS[resp] = {}
        CONNECTIONS[resp]["status"] = "connected"
        CONNECTIONS[resp]["error_count"] = 0
        CONNECTIONS[resp]["message_count"] = 0
        MESSAGE_PIPES[resp] = queue.Queue(0)
        log.info(f"{resp} - connected with address {addr}")


# Set up the logging
log.basicConfig(
    filename=LOG_FILENAME,
    level=log.DEBUG,
    format="%(levelname)s : %(relativeCreated).0f : %(message)s"
    # datefmt="%Y-%m-%d_%H-%M-%S"
)
log.info(f"Starting at {START_TIMESTAMP}")
server_hello()

sys.exit(0)