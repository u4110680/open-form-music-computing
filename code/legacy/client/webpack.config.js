const path = require('path');

module.exports = {
  entry: './src/model-d.js',
  // entry: './src/poc.js',
  mode: "production",
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
};