# Websocket client implementation

## Building

### Installation

For this you will need the Typescript transpiler and the node dependencies for webpack. 

To install the typescript transpiler:
```bash
npm install typescript -g
```

To install webpack and other dev dependencies:
```bash
# In this directory
npm install
```

### Compiling

The client is developed in `index.html` and `src/<SOURCE_FILE>.ts`.

Currently it is using webpack to bundle the necessary libraries into the outputted javascript file.

To transpile please run:

```bash
tsc src/<SOURCE_FILE>.ts && npx webpack
```

## Running

Host the directory and navigate to the website.

You can also find the current client [here](http://bschuetze.xyz/app/).

The current default IP and Port for the server are `203.129.19.254` and `3310`.