// import * as net from "net"; // require("net");
// var net = require("net");
// const socket_io = require("socket.io");
// import { io } from "../lib/socket.io.js";
// import { io } from "socket.io-client";
// var Socket = new WebSocket(url, [protocal] );
// const socket = io("127.0.0.1:4001");
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var ID = Math.round(Math.random() * 1000000);
var HOST;
var PORT;
var SOCKET;
var CONNECTED = false;
var HB_INTERVAL;
var DUMMY_MESSAGE;
function init(host, port, protocol) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    HOST = host;
                    PORT = port;
                    console.log("Client connecting to ".concat(HOST, ":").concat(PORT, " using ").concat(protocol));
                    SOCKET = new WebSocket("ws://".concat(HOST, ":").concat(PORT));
                    SOCKET.onopen = on_connect;
                    SOCKET.onmessage = on_message;
                    setInterval(function () { console.log(SOCKET.readyState); }, 1000);
                    _a.label = 1;
                case 1:
                    if (!true) return [3 /*break*/, 3];
                    return [4 /*yield*/, sleep(1000)];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 1];
                case 3: return [2 /*return*/];
            }
        });
    });
}
function on_connect() {
    console.log("Client connected to ".concat(HOST, ":").concat(PORT));
    CONNECTED = true;
    send_heartbeat();
    HB_INTERVAL = setInterval(send_heartbeat, 1000);
    DUMMY_MESSAGE = setInterval(function () {
        return send_message("".concat(ID, "_DUMMY_").concat(Date.now()));
    }, 1000);
}
function on_message(message) {
    console.info("Client got message ".concat(message.data));
    console.log(message);
}
function on_close() {
    console.log("Client disconnected from ".concat(HOST, ":").concat(PORT));
    clearInterval(HB_INTERVAL);
}
function send_message(message) {
    if (CONNECTED) {
        console.info("Client sending ".concat(message));
        SOCKET.send(message);
    }
}
function send_heartbeat() {
    send_message("HB");
}
function sleep(ms) {
    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
}
init("203.129.19.254", 3310, "tcp");
