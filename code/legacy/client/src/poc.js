"use strict";
// import * as net from "net"; // require("net");
// var net = require("net");
// const socket_io = require("socket.io");
// import { io } from "../lib/socket.io.js";
// import { io } from "socket.io-client";
// var Socket = new WebSocket(url, [protocal] );
// const socket = io("127.0.0.1:4001");
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _a, _b;
exports.__esModule = true;
// const Nexus = require("nexusui");
// const Tone = require("tone");
var Tone = require("tone");
var Nexus = require("nexusui");
// import { Tone } from "./tone";
// const Nexus = NexusLIB.Nexus;
// HTML ELEMENTS
var initServerStatusField = document.getElementById('initServerStatus');
// COLOURS
var connectedColour = "rgb(68, 207, 68)";
var disconnectedColour = "rgb(104, 106, 109)";
var connectingColour = "rgb(205, 207, 68)";
var connectionErrorColour = "rgb(207, 68, 68)";
var CONFIGURED = false;
var NAME = null;
var ID = Math.round(Math.random() * 100000000).toString().substring(0, 4);
var HOST;
var PORT;
var SOCKET;
var CONNECTED = false;
var HB_INTERVAL;
var DUMMY_MESSAGE;
var CONTROL_DICT = {};
function init(host, port, protocol) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            HOST = host;
            PORT = port;
            console.log("Client connecting to ".concat(HOST, ":").concat(PORT, " using ").concat(protocol));
            initServerStatusField.value = "Connecting...";
            initServerStatusField.style.backgroundColor = connectingColour;
            CONFIGURED = true;
            SOCKET = new WebSocket("ws://".concat(HOST, ":").concat(PORT));
            SOCKET.onopen = on_connect;
            SOCKET.onmessage = on_message;
            SOCKET.onerror = on_error;
            SOCKET.onclose = on_close;
            return [2 /*return*/];
        });
    });
}
function on_connect() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log("Client connected to ".concat(HOST, ":").concat(PORT));
                    initServerStatusField.value = "Connected!";
                    initServerStatusField.style.backgroundColor = connectedColour;
                    CONNECTED = true;
                    CONFIGURED = true;
                    send_hello();
                    HB_INTERVAL = setInterval(send_heartbeat, 10000);
                    setInterval(function () { console.log(SOCKET.readyState); }, 10000);
                    _a.label = 1;
                case 1:
                    if (!true) return [3 /*break*/, 3];
                    return [4 /*yield*/, sleep(1000)];
                case 2:
                    _a.sent();
                    return [3 /*break*/, 1];
                case 3: return [2 /*return*/];
            }
        });
    });
}
function on_message(message) {
    return __awaiter(this, void 0, void 0, function () {
        var parsed_message;
        return __generator(this, function (_a) {
            console.info("Client got message ".concat(message.data));
            parsed_message = CTMessage.parse_message(message.data);
            process_message(parsed_message);
            return [2 /*return*/];
        });
    });
}
function on_close(event) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            console.log("Client disconnected from ".concat(HOST, ":").concat(PORT));
            console.log(event);
            clearInterval(HB_INTERVAL);
            if (event.wasClean) {
                initServerStatusField.value = "Connection Closed";
                initServerStatusField.style.backgroundColor = disconnectedColour;
            }
            CONNECTED = false;
            CONFIGURED = false;
            return [2 /*return*/];
        });
    });
}
function on_error(event) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            console.log("Error from websocket:");
            console.log(event);
            initServerStatusField.value = "Connection Error";
            initServerStatusField.style.backgroundColor = connectionErrorColour;
            CONNECTED = false;
            CONFIGURED = false;
            return [2 /*return*/];
        });
    });
}
function send_message(message) {
    if (CONNECTED) {
        console.info("Client sending ".concat(message));
        SOCKET.send(message);
    }
}
function send_CTMessage(message) {
    if (CONNECTED) {
        console.debug("Client sending ".concat(message.construct_message()));
        SOCKET.send(message.construct_message());
    }
}
function send_hello() {
    var hbm = new CTMessage(NAME, CTType.NEW_CONNECTION);
    send_CTMessage(hbm);
}
function send_heartbeat() {
    console.log("Client sending heartbeat");
    var hbm = new CTMessage(NAME, CTType.HEARTBEAT);
    send_CTMessage(hbm);
}
function sleep(ms) {
    return new Promise(function (resolve) { return setTimeout(resolve, ms); });
}
(_a = document.getElementById('configSubmit')) === null || _a === void 0 ? void 0 : _a.addEventListener('click', function () { return __awaiter(void 0, void 0, void 0, function () {
    var host, port_str, name, port;
    return __generator(this, function (_a) {
        if (CONFIGURED) {
            console.error("Already configured");
            return [2 /*return*/];
        }
        host = document.getElementById('address').value;
        if (host == null || host == "") {
            host = document.getElementById('address').placeholder;
        }
        port_str = document.getElementById('port').value;
        if (port_str == null || port_str == "") {
            port_str = document.getElementById('port').placeholder;
        }
        name = document.getElementById('name').value;
        if (name == null || name.trim().length <= 0 || name.length > 10) {
            console.error("Name must be 1-10 characters");
            return [2 /*return*/];
        }
        port = null;
        try {
            port = Number.parseInt(port_str);
            if (Number.isNaN(port) || port < 1 || port > 65535) {
                throw "Port Out Of Range (1 - 65535)";
            }
        }
        catch (err) {
            console.error("Port value error:", err);
            return [2 /*return*/];
        }
        NAME = "".concat(name, "#").concat(ID);
        console.log("Starting up with '".concat(NAME, "' ").concat(host, ":").concat(port));
        init(host, port, "tcp");
        return [2 /*return*/];
    });
}); });
// Message functions
var CTType;
(function (CTType) {
    CTType["NEW_CONNECTION"] = "NEW_CONNECTION";
    CTType["END_CONNECTION"] = "END_CONNECTION";
    CTType["NEW_PLAYER"] = "NEW_PLAYER";
    CTType["REMOVE_PLAYER"] = "REMOVE_PLAYER";
    CTType["INPUT_CONTROL"] = "INPUT_CONTROL";
    CTType["SERVER_CONTROL"] = "SERVER_CONTROL";
    CTType["GLOBAL_MESSAGE"] = "GLOBAL_MESSAGE";
    CTType["PRIVATE_MESSAGE"] = "PRIVATE_MESSAGE";
    CTType["HEARTBEAT"] = "HEARTBEAT";
    CTType["OTHER"] = "OTHER";
})(CTType || (CTType = {}));
var CTMessage = /** @class */ (function () {
    function CTMessage(sender, type, content) {
        this.sender = sender;
        this.type = type;
        this.content = content;
    }
    CTMessage.prototype.construct_message = function () {
        var ret = "".concat(CTMessage._MESSAGE_START).concat(this.sender).concat(CTMessage._MESSAGE_SEPARATOR).concat(this.type.toString());
        if (this.content != null) {
            ret += "".concat(CTMessage._MESSAGE_SEPARATOR).concat(this.content);
        }
        ret += "".concat(CTMessage._MESSAGE_END);
        return ret;
    };
    CTMessage.parse_message = function (raw_message) {
        var sender = null;
        var messageType = null;
        var content = null;
        var ret = null;
        if (raw_message.startsWith(CTMessage._MESSAGE_START) &&
            raw_message.endsWith(CTMessage._MESSAGE_END)) {
            try {
                var temp_string = raw_message.substring(CTMessage._MESSAGE_START.length);
                temp_string = temp_string.substring(0, temp_string.length - CTMessage._MESSAGE_END.length);
                var entries = temp_string.split(CTMessage._MESSAGE_SEPARATOR);
                if (entries.length > 0) {
                    sender = entries[0];
                }
                if (entries.length > 1) {
                    messageType = entries[1];
                }
                if (entries.length > 2) {
                    content = entries[2];
                }
                ret = new CTMessage(sender, messageType, content);
            }
            catch (err) {
                console.error("PARSER: error parsing message '".concat(raw_message, "' - ").concat(err));
            }
        }
        else {
            console.warn("PARSER: message '".concat(raw_message, "' does not conform to CTMessage standard"));
        }
        return ret;
    };
    CTMessage._MESSAGE_START = "[[";
    CTMessage._MESSAGE_SEPARATOR = "%";
    CTMessage._MESSAGE_END = "]]";
    CTMessage._INPUT_CONTROL_SEPARATOR = "/";
    return CTMessage;
}());
function sendControlMessage(controlString) {
    var message = new CTMessage(NAME, CTType.INPUT_CONTROL, controlString);
    send_CTMessage(message);
}
function process_message(message) {
    if (message == null) {
        // TODO: Maybe do something here who knows
        return;
    }
    switch (message.type) {
        case CTType.HEARTBEAT:
            // TODO: What to do with heartbeat here?
            break;
        case CTType.INPUT_CONTROL:
            console.log("PROCESS: input control");
            var path = message.content.substring(CTMessage._INPUT_CONTROL_SEPARATOR.length).split(CTMessage._INPUT_CONTROL_SEPARATOR);
            console.log(path);
            var ref = CONTROL_DICT[path[0]];
            var field = path[path.length - 2];
            var value = path[path.length - 1];
            console.log(ref);
            for (var i = 1; i < path.length - 2; i++) {
                ref = ref[path[i]];
                console.log(ref);
            }
            if (field == "value") {
                ref["_value"]["value"] = value;
                ref.render();
            }
            break;
        default:
            console.warn("PROCESS: no behaviour implemented for type ".concat(message.type));
            break;
    }
}
// SERVER STATUS
var serverStatusElement = document.getElementById("serverStatus");
// MUSIC
var power = new Nexus.Add.Toggle("#instrument");
(_b = document.getElementById('button')) === null || _b === void 0 ? void 0 : _b.addEventListener('click', function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        setup();
        return [2 /*return*/];
    });
}); });
function setup() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, Tone.start()];
                case 1:
                    _a.sent();
                    console.log('audio is ready');
                    CONTROL_DICT["dial"] = {};
                    CONTROL_DICT["dial"]["harmDial"] = harmDial;
                    CONTROL_DICT["dial"]["slaveDial"] = slaveDial;
                    return [2 /*return*/];
            }
        });
    });
}
var harmDial = Nexus.Add.Dial('#instrument', {
    'size': [75, 75],
    'value': 1.0,
    'min': 0,
    'max': 2
});
var slaveDial = Nexus.Add.Dial('#instrument', {
    'size': [75, 75],
    'value': 1.0,
    'min': 0,
    'max': 2
});
harmDial.on('change', function (v) {
    // synth.harmonicity.rampTo(v,.1)
    console.log("harmDial", v);
    sendControlMessage("/dial/harmDial/value/".concat(v));
    // slaveDial._value.value = v;
    // slaveDial.render();
});
slaveDial.on('change', function (v) {
    // synth.harmonicity.rampTo(v,.1)
    console.log("slaveDial", v);
    sendControlMessage("/dial/slaveDial/value/".concat(v));
});
