// import * as net from "net"; // require("net");
// var net = require("net");
// const socket_io = require("socket.io");
// import { io } from "../lib/socket.io.js";
// import { io } from "socket.io-client";
// var Socket = new WebSocket(url, [protocal] );
// const socket = io("127.0.0.1:4001");

var ID = Math.round(Math.random()*1000000);
var HOST: String;
var PORT: Number;
var SOCKET: WebSocket;
var CONNECTED: Boolean = false;
var HB_INTERVAL;
var DUMMY_MESSAGE;

async function init(host: String, port: Number, protocol: String) {
    HOST = host;
    PORT = port;
    console.log(`Client connecting to ${HOST}:${PORT} using ${protocol}`);
    SOCKET = new WebSocket(`ws://${HOST}:${PORT}`);
    SOCKET.onopen = on_connect;
    SOCKET.onmessage = on_message;
    setInterval(() => {console.log(SOCKET.readyState)}, 1000);
    while (true) {
        await sleep(1000);
    }
}

function on_connect() {
    console.log(`Client connected to ${HOST}:${PORT}`);
    CONNECTED = true;
    send_heartbeat();
    HB_INTERVAL = setInterval(send_heartbeat, 1000);
    DUMMY_MESSAGE = setInterval(
        () => {
            return send_message(`${ID}_DUMMY_${Date.now()}`);
        }, 
        1000
    );
}

function on_message(message: MessageEvent) {
    console.info(`Client got message ${message.data}`);
    console.log(message)
}

function on_close() {
    console.log(`Client disconnected from ${HOST}:${PORT}`);
    clearInterval(HB_INTERVAL);
}

function send_message(message: string) {
    if (CONNECTED) {
        console.info(`Client sending ${message}`)
        SOCKET.send(message);
    }
}

function send_heartbeat() {
    send_message("HB")
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

init("203.129.19.254", 3310, "tcp");