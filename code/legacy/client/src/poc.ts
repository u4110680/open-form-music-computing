// import * as net from "net"; // require("net");
// var net = require("net");
// const socket_io = require("socket.io");
// import { io } from "../lib/socket.io.js";
// import { io } from "socket.io-client";
// var Socket = new WebSocket(url, [protocal] );
// const socket = io("127.0.0.1:4001");

// const Nexus = require("nexusui");
// const Tone = require("tone");
import * as Tone from "tone";
import * as Nexus from "nexusui";
// import { Tone } from "./tone";
// const Nexus = NexusLIB.Nexus;

// HTML ELEMENTS
const initServerStatusField : HTMLInputElement = (<HTMLInputElement> document.getElementById('initServerStatus'));

// COLOURS
const connectedColour : string = "rgb(68, 207, 68)";
const disconnectedColour : string = "rgb(104, 106, 109)";
const connectingColour : string = "rgb(205, 207, 68)";
const connectionErrorColour : string = "rgb(207, 68, 68)";

var CONFIGURED: Boolean = false;
var NAME: string = null;
const ID: string = Math.round(Math.random()*100000000).toString().substring(0, 4) ;
var HOST: string;
var PORT: Number;
var SOCKET: WebSocket;
var CONNECTED: Boolean = false;
var HB_INTERVAL;
var DUMMY_MESSAGE;

var CONTROL_DICT = {};

async function init(host: string, port: Number, protocol: string) {
    HOST = host;
    PORT = port;
    console.log(`Client connecting to ${HOST}:${PORT} using ${protocol}`);
    initServerStatusField.value = "Connecting...";
    initServerStatusField.style.backgroundColor = connectingColour;
    CONFIGURED = true;
    SOCKET = new WebSocket(`ws://${HOST}:${PORT}`);
    SOCKET.onopen = on_connect;
    SOCKET.onmessage = on_message;
    SOCKET.onerror = on_error;
    SOCKET.onclose = on_close;
}

async function on_connect() {
    console.log(`Client connected to ${HOST}:${PORT}`);
    initServerStatusField.value = "Connected!";
    initServerStatusField.style.backgroundColor = connectedColour;
    CONNECTED = true;
    CONFIGURED = true;
    send_hello();
    HB_INTERVAL = setInterval(send_heartbeat, 10000);

    setInterval(() => {console.log(SOCKET.readyState)}, 10000);
    while (true) {
        await sleep(1000);
    }
    // DUMMY_MESSAGE = setInterval(
    //     () => {
    //         return send_message(`${ID}_DUMMY_${Date.now()}`);
    //     }, 
    //     1000
    // );
}

async function on_message(message: MessageEvent) {
    console.info(`Client got message ${message.data}`);
    // console.log(message);
    let parsed_message = CTMessage.parse_message(message.data);
    process_message(parsed_message);
}

async function on_close(event: CloseEvent) {
    console.log(`Client disconnected from ${HOST}:${PORT}`);
    console.log(event);
    clearInterval(HB_INTERVAL);
    if (event.wasClean) {
        initServerStatusField.value = "Connection Closed";
        initServerStatusField.style.backgroundColor = disconnectedColour;
    }
    CONNECTED = false;
    CONFIGURED = false;
}

async function on_error(event: Event) {
    console.log(`Error from websocket:`);
    console.log(event);
    initServerStatusField.value = "Connection Error";
    initServerStatusField.style.backgroundColor = connectionErrorColour;
    CONNECTED = false;
    CONFIGURED = false;
}

function send_message(message: string) {
    if (CONNECTED) {
        console.info(`Client sending ${message}`)
        SOCKET.send(message);
    }
}

function send_CTMessage(message: CTMessage) {
    if (CONNECTED) {
        console.debug(`Client sending ${message.construct_message()}`)
        SOCKET.send(message.construct_message());
    }
}

function send_hello() {
    let hbm = new CTMessage(NAME, CTType.NEW_CONNECTION);
    send_CTMessage(hbm);
}

function send_heartbeat() {
    console.log("Client sending heartbeat")
    let hbm = new CTMessage(NAME, CTType.HEARTBEAT);
    send_CTMessage(hbm);
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

document.getElementById('configSubmit')?.addEventListener('click', async () => {
    if (CONFIGURED) {
        console.error("Already configured");
        return;
    }

    let host: string = (<HTMLInputElement> document.getElementById('address')).value;
    if (host == null || host == "") {
        host = (<HTMLInputElement> document.getElementById('address')).placeholder;
    }
    
    let port_str: string = (<HTMLInputElement>document.getElementById('port')).value;
    if (port_str == null || port_str == "") {
        port_str = (<HTMLInputElement>document.getElementById('port')).placeholder;
    }

    let name: string = (<HTMLInputElement>document.getElementById('name')).value;
    if (name == null || name.trim().length <= 0 || name.length > 10) {
        console.error("Name must be 1-10 characters");
        return;
    }
    let port: number = null;
    try {
        port = Number.parseInt(port_str);
        if (Number.isNaN(port) || port < 1 || port > 65535) {
            throw "Port Out Of Range (1 - 65535)";
        }
    } catch(err) {
        console.error("Port value error:", err);
        return;
    }
    NAME = `${name}#${ID}`;
    console.log(`Starting up with '${NAME}' ${host}:${port}`)
    init(host, port, "tcp");
});

// Message functions
enum CTType {
    NEW_CONNECTION  = "NEW_CONNECTION",
    END_CONNECTION  = "END_CONNECTION",
    NEW_PLAYER      = "NEW_PLAYER",
    REMOVE_PLAYER   = "REMOVE_PLAYER",
    INPUT_CONTROL   = "INPUT_CONTROL",
    SERVER_CONTROL  = "SERVER_CONTROL",
    GLOBAL_MESSAGE  = "GLOBAL_MESSAGE",
    PRIVATE_MESSAGE = "PRIVATE_MESSAGE",
    HEARTBEAT       = "HEARTBEAT",
    OTHER           = "OTHER",
}

class CTMessage {
    sender:  string;
    type:    CTType;
    content: string;

    static _MESSAGE_START: string = "[[";
    static _MESSAGE_SEPARATOR: string = "%";
    static _MESSAGE_END: string = "]]";
    static _INPUT_CONTROL_SEPARATOR = "/";

    constructor(sender: string, type: CTType, content?: string) {
        this.sender = sender;
        this.type = type;
        this.content = content;
    }

    construct_message(): string {
        let ret: string = `${CTMessage._MESSAGE_START}${this.sender}${CTMessage._MESSAGE_SEPARATOR}${this.type.toString()}`;
        if (this.content != null) {
            ret += `${CTMessage._MESSAGE_SEPARATOR}${this.content}`;
        }
        ret += `${CTMessage._MESSAGE_END}`;
        return ret;
    }

    static parse_message(raw_message: string): CTMessage {
        let sender: string = null;
        let messageType: CTType = null;
        let content: string = null;

        let ret: CTMessage = null;

        if (raw_message.startsWith(CTMessage._MESSAGE_START) &&
            raw_message.endsWith(CTMessage._MESSAGE_END)) {
                try {
                let temp_string = raw_message.substring(CTMessage._MESSAGE_START.length);
                temp_string = temp_string.substring(0, temp_string.length - CTMessage._MESSAGE_END.length);
                let entries = temp_string.split(CTMessage._MESSAGE_SEPARATOR);
                if (entries.length > 0) {
                    sender = entries[0];
                }
                if (entries.length > 1) {
                    messageType = <CTType> entries[1];
                }
                if (entries.length > 2) {
                    content = entries[2];
                }
                ret = new CTMessage(sender, messageType, content);
            } catch (err) {
                console.error(`PARSER: error parsing message '${raw_message}' - ${err}`);
            }
        } else {
            console.warn(`PARSER: message '${raw_message}' does not conform to CTMessage standard`)
        }
        return ret;
    }
}

function sendControlMessage(controlString: string) {
    let message = new CTMessage(NAME, CTType.INPUT_CONTROL, controlString);
    send_CTMessage(message);
}

function process_message(message: CTMessage) {
    if (message == null) {
        // TODO: Maybe do something here who knows
        return;
    }

    switch(message.type) {
        case CTType.HEARTBEAT:
            // TODO: What to do with heartbeat here?
            break;
        case CTType.INPUT_CONTROL:
            console.log(`PROCESS: input control`);
            let path = message.content.substring(CTMessage._INPUT_CONTROL_SEPARATOR.length).split(CTMessage._INPUT_CONTROL_SEPARATOR);
            console.log(path);
            let ref: any = CONTROL_DICT[path[0]];
            let field: string = path[path.length - 2];
            let value: any = path[path.length - 1];
            console.log(ref);
            for (let i = 1; i < path.length - 2; i++) {
                ref = ref[path[i]];
                console.log(ref);
            }
            if (field == "value") {
                ref["_value"]["value"] = value;
                ref.render();
            }
            break;
        default:
            console.warn(`PROCESS: no behaviour implemented for type ${message.type}`);
            break;
    }
}

// SERVER STATUS
const serverStatusElement = document.getElementById("serverStatus");


// MUSIC

var power = new Nexus.Add.Toggle("#instrument");

document.getElementById('button')?.addEventListener('click', async () => {
    setup();
});
  
async function setup() {
    await Tone.start();
    console.log('audio is ready');
    CONTROL_DICT["dial"] = {};
    CONTROL_DICT["dial"]["harmDial"] = harmDial;
    CONTROL_DICT["dial"]["slaveDial"] = slaveDial;
}

var harmDial = Nexus.Add.Dial('#instrument',{
    'size': [75,75],
    'value': 1.0,
    'min': 0,
    'max': 2,
});

var slaveDial = Nexus.Add.Dial('#instrument',{
    'size': [75,75],
    'value': 1.0,
    'min': 0,
    'max': 2,
});

harmDial.on('change',function(v) {
    // synth.harmonicity.rampTo(v,.1)
    console.log("harmDial", v);
    sendControlMessage(`/dial/harmDial/value/${v}`);
    // slaveDial._value.value = v;
    // slaveDial.render();
});

slaveDial.on('change',function(v) {
    // synth.harmonicity.rampTo(v,.1)
    console.log("slaveDial", v);
    sendControlMessage(`/dial/slaveDial/value/${v}`);
});

