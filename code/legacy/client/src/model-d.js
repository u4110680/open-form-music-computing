var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import * as Tone from "./tone.js";
import * as Nexus from "./nexusui.js";
// RACK
class ModelDRack {
    constructor(parentDiv, parentID, numberOfOscillators = 2) {
        this.oscillators = [];
        this.root = parentDiv;
        this.rootID = parentID;
        this.numOsc = numberOfOscillators;
    }
    create() {
        this.power = Nexus.Add.Toggle(`#${this.rootID}OnSwitch`);
        const buttonWidth = 100;
        //  ---------- Oscillators
        for (let i = 0; i < this.numOsc; i++) {
            // ----- Range Selector
            const numRangeButtons = 6;
            const rangeLabels = [
                "LO", "32'", "16'", "8'", "4'", "2'"
            ];
            let oscRange = Nexus.Add.RadioButton(`#${this.rootID}Osc${i}Range`, {
                "size": [numRangeButtons * buttonWidth, buttonWidth],
                "numberOfButtons": numRangeButtons,
                "active": 3
            });
            let rangeCircles = document.querySelectorAll(`#${this.rootID}Osc${i}Range div div span svg circle`);
            for (let j = 0; j < rangeCircles.length; j++) {
                const circle = rangeCircles.item(j);
                circle.insertAdjacentHTML("afterend", `<text x="50%" y="50%" dominant-baseline="central" text-anchor="middle" pointer-events="none" font-size="${buttonWidth * 0.5}px">${rangeLabels[j]}</text>`);
            }
            // ----- Waveform Selector
            const numWaveformButtons = 6;
            const waveformLabels = [
                "∿", "⎍", "	⌁", "∧", "?", "?"
            ];
            let oscWaveform = Nexus.Add.RadioButton(`#${this.rootID}Osc${i}Waveform`, {
                "size": [numWaveformButtons * buttonWidth, buttonWidth],
                "numberOfButtons": numWaveformButtons,
                "active": 3
            });
            let waveformCircles = document.querySelectorAll(`#${this.rootID}Osc${i}Waveform div div span svg circle`);
            for (let j = 0; j < waveformCircles.length; j++) {
                const circle = waveformCircles.item(j);
                circle.insertAdjacentHTML("afterend", `<text x="50%" y="50%" dominant-baseline="central" text-anchor="middle" pointer-events="none" font-size="${buttonWidth * 0.5}px">${waveformLabels[j]}</text>`);
            }
            // ----- Volume Dial
            let volumeDial = Nexus.Add.Dial(`#${this.rootID}Osc${i}Volume`, {
                "size": [buttonWidth, buttonWidth],
                "min": 0,
                "max": 10,
                "value": 5
            });
            // ----- Toggle Switch
            let onOffSwitch = Nexus.Add.Toggle(`#${this.rootID}Osc${i}OnOff`, {
                "size": [buttonWidth / 2, buttonWidth],
                "state": true
            });
            let oscDict = {
                "range": oscRange,
                "waveform": oscWaveform,
                "volume": volumeDial,
                "onOffSwitch": onOffSwitch
            };
            if (i > 0) {
                // ----- Detune Dial
                let detuneDial = Nexus.Add.Dial(`#${this.rootID}Osc${i}Detune`, {
                    "size": [buttonWidth, buttonWidth],
                    "min": -7,
                    "max": 7,
                    "value": 0
                });
                oscDict["detune"] = detuneDial;
            }
            this.oscillators.push(oscDict);
        }
        // ---------- External In
        // ----- Toggle Switch
        let externalOnOff = new Nexus.Toggle(`#${this.rootID}ExternalOnOff`, {
            "size": [buttonWidth / 2, buttonWidth],
            "state": false
        });
        console.log(externalOnOff);
        // ----- Volume Dial
        let externalVolumeDial = Nexus.Add.Dial(`#${this.rootID}ExternalVolume`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 5
        });
        // ---------- Noise
        // ----- On Off Switch
        let noiseOnOff = Nexus.Add.Toggle(`#${this.rootID}NoiseOnOff`, {
            "size": [buttonWidth / 2, buttonWidth],
            "state": false
        });
        // ----- Volume Dial
        let noiseVolumeDial = Nexus.Add.Dial(`#${this.rootID}NoiseVolume`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 5
        });
        // ----- Type Toggle Switch
        let noiseType = Nexus.Add.Toggle(`#${this.rootID}NoiseType`, {
            "size": [buttonWidth / 2, buttonWidth],
            "state": false
        });
        // ---------- Filter
        // ----- Mode Toggle
        let lowHighFilterToggle = Nexus.Add.Toggle(`#${this.rootID}FilterLowHigh`, {
            "size": [buttonWidth / 2, buttonWidth],
            "state": false
        });
        // ----- Modulation Toggle
        let modulationFilterToggle = Nexus.Add.Toggle(`#${this.rootID}FilterModulationOnOff`, {
            "size": [buttonWidth / 2, buttonWidth],
            "state": false
        });
        // ----- Cutoff Frequency
        let cutoffFrequencyFilterDial = Nexus.Add.Dial(`#${this.rootID}FilterCutoff`, {
            "size": [buttonWidth, buttonWidth],
            "min": -5,
            "max": 5,
            "value": 0
        });
        // ----- Emphasis
        let emphasisFilterDial = Nexus.Add.Dial(`#${this.rootID}FilterEmphasis`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 1
        });
        // ----- Amount of Control
        let contourFilterDial = Nexus.Add.Dial(`#${this.rootID}FilterContour`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 1
        });
        // ---------- Envelope
        // ----- Attack Dial
        let attackEnvelopeDial = Nexus.Add.Dial(`#${this.rootID}EnvelopeAttack`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 0
        });
        // ----- Decay Dial
        let decayEnvelopeDial = Nexus.Add.Dial(`#${this.rootID}EnvelopeDecay`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 1
        });
        // ----- Sustain Dial
        let sustainEnvelopeDial = Nexus.Add.Dial(`#${this.rootID}EnvelopeSustain`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 5
        });
        // ---------- Loudness Contour
        // ----- Attack Dial
        let attackLoudnessContourDial = Nexus.Add.Dial(`#${this.rootID}LoudnessContourAttack`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 0
        });
        // ----- Decay Dial
        let decayLoudnessContourDial = Nexus.Add.Dial(`#${this.rootID}LoudnessContourDecay`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 1
        });
        // ----- Sustain Dial
        let sustainLoudnessContourDial = Nexus.Add.Dial(`#${this.rootID}LoudnessContourSustain`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 5
        });
        // --------- Output
        // ----- Volume
        let volumeOutputDial = Nexus.Add.Dial(`#${this.rootID}OutputVolume`, {
            "size": [buttonWidth, buttonWidth],
            "min": 0,
            "max": 10,
            "value": 5
        });
        // ----- OnOff Toggle
        let onOffOutputToggle = Nexus.Add.Toggle(`#${this.rootID}OutputOnOff`, {
            "size": [buttonWidth / 2, buttonWidth],
            "state": true
        });
    }
}
// HTML ELEMENTS
const initSettingsDiv = document.getElementById('initSettings');
const audioContextDiv = document.getElementById('audioContext');
const audioContextTextDiv = document.getElementById('audioContextText');
const audioContext = Nexus.Add.Button("#audioContextButton", {
    "size": [300, 300],
    "mode": "toggle",
});
const localRackID = "localRack";
const localRackDiv = document.getElementById(localRackID);
// main
initSettingsDiv.style.display = "none";
audioContext.on("change", (buttonState) => __awaiter(void 0, void 0, void 0, function* () {
    if (buttonState) {
        yield Tone.start();
        console.log('audio is ready');
        audioContextTextDiv.innerHTML = "Audio Ready!";
        setTimeout(() => {
            audioContextDiv.style.display = "none";
        }, 2000);
    }
    // else {
    //     console.log('audio is off');
    // }
}));
// Rack
const LOCALRACK = new ModelDRack(localRackDiv, localRackID);
LOCALRACK.create();
function setup() {
    return __awaiter(this, void 0, void 0, function* () {
    });
}
