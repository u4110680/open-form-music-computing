import sys
import os
import socket
import threading
import time
import queue
import uuid
import random


def getsocket(prot):
    if prot == "udp":
        return socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # INET, UDP
    else: # prot == "tcp"
        return socket.socket(socket.AF_INET, socket.SOCK_STREAM) # INET, TCP


def t_print(*text):
    # if type(text) == list:
    res = ""
    for i, item in enumerate(text):
        if i > 0:
            res = res + " "
        res = res + str(item)
    print(res, flush=True)


def send_message(sock, host, port, message, protocol="tcp"):
    if protocol == "udp":
        sock.sendto(message.encode("utf-8"), (host, port))
    else: # protocol == "tcp"
        sock.send(message.encode("utf-8"))


def receive_message(conn, addr):
    try:
        message = conn.recv(4096)
    except Exception as e:
        t_print(f"failed to receive message\n{e}")
        raise e
    try:
        message = message.decode("utf-8")
    except Exception as e:
        t_print(f"Failed to decode message\n{e}")
        raise e
    return message


def listener(port, prot, host="localhost"):
    # Listens to local port, passes on messages to server-send-queue
    t_print(f"Listener spawning on {host}:{port}:{prot}")
    sock = getsocket(prot)
    sock.bind((host, port))
    if prot == "udp":
        while True:
            while True:
                data, addr = sock.recvfrom(4096)
                message = data.decode("utf-8")
                if message == "":
                    t_print(f"Got disconnect from {addr}")
                    sock.close()
                    break
                # t_print(addr, message)
                SEND_MESSAGE_PIPE.put(message, block=True)
    else: # prot == "tcp"
        sock.listen(20)
        while True:
            t_print(f"Listener waiting for connection on {host}:{port}")
            conn, addr = sock.accept()
            t_print("Connected!")
            while True:
                message = conn.recv(4096)
                message = message.decode("utf-8")
                if message == "":
                    t_print(f"Got disconnect from {addr}")
                    sock.close()
                    break
                # t_print(addr, message)
                SEND_MESSAGE_PIPE.put(message, block=True)

    t_print(f"Listener {host}:{port}:{prot} shutting down")


def sender(port, prot, host="localhost"):
    # Receives from server-receive-queue, passes on messages to local port
    t_print(f"Sender spawning on {host}:{port}:{prot}")
    sock = getsocket(prot)
    if prot == "udp":
        while True:
            # get message
            message = RECV_MESSAGE_PIPE.get(block=True)
            sock.sendto(message.encode("utf-8"), (host, port))
    else: # prot == "tcp"
        socket_connected = False
        while True:
            if not socket_connected:
                sock.connect((host, port))
                t_print(f"Sender connected on {host}:{port}:{prot}")
                socket_connected = True
            
            # get message
            message = RECV_MESSAGE_PIPE.get(block=True)

            try:
                sock.send(message.encode('utf-8'))
            except socket.error as e:
                socket_connected = False
                t_print(f"Socket error on {host}:{port}:{prot}\n{e}")


    t_print(f"Sender {host}:{port}:{prot} shutting down")


def srv_sender(sock, host, port):
    global SERVER_CONNECTED
    global LOCAL_MESSAGES_SENT
    sock.settimeout(60)
    while True:
        message = SEND_MESSAGE_PIPE.get(block=True)
        message = "PD" + message
        try:
            send_message(sock, host, port, message)
            LOCAL_MESSAGES_SENT = LOCAL_MESSAGES_SENT + 1
        except:
            t_print("Sender connection with server is dead, attempting to reconnect...")
            SERVER_CONNECTED = False
            break
            # TODO: actually attempt a reconnect


def srv_receiver(sock, host, port):
    global SERVER_CONNECTED
    global LOCAL_MESSAGES_SENT
    sock.settimeout(60)
    while True:
        try:
            message = receive_message(sock, (host, port))
        except Exception as e:
            t_print("Receiver connection with server is dead, attempting to reconnect...")
            t_print(e)
            # break
            # TODO: actually attempt a reconnect
        if message == "":
            # Quit
            t_print(f"{MY_IDs} ending connection with '{host}:{port}'")
            sock.close()
            break
        elif message == "HB":
            # Hearbeat message, don't disconnect but don't do anything
            t_print(f"{MY_IDs} got heartbeat from '{host}:{port}'")
            t_print(f"{MY_IDs} has sent: {LOCAL_MESSAGES_SENT} messages")
        elif message[0:2] == "PD":
            # PD MESSAGE
            RECV_MESSAGE_PIPE.put(message[2:], block=True)
            t_print(f"{MY_IDs} got '{message}' from '{host}:{port}'")


def srv_heartbeat(sock, host, port):
    global SERVER_CONNECTED
    sock.settimeout(10)
    message = "HB"
    while True:
        if not SERVER_CONNECTED:
            break
        try:
            send_message(sock, host, port, message)
        except Exception as e:
            t_print(f"Heartbeat from {MY_IDs} got error: {e}")
        time.sleep(20)

    t_print(f"Heartbeat for {MY_IDs} shutting down...")



##### START #####
# FIFO unbounded queue
SEND_MESSAGE_PIPE = queue.Queue(0)
RECV_MESSAGE_PIPE = queue.Queue(0)

SERVER_IP = ""
SERVER_PORT = 32710
SERVER_CONNECTED = False
MY_ID = ""

LOCAL_SEND_PORT = 22711
LOCAL_LISTEN_PORT = 22710
LOCAL_PROTOCOL = "udp"

LOCAL_MESSAGES_SENT = 0

SERVER_SENDER = None
SERVER_RECEIVER = None

threads = []

# Generate uniqueID
MY_ID = uuid.uuid1()
MY_IDx = MY_ID.hex
MY_IDs = MY_IDx[0:8]

print(f"CLIENT: {MY_IDs} spooling up!")
# Get server IP
SERVER_IP = input("Input server IP:\n")


# Get server port
if SERVER_PORT == -1:
    # Only if we need to
    while True:
        try: 
            SERVER_PORT = input("Input server PORT:\n")
            SERVER_PORT = int(SERVER_PORT)
            if SERVER_PORT > 65535 or SERVER_PORT < 8000:
                raise ValueError
            break
        except ValueError as e:
            print("Server port must be integer in range [8000 - 65535]")


# Connecting to server
print(f"Testing connection to {SERVER_IP}:{SERVER_PORT}")

sock = getsocket("tcp")
sock.connect((SERVER_IP, SERVER_PORT))

message = ""
while message != "Hello!":
    message = sock.recv(4096)
    message = message.decode("utf-8")
    print(f"got {message} from server")

# Got Hello!, respond with uid
send_message(sock, SERVER_IP, SERVER_PORT, f"ID-{MY_IDs}")

message = ""
while message != f"Ack-{MY_IDs}":
    message = sock.recv(4096)
    message = message.decode("utf-8")
    print(f"got {message} from server")

SERVER_SENDER = threading._start_new_thread(srv_sender, (sock, SERVER_IP, SERVER_PORT))
SERVER_RECEIVER = threading._start_new_thread(srv_receiver, (sock, SERVER_IP, SERVER_PORT))
SERVER_CONNECTED = True
SERVER_RECEIVER = threading._start_new_thread(srv_heartbeat, (sock, SERVER_IP, SERVER_PORT))

print("Connection to server successful")

print(f"Spawning default sender on {LOCAL_SEND_PORT}:{LOCAL_PROTOCOL}")
# Spawn preconfigured sender
threads.append(threading._start_new_thread(sender, (LOCAL_SEND_PORT, LOCAL_PROTOCOL)))
# and listener
print(f"Spawning default listener on {LOCAL_LISTEN_PORT}:{LOCAL_PROTOCOL}")
threads.append(threading._start_new_thread(listener, (LOCAL_LISTEN_PORT, LOCAL_PROTOCOL)))


# Spawn listener or sender
while True:
    while True:
        try:
            spawn_item = input("Spawn new PD Listener / Sender? [y/n/QUIT]\n")
            if spawn_item == "y":
                break
            elif spawn_item == "QUIT":
                print("Exiting...")
                sys.exit(0)
            else:
                raise ValueError
        except KeyboardInterrupt:
            print("Exiting...")
            sys.exit(0)
        except ValueError as e:
            print("please respond with 'y' or 'n'")

    while True:
        spawn_item = input("Spawn [l]istener / [s]ender?\n")
        if spawn_item == "l":
            # Spawn new listener
            print("Spawning Listener")
            break
        elif spawn_item == "s":
            # Spawn new sender
            print("Spawning Sender")
            break
        else:
            print("please respond with 'l' or 's'")

    if spawn_item == "l" or spawn_item == "s":
        # Get sender/listener port
        while True:
            try:
                item_port = int(input("Port to use?\n"))
                if item_port < 1 or item_port > 65535:
                    raise ValueError
                else:
                    break
            except ValueError as e:
                print("Port must be integer in range [1 - 65535]")
        
        # Get sender/listener protocol
        while True:
            item_protocol = input("Protocol to use? [tcp/udp]\n")
            if item_protocol != "tcp" and item_protocol != "udp":
                print("Please enter one of 'tcp' or 'udp'")
            else:
                break
        
        if spawn_item == "l":
            item_target = listener
        else: # spawn_item == "s"
            item_target = sender

        threads.append(threading._start_new_thread(item_target, (item_port, item_protocol)))