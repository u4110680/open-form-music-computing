// import * as jsyaml from "./js-yaml.min.js";
// import * as ajv7 from "./ajv7.min.js";
import { sample } from "./utils.js";
import { ServerControl, ServerControlType } from "./messages.js";
export function loadYaml(data) {
    return jsyaml.load(data);
}
export function validateYaml(dataString, schemaString) {
    let schema = loadYaml(schemaString);
    let data = loadYaml(dataString);
    const AJV = new ajv7();
    const schemaValidator = AJV.compile(schema);
    return schemaValidator(data);
}
function durationToSeconds(durationString, delimiter) {
    if (durationString == undefined || durationString.length == 0) {
        return undefined;
    }
    if (delimiter == undefined) {
        delimiter = ":";
    }
    let durationSplit = durationString.split(delimiter);
    if (durationSplit.length > 4) {
        return undefined;
    }
    let duration = 0;
    const durationFactors = [1, 60, 60 * 60, 24 * 60 * 60];
    for (let i = 0; i < durationSplit.length; i++) {
        // Start with last element (seconds) and go from there
        const timeValueString = durationSplit[(durationSplit.length - 1) - i];
        const timeValue = Number.parseInt(timeValueString);
        if (Number.isNaN(timeValue)) {
            return undefined;
        }
        duration = duration + (timeValue * durationFactors[i]);
    }
    return duration;
}
export class CompositionManager {
    constructor(compositionDefinition, messageLog, logger) {
        // compositionDefinition data should already be validated
        this.state = "dead";
        this.valid = false;
        this.playerMap = new Map();
        this.disconnectTimeouts = [];
        this.possibleDelayValues = [0, 0.5, 1, 3, 5, 10];
        this.title = compositionDefinition["metadata"]["title"];
        this.composer = compositionDefinition["metadata"]["composer"];
        this.creationDate = compositionDefinition["metadata"]["date"];
        if (compositionDefinition["player-data"].hasOwnProperty("number-of-players")) {
            this.numberOfPlayers = compositionDefinition["player-data"]["number-of-players"];
        }
        else {
            this.numberOfPlayers = 0;
        }
        if (compositionDefinition["player-data"].hasOwnProperty("named-players")) {
            this.namedPlayers = compositionDefinition["player-data"]["named-players"];
        }
        else {
            this.namedPlayers = [];
        }
        if (compositionDefinition["player-data"].hasOwnProperty("other-players")) {
            this.otherPlayers = compositionDefinition["player-data"]["other-players"];
        }
        else {
            this.otherPlayers = [];
        }
        this.composition = compositionDefinition["composition"];
        for (const compositionElement of this.composition) {
            const startSeconds = durationToSeconds(compositionElement["time"]);
            compositionElement["timeString"] = compositionElement["time"];
            compositionElement["time"] = startSeconds;
            if (compositionElement.hasOwnProperty("duration")) {
                const durationSeconds = durationToSeconds(compositionElement["duration"]);
                compositionElement["durationString"] = compositionElement["duration"];
                compositionElement["duration"] = durationSeconds;
            }
            else {
                // Default
                compositionElement["durationString"] = "00:00";
                compositionElement["duration"] = 0;
            }
        }
        this.composition.sort((x, y) => {
            return x["time"] - y["time"];
        });
        this.logger = logger;
        this.messageLog = messageLog;
    }
    loadAndValidateSession(userList, pathList) {
        // Re-init values
        this.playerMap = new Map();
        this.valid = false;
        if (userList.length < this.numberOfPlayers || userList.length < this.namedPlayers.length) {
            this.logger.debug("CompositionMagager", "load failed: not enough named players");
            return false;
        }
        this.avaliablePlayers = userList;
        let usedPlayers = [];
        // Assign players with distinct id
        for (const namedPlayer of this.namedPlayers) {
            if (namedPlayer.hasOwnProperty("id")) {
                let matchingPlayer = false;
                for (const player of userList) {
                    if (player.split("#")[0] == namedPlayer["id"]) {
                        // Found a matching player, update the player map
                        matchingPlayer = true;
                        this.playerMap.set(namedPlayer["name"], player);
                        usedPlayers.push(player);
                        break;
                    }
                }
                if (!matchingPlayer) {
                    this.logger.debug("CompositionMagager", "load failed: no matching player for definition");
                    return false;
                }
            }
        }
        // Assign players without a distinct id
        for (const namedPlayer of this.namedPlayers) {
            if (!namedPlayer.hasOwnProperty("id")) {
                let matchingPlayer = false;
                for (const player of userList) {
                    if (!(usedPlayers.includes(player))) {
                        // Found an available player, update the player map
                        matchingPlayer = true;
                        this.playerMap.set(namedPlayer["name"], player);
                        usedPlayers.push(player);
                        break;
                    }
                }
                if (!matchingPlayer) {
                    this.logger.debug("CompositionMagager", "load failed: no matching player for definition");
                    return false;
                }
            }
        }
        // Check all players listed in not and only other lists are valid
        for (const otherPlayerData of this.otherPlayers) {
            let key;
            if (otherPlayerData.hasOwnProperty("not")) {
                key = "not";
            }
            else if (otherPlayerData.hasOwnProperty("only")) {
                key = "only";
            }
            else {
                this.logger.debug("CompositionMagager", `load failed: unknown key for other player entry: ${otherPlayerData}`);
                return false;
            }
            for (const namedPlayer of otherPlayerData[key]) {
                if (!this.playerMap.has(namedPlayer)) {
                    // Listed named player doesn't exist
                    this.logger.debug("CompositionMagager", `load failed: listed named player doesn't exist: ${namedPlayer}`);
                    return false;
                }
            }
        }
        for (const composition of this.composition) {
            for (const connection of composition["connections"]) {
                const pathExists = ((connection["source-path"] == "RANDOM" || pathList.includes(connection["source-path"])) &&
                    (connection["dest-path"] == "RANDOM" || pathList.includes(connection["dest-path"])));
                if (!pathExists) {
                    this.logger.debug("CompositionMagager", `load failed: no matching valid path found for: ${connection}`);
                    return false;
                }
            }
        }
        this.availablePaths = pathList;
        this.valid = true;
        this.state = "loaded";
        return true;
    }
    getPlayer(playerId) {
        if (playerId == undefined) {
            return undefined;
        }
        // Named?
        if (this.playerMap.has(playerId)) {
            return this.playerMap.get(playerId);
        }
        // Not named?
        for (const otherPlayer of this.otherPlayers) {
            if (otherPlayer["name"] == playerId) {
                if (otherPlayer.hasOwnProperty("only")) {
                    let selectedPlayerId = sample(otherPlayer["only"]);
                    return this.playerMap.get(selectedPlayerId);
                }
                else if (otherPlayer.hasOwnProperty("not")) {
                    let possiblePlayers = JSON.parse(JSON.stringify(this.avaliablePlayers)); // Deep copy
                    for (const invalidPlayer of otherPlayer["not"]) {
                        const removeIndex = possiblePlayers.indexOf(this.playerMap.get(invalidPlayer));
                        if (removeIndex >= 0) {
                            possiblePlayers = possiblePlayers.splice(removeIndex, 1);
                        }
                    }
                    return sample(possiblePlayers);
                }
            }
        }
        // Should have already returned
        return undefined;
    }
    getPath(path) {
        if (path == undefined) {
            return undefined;
        }
        if (path == "RANDOM") {
            return sample(this.availablePaths);
        }
        else {
            return path;
        }
    }
    getInverse(inverse) {
        if (inverse == undefined) {
            return undefined;
        }
        if (inverse == "RANDOM") {
            return sample(["normal", "inverse"]);
        }
        else if (inverse == "true" || inverse == true) {
            return "inverse";
        }
        else {
            return "normal";
        }
    }
    getDelay(delayValue) {
        if (delayValue == undefined) {
            return undefined;
        }
        if (delayValue == "RANDOM") {
            return sample(this.possibleDelayValues);
        }
        if (typeof (delayValue) == "number") {
            return delayValue;
        }
        else {
            return Number.parseFloat(delayValue);
        }
    }
    setNextPlayback(nextIndex, messageHandlingFunction, currentTime) {
        if (nextIndex < this.composition.length) {
            // Not at the end of the composition yet
            const ne = this.composition[nextIndex];
            const nextRunTime = this.startTime + (ne["time"] * 1000);
            this.playTimeout = {
                "timeout": setTimeout(() => { this.playCallback(nextIndex, messageHandlingFunction); }, nextRunTime - currentTime),
                "function": messageHandlingFunction,
                "runtime": nextRunTime,
                "index": nextIndex
            };
        }
    }
    async play(messageHandlingFunction) {
        if (!this.valid) {
            this.logger.debug("CompositionMagager", "play called before valid composition acquired");
            return;
        }
        if (this.state == "paused") {
            this.logger.debug("CompositionMagager", "resuming playback");
            this.messageLog("Resuming playback");
            this.resume();
        }
        else {
            this.logger.debug("CompositionMagager", "starting playback");
            this.messageLog("Starting playback");
            this.state = "playing";
            this.startTime = Date.now();
            this.setNextPlayback(0, messageHandlingFunction, this.startTime);
        }
    }
    playCallback(compositionIx, messageHandlingFunction) {
        const currentTime = Date.now();
        // Composition Element
        const ce = this.composition[compositionIx];
        // Execute element
        for (const conn of ce["connections"]) {
            const controlId = ServerControl.generateId();
            const sourceUser = this.getPlayer(conn["source"]);
            const sourcePath = this.getPath(conn["source-path"]);
            const destUser = this.getPlayer(conn["dest"]);
            const destPath = this.getPath(conn["dest-path"]);
            const inverse = this.getInverse(conn["invert"]);
            const messageDelay = this.getDelay(conn["delay"]);
            const scm = new ServerControl(controlId, ServerControlType.CONNECT, [
                sourceUser,
                sourcePath,
                destUser,
                destPath,
                inverse,
                messageDelay.toPrecision(2)
            ]);
            messageHandlingFunction(scm);
            if (ce.hasOwnProperty("duration") && ce["duration"] > 0) {
                const disconnectControlId = ServerControl.generateId();
                const dscm = new ServerControl(disconnectControlId, ServerControlType.DISCONNECT, [
                    sourceUser,
                    sourcePath,
                    destUser,
                    destPath,
                    inverse,
                    messageDelay.toPrecision(2)
                ]);
                // Message has a valid timeout
                const disconnectionFunction = () => { messageHandlingFunction(dscm); };
                this.disconnectTimeouts.push({
                    "timeout": setTimeout(disconnectionFunction, ce["duration"] * 1000),
                    "function": disconnectionFunction,
                    "runtime": Date.now() + (ce["duration"] * 1000)
                });
            }
        }
        // Set up next run
        this.setNextPlayback(compositionIx + 1, messageHandlingFunction, currentTime);
    }
    pause() {
        if (this.state == "paused") {
            this.logger.debug("CompositionManager", "pause called but already paused");
            return;
        }
        this.messageLog("Pausing playback");
        const currentTime = Date.now();
        this.pauseTime = currentTime;
        this.state = "paused";
        // Interrupt play thread
        clearTimeout(this.playTimeout["timeout"]);
        // Clear timeouts
        for (const disconnector of this.disconnectTimeouts) {
            if (disconnector["runtime"] > currentTime) {
                // Timeout still due to run
                clearTimeout(disconnector["timeout"]);
            }
        }
    }
    resume() {
        if (this.state != "paused") {
            this.logger.debug("CompositionManager", "resume called but not currently paused");
            return;
        }
        const currentTime = Date.now();
        // New Start Time (effective)
        this.startTime = currentTime - (this.pauseTime - this.startTime);
        // Rest play thread
        this.state = "playing";
        const ne = this.composition[this.playTimeout["index"]];
        const runTime = this.startTime + (ne["time"] * 1000);
        this.playTimeout["runtime"] = runTime;
        this.playTimeout["timeout"] = setTimeout(() => {
            this.playCallback(this.playTimeout["index"], this.playTimeout["function"]);
        }, runTime - currentTime);
        // Restart timeouts
        for (const disconnector of this.disconnectTimeouts) {
            if (disconnector["runtime"] > this.pauseTime) {
                // Timeout still due to run
                disconnector["timeout"] = setTimeout(disconnector["function"], disconnector["runtime"] - this.pauseTime);
                disconnector["runtime"] = (disconnector["runtime"] - this.pauseTime) + currentTime;
            }
        }
    }
    reset() {
        if (!this.valid) {
            return;
        }
        this.messageLog("Resetting composition playback");
        this.state = "loaded";
        // Interrupt play thread
        if (this.playTimeout != undefined) {
            clearTimeout(this.playTimeout["timeout"]);
            this.playTimeout = undefined;
        }
        // Clear timeouts
        for (const disconnector of this.disconnectTimeouts) {
            clearTimeout(disconnector["timeout"]);
        }
        this.disconnectTimeouts = [];
        // TODO: Should this remove the rules added?
    }
    clear() {
        this.state = "dead";
        this.messageLog("Clearing composition data");
        // Interrupt play thread
        if (this.playTimeout != undefined) {
            clearTimeout(this.playTimeout["timeout"]);
            this.playTimeout = undefined;
        }
        // Clear timeouts
        for (const disconnector of this.disconnectTimeouts) {
            clearTimeout(disconnector["timeout"]);
        }
        this.disconnectTimeouts = [];
        this.playerMap = new Map();
        this.valid = false;
        this.availablePaths = [];
    }
}
