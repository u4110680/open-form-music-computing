var _a;
// import * as Nexus from "./NexusUI.js";
import { LOG, sample } from "./utils.js";
import { NC, getSocket } from "./network-controller.js";
import { CTType, ForwardRule, ServerControl, ServerControlResponse, ServerControlType } from "./messages.js";
import { CFG } from "./model-d-cfg.js";
import { CompositionManager, loadYaml, validateYaml } from "./composition-manager.js";
// ----- HTML ELEMENTS
const compositionTextArea = document.getElementById("compositionTextArea");
const compositionMetadata = document.getElementById('compositionMetadata');
const initSettingsDiv = document.getElementById('initSettings');
const initSettingsWrapperDiv = document.getElementById('initSettingsWrapper');
const initSettingsShowHideDiv = document.getElementById('initSettingsShowHide');
initSettingsShowHideDiv["show"] = true;
const statusDiv = document.getElementById("status");
const statusWrapperDiv = document.getElementById("statusWrapper");
const statusShowHideDiv = document.getElementById("statusShowHide");
statusShowHideDiv["show"] = false;
const responseDiv = document.getElementById("messageLog");
// ----- Server Setting Div
(_a = document.getElementById('configSubmit')) === null || _a === void 0 ? void 0 : _a.addEventListener('click', async () => {
    let host = document.getElementById('address').value;
    if (host == null || host == undefined || host == "") {
        host = document.getElementById('address').placeholder;
    }
    let port_str = document.getElementById('port').value;
    if (port_str == null || port_str == undefined || port_str == "") {
        port_str = document.getElementById('port').placeholder;
    }
    let name = document.getElementById('name').value;
    if (name == null || name == undefined || name.trim().length <= 0 || name.length > 10) {
        LOG.error("NetworkConfig", "name must be 1-10 characters");
        return;
    }
    let port = undefined;
    try {
        port = Number.parseInt(port_str);
        if (Number.isNaN(port) || port < 1 || port > 65535) {
            throw "Port Out Of Range (1 - 65535)";
        }
    }
    catch (err) {
        LOG.error("NetworkConfig", "port value error: " + err);
        return;
    }
    NC.init(host, port, name);
});
// ----- Status show/hide
statusShowHideDiv.addEventListener("click", async () => {
    statusShowHideDiv["show"] = !statusShowHideDiv["show"];
    if (statusShowHideDiv["show"]) {
        statusWrapperDiv.style.bottom = "0px";
        statusShowHideDiv.getElementsByTagName("span")[0].textContent = "HIDE";
    }
    else {
        statusWrapperDiv.style.bottom = "-110px";
        statusShowHideDiv.getElementsByTagName("span")[0].textContent = "SHOW";
    }
});
// ----- Init settings show/hide
initSettingsShowHideDiv.addEventListener("click", async () => {
    initSettingsShowHideDiv["show"] = !initSettingsShowHideDiv["show"];
    if (initSettingsShowHideDiv["show"]) {
        initSettingsWrapperDiv.style.top = "0px";
        initSettingsShowHideDiv.getElementsByTagName("span")[0].textContent = "HIDE";
    }
    else {
        initSettingsWrapperDiv.style.top = "-140px";
        initSettingsShowHideDiv.getElementsByTagName("span")[0].textContent = "SHOW";
    }
});
let dialPathsAvailable = undefined;
let dialPathsUnavailable = undefined;
let togglePathsAvailable = undefined;
function buildPaths(paths) {
    if (dialPathsAvailable == undefined) {
        dialPathsAvailable = [];
    }
    if (dialPathsUnavailable == undefined) {
        dialPathsUnavailable = [];
    }
    if (togglePathsAvailable == undefined) {
        togglePathsAvailable = [];
    }
    if (paths.length >= 1) {
        if (paths[0] == "No paths present") {
            return;
        }
    }
    for (const path of paths) {
        if (path.endsWith("onOff") || path.endsWith("type")) {
            // toggle
            togglePathsAvailable.push(path);
        }
        else if (path.endsWith("volume")) {
            // dial we probably don't want to mess with
            dialPathsUnavailable.push(path);
        }
        else {
            // dial
            dialPathsAvailable.push(path);
        }
    }
    LOG.debug("BuildPaths", `updating paths to ${paths.toString()}`);
}
let usersAvailable = undefined;
function buildUsers(users) {
    // Reset current rules
    usersAvailable = [];
    if (users == undefined || users.length == 0) {
        LOG.debug("BuildUsers", `users given is undefined or empty, skipping analysis`);
        return;
    }
    // Check if the server response contains no rules
    if (users[0] == "No users present") {
        LOG.debug("BuildUsers", `no users present`);
        return;
    }
    usersAvailable = users;
    LOG.debug("BuildUsers", `updating users to ${usersAvailable.toString()}`);
}
let currentRules = undefined;
function buildRules(rules) {
    // Reset current rules
    currentRules = [];
    if (rules == undefined || rules.length == 0) {
        LOG.debug("BuildRules", `rules given is undefined or empty, skipping analysis`);
        return;
    }
    // Check if the server response contains no rules
    if (rules[0] == "No rules present") {
        LOG.debug("BuildRules", `no rules present`);
        return;
    }
    // Parse rules
    for (const rule of rules) {
        let parsedRule = ForwardRule.parseRule(rule);
        if (parsedRule != undefined) {
            currentRules.push(parsedRule);
        }
        else {
            LOG.debug(`BuildRules`, `unable to parse ${rule}`);
        }
    }
    LOG.debug("BuildRules", `updating rules to ${rules.toString()}`);
}
// ----- Controller
let connectedToServer = false;
let configured = false;
let updater;
function writeToMessageLog(data) {
    responseDiv.innerHTML = `<p class="message">${data.replace(/\n/g, "<br/>")}</p>` + responseDiv.innerHTML;
}
async function setup() {
    NC.registerMessageHandler(CTType.SERVER_CONTROL_RESPONSE, (message) => {
        LOG.debug("ControllerMessageHandler", `received: ${message}`);
        let responseMessage = ServerControlResponse.parseResponse(message.content);
        // let updateScrollPosition: boolean = this.divLog.scrollHeight - this.divLog.scrollTop <= this.divLog.clientHeight + 1.1; // epsilon
        let request = undefined;
        let printResponse = undefined;
        if (waitingResponseMessages.has(responseMessage.id)) {
            request = waitingResponseMessages.get(responseMessage.id)[0];
            printResponse = waitingResponseMessages.get(responseMessage.id)[1];
            waitingResponseMessages.delete(responseMessage.id);
            LOG.debug("ControllerMessageHandler", `matching response for message ${request.toString()}`);
        }
        // responseDiv.innerHTML = `<p class="message">${responseMessage.response.join(" ")}</p>` + responseDiv.innerHTML;
        // Scroll to the new bottom if we were at the bottom before
        // if (updateScrollPosition) {
        //     this.divLog.scrollTop = this.divLog.scrollHeight;
        // }
        switch (responseMessage.type) {
            case ServerControlType.CONNECT:
                if (configured && printResponse) {
                    writeToMessageLog(responseMessage.response.join(" "));
                }
                break;
            case ServerControlType.DISCONNECT:
                if (configured && printResponse) {
                    writeToMessageLog(responseMessage.response.join(" "));
                }
                break;
            case ServerControlType.HELP:
                if (configured && printResponse) {
                    writeToMessageLog(`Help:\n  - ${responseMessage.response.join("\n  - ")}`);
                }
                break;
            case ServerControlType.PATHS:
                buildPaths(responseMessage.response);
                if (configured && printResponse) {
                    writeToMessageLog(`Paths:\n  - ${responseMessage.response.join("\n  - ")}`);
                }
                break;
            case ServerControlType.RULES:
                buildRules(responseMessage.response);
                if (configured && printResponse) {
                    writeToMessageLog(`Rules:\n  - ${responseMessage.response.join("\n  - ")}`);
                }
                break;
            case ServerControlType.USERS:
                buildUsers(responseMessage.response);
                if (configured && printResponse) {
                    writeToMessageLog(`Users:\n  - ${responseMessage.response.join("\n  - ")}`);
                }
                break;
        }
        if (!configured) {
            configured = (connectedToServer &&
                currentRules != undefined &&
                usersAvailable != undefined &&
                dialPathsAvailable != undefined &&
                togglePathsAvailable != undefined);
        }
    });
    NC.registerConnectionListener((connected) => {
        connectedToServer = connected;
        if (connected) {
            pathsUpdate(false);
            rulesUpdate(false);
            usersUpdate(false);
        }
        else {
            configured = false;
            dialPathsAvailable = undefined;
            togglePathsAvailable = undefined;
            currentRules = undefined;
            usersAvailable = undefined;
        }
    });
    updater = setInterval(() => {
        if (connectedToServer) {
            LOG.debug("Updater", "requesting update on current state from server");
            pathsUpdate(false);
            rulesUpdate(false);
            usersUpdate(false);
        }
    }, CFG.controlUpdateInterval);
    // Load the schema
    loadSchemaData();
}
// ----- RULES
function newRule(isUserTriggered) {
    if (!configured) {
        LOG.warn("NewRuleButton", "can't create new rules before configuration is complete");
    }
    else if (usersAvailable.length == 0) {
        LOG.warn("NewRuleButton", "can't create new rules when there are no users");
    }
    else {
        let control_id = ServerControl.generateId();
        let sourceUser = sample(usersAvailable);
        let sourcePath = sample(dialPathsAvailable);
        let destUser = sample(usersAvailable);
        let destPath = sample(dialPathsAvailable);
        let inverse = sample(["inverse", "normal"]);
        let message_delay = sample([0, 0, 0.5, 1, 3, 5]);
        let scm = new ServerControl(control_id, ServerControlType.CONNECT, [
            sourceUser,
            sourcePath,
            destUser,
            destPath,
            inverse,
            message_delay.toPrecision(2)
        ]);
        sendServerControlMessage(scm, isUserTriggered);
    }
}
const createRuleButton = Nexus.Add.Button("#buttonCreateRule", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
createRuleButton.on("change", async (buttonState) => {
    if (buttonState) {
        newRule(true);
    }
});
function deleteRule(isUserTriggered) {
    if (configured) {
        const rule = sample(currentRules);
        if (rule == undefined) {
            LOG.debug("RemoveRuleButton", "unable to remove rule as no rules present");
            return;
        }
        let control_id = ServerControl.generateId();
        let scm = new ServerControl(control_id, ServerControlType.DISCONNECT, [rule.id.toString()]);
        sendServerControlMessage(scm, isUserTriggered);
    }
    else {
        LOG.warn("deleteRuleButton", "can't delete rules before configuration is complete");
    }
}
const deleteRuleButton = Nexus.Add.Button("#buttonDeleteRule", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
deleteRuleButton.on("change", async (buttonState) => {
    if (buttonState) {
        deleteRule(true);
    }
});
function deleteAllRules(isUserTriggered) {
    if (configured) {
        for (const rule of currentRules) {
            if (rule == undefined) {
                LOG.debug("RemoveRuleButton", "unable to remove rule as no rules present");
                return;
            }
            let control_id = ServerControl.generateId();
            let scm = new ServerControl(control_id, ServerControlType.DISCONNECT, [rule.id.toString()]);
            sendServerControlMessage(scm, false);
        }
    }
    else {
        LOG.warn("deleteAllRulesButton", "can't delete rules before configuration is complete");
    }
}
const deleteAllRulesButton = Nexus.Add.Button("#buttonDeleteAllRules", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
deleteAllRulesButton.on("change", async (buttonState) => {
    if (buttonState) {
        deleteAllRules(true);
    }
});
function rulesUpdate(isUserTriggered) {
    let control_id = ServerControl.generateId();
    let scm = new ServerControl(control_id, ServerControlType.RULES);
    sendServerControlMessage(scm, isUserTriggered);
}
const rulesButton = Nexus.Add.Button("#buttonListRules", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
rulesButton.on("change", async (buttonState) => {
    if (buttonState) {
        rulesUpdate(true);
    }
});
// ----- COMPOSITION
let CM;
compositionTextArea.addEventListener("change", () => {
    const valid = validateYaml(compositionTextArea.value, schemaData);
    if (valid) {
        compositionTextArea.style.borderBottomColor = NC.connectedColour;
    }
    else {
        compositionTextArea.style.borderBottomColor = NC.connectionErrorColour;
    }
});
let schemaData;
function loadSchemaData() {
    // Load the schema
    let schemaURL = new URL("composition-schema.yml", window.location.href.split("index.html")[0]);
    return fetch(schemaURL)
        .then(response => response.text())
        .then((data) => {
        schemaData = data;
    });
}
function loadComposition(isUserTriggered) {
    if (schemaData == undefined) {
        loadSchemaData().then(() => loadComposition(isUserTriggered));
        return;
    }
    const compositionData = compositionTextArea.value;
    const compositionValid = validateYaml(compositionData, schemaData);
    if (compositionValid) {
        const compositionDefinition = loadYaml(compositionData);
        CM = new CompositionManager(compositionDefinition, writeToMessageLog, LOG);
        const sessionValid = CM.loadAndValidateSession(usersAvailable, dialPathsAvailable);
        if (sessionValid) {
            if (isUserTriggered) {
                writeToMessageLog("Composition loaded");
            }
            const title = compositionDefinition["metadata"]["title"];
            const composer = compositionDefinition["metadata"]["composer"];
            const date = compositionDefinition["metadata"]["date"];
            const titleSpan = document.createElement("span");
            titleSpan.innerText = `${title} - ${composer} - ${date}`;
            titleSpan.className = "standardText";
            compositionMetadata.innerHTML = "";
            compositionMetadata.appendChild(titleSpan);
        }
        else {
            if (isUserTriggered) {
                writeToMessageLog("Composition not valid");
            }
        }
    }
}
const loadCompositionButton = Nexus.Add.Button("#buttonLoadComposition", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
loadCompositionButton.on("change", async (buttonState) => {
    if (buttonState) {
        loadComposition(true);
    }
});
function startComposition(isUserTriggered) {
    if (CM != undefined && CM.valid) {
        CM.play((message) => sendServerControlMessage(message, isUserTriggered));
    }
}
const startCompositionButton = Nexus.Add.Button("#buttonStartComposition", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
startCompositionButton.on("change", async (buttonState) => {
    if (buttonState) {
        startComposition(true);
    }
});
function stopComposition(isUserTriggered) {
    if (CM != undefined && CM.valid) {
        CM.pause();
    }
}
const stopCompositionButton = Nexus.Add.Button("#buttonStopComposition", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
stopCompositionButton.on("change", async (buttonState) => {
    if (buttonState) {
        stopComposition(true);
    }
});
function resetComposition(isUserTriggered) {
    if (CM != undefined && CM.valid) {
        CM.reset();
    }
}
const resetCompositionButton = Nexus.Add.Button("#buttonResetComposition", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
resetCompositionButton.on("change", async (buttonState) => {
    if (buttonState) {
        resetComposition(true);
    }
});
function clearComposition(isUserTriggered) {
    LOG.debug("clearComposition", "clearing composition text area");
    compositionTextArea.value = "";
    compositionMetadata.innerHTML = "";
    if (CM != undefined && CM.valid) {
        CM.clear();
    }
}
const clearCompositionButton = Nexus.Add.Button("#buttonClearComposition", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
clearCompositionButton.on("change", async (buttonState) => {
    if (buttonState) {
        clearComposition(true);
    }
});
// ----- HELP
function usersUpdate(isUserTriggered) {
    let control_id = ServerControl.generateId();
    let scm = new ServerControl(control_id, ServerControlType.USERS);
    sendServerControlMessage(scm, isUserTriggered);
}
const usersButton = Nexus.Add.Button("#buttonUsers", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
usersButton.on("change", async (buttonState) => {
    if (buttonState) {
        usersUpdate(true);
    }
});
function pathsUpdate(isUserTriggered) {
    let control_id = ServerControl.generateId();
    let scm = new ServerControl(control_id, ServerControlType.PATHS);
    sendServerControlMessage(scm, isUserTriggered);
}
const pathsButton = Nexus.Add.Button("#buttonPaths", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
pathsButton.on("change", async (buttonState) => {
    if (buttonState) {
        pathsUpdate(true);
    }
});
function helpMessage(isUserTriggered) {
    let control_id = ServerControl.generateId();
    let scm = new ServerControl(control_id, ServerControlType.HELP);
    sendServerControlMessage(scm, isUserTriggered);
}
const helpButton = Nexus.Add.Button("#buttonCommands", {
    "size": [CFG.buttonWidth, CFG.buttonHeight],
    "mode": "button",
});
helpButton.on("change", async (buttonState) => {
    if (buttonState) {
        helpMessage(true);
    }
});
// ----- MESSAGE HANDLING
let waitingResponseMessages = new Map();
function sendServerControlMessage(message, printResponse) {
    if (printResponse == undefined) {
        printResponse = true;
    }
    waitingResponseMessages.set(message.id, [message, printResponse]);
    NC.sendServerControl(message);
}
setup();
// Export things to the browser to be able to debug in console
const MODULEEXPORTS = { LOG, NC, initSettingsWrapperDiv, getSocket };
// @ts-ignore
window.mod = MODULEEXPORTS;
