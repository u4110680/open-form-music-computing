import { LOG, rightSplit } from "./utils.js";
// Message functions
export var CTType;
(function (CTType) {
    CTType["NEW_CONNECTION"] = "NEW_CONNECTION";
    CTType["END_CONNECTION"] = "END_CONNECTION";
    CTType["NEW_PLAYER"] = "NEW_PLAYER";
    CTType["REMOVE_PLAYER"] = "REMOVE_PLAYER";
    CTType["INPUT_CONTROL"] = "INPUT_CONTROL";
    CTType["SERVER_CONTROL"] = "SERVER_CONTROL";
    CTType["SERVER_CONTROL_RESPONSE"] = "SERVER_CONTROL_RESPONSE";
    CTType["GLOBAL_MESSAGE"] = "GLOBAL_MESSAGE";
    CTType["PRIVATE_MESSAGE"] = "PRIVATE_MESSAGE";
    CTType["HEARTBEAT"] = "HEARTBEAT";
    CTType["OTHER"] = "OTHER";
})(CTType || (CTType = {}));
export class CTMessage {
    constructor(sender, type, content) {
        this.sender = sender;
        this.type = type;
        this.content = content;
    }
    construct_message() {
        let ret = `${CTMessage._MESSAGE_START}${this.sender}${CTMessage._MESSAGE_SEPARATOR}${this.type.toString()}`;
        if (this.content != null) {
            ret += `${CTMessage._MESSAGE_SEPARATOR}${this.content}`;
        }
        ret += `${CTMessage._MESSAGE_END}`;
        return ret;
    }
    static parse_message(raw_message) {
        let sender;
        let messageType;
        let content = undefined;
        let ret = undefined;
        if (raw_message.startsWith(CTMessage._MESSAGE_START) &&
            raw_message.endsWith(CTMessage._MESSAGE_END)) {
            try {
                let temp_string = raw_message.substring(CTMessage._MESSAGE_START.length);
                temp_string = temp_string.substring(0, temp_string.length - CTMessage._MESSAGE_END.length);
                let entries = temp_string.split(CTMessage._MESSAGE_SEPARATOR);
                if (entries.length < 2) {
                    throw new Error("Message does not contain at least sender and type.");
                }
                sender = entries[0];
                messageType = entries[1];
                if (entries.length > 2) {
                    content = entries[2];
                }
                ret = new CTMessage(sender, messageType, content);
            }
            catch (err) {
                console.error(`PARSER: error parsing message '${raw_message}' - ${err}`);
            }
        }
        else {
            console.warn(`PARSER: message '${raw_message}' does not conform to CTMessage standard`);
        }
        return ret;
    }
}
CTMessage._MESSAGE_START = "[[";
CTMessage._MESSAGE_SEPARATOR = "%";
CTMessage._MESSAGE_END = "]]";
CTMessage._INPUT_CONTROL_SEPARATOR = "/";
CTMessage._SERVER_CONTROL_SEPARATOR = "|";
export var ServerControlType;
(function (ServerControlType) {
    ServerControlType["HELP"] = "help";
    ServerControlType["USERS"] = "users";
    ServerControlType["PATHS"] = "paths";
    ServerControlType["RULES"] = "rules";
    ServerControlType["CONNECT"] = "connect";
    ServerControlType["DISCONNECT"] = "disconnect";
})(ServerControlType || (ServerControlType = {}));
export class ServerControl {
    constructor(/*sender: string, */ control_id, control_type, command_args) {
        // this.sender = sender;
        this.id = control_id;
        this.control_type = control_type;
        if (command_args == undefined) {
            this.command_args = [];
        }
        else {
            this.command_args = command_args;
        }
    }
    toMessageString() {
        let messageString = this.id + CTMessage._SERVER_CONTROL_SEPARATOR + this.control_type.toString();
        for (let i = 0; i < this.command_args.length; i++) {
            messageString += CTMessage._SERVER_CONTROL_SEPARATOR + this.command_args[i];
        }
        return messageString;
    }
    toString() {
        return `ServerControl(${this.id}, ${this.control_type}, ` + this.command_args.join(", ") + ")";
    }
    static generateId() {
        let chars = "";
        for (let i = 0; i < 4; i++) {
            chars += String.fromCharCode(65 + Math.floor(Math.random() * 26));
        }
        const time = Date.now().toString();
        return `${chars}${time.slice(time.length - 4)}`;
    }
}
export class ServerControlResponse {
    constructor(control_id, type, response) {
        this.id = control_id;
        this.type = type;
        this.response = response;
    }
    static parseResponse(responseString) {
        const elements = responseString.split(CTMessage._SERVER_CONTROL_SEPARATOR);
        if (elements.length < 3) {
            LOG.error("ServerControlResponse", `unable to parse data from message ${responseString}`);
            return undefined;
        }
        const id = elements[0];
        let parsedType;
        const typeString = elements[1];
        if (Object.values(ServerControlType).some((ict) => ict === typeString)) {
            parsedType = typeString;
        }
        else {
            LOG.error("ServerControlResponse", `received a server control response message with unknown type string: ${typeString}, full message: ${responseString}`);
            return undefined;
        }
        const responseData = elements.slice(2);
        return new ServerControlResponse(id, parsedType, responseData);
    }
}
export var ControlState;
(function (ControlState) {
    ControlState["DISABLED"] = "disabled";
    ControlState["DISCONNECTED"] = "disconnected";
    ControlState["LOCAL"] = "local";
    ControlState["NETWORK"] = "network";
    ControlState["BOTH"] = "both";
})(ControlState || (ControlState = {}));
/**
 * A specific type of CTMessage, the subtypes of this message are:
 * - VALUE: An update to the value of the given component from the server
 * - STATE: An update to the state of the given component from the server
 * - REQUEST_VALUE: A request for the current value of the component to be sent to the server
 * - REQUEST_STATE: A request for the current state of the component to be sent to the server
 */
export var InputControlType;
(function (InputControlType) {
    InputControlType["VALUE"] = "value";
    InputControlType["STATE"] = "state";
    InputControlType["REQUEST_VALUE"] = "request_value";
    InputControlType["REQUEST_STATE"] = "request_state";
})(InputControlType || (InputControlType = {}));
/**
 * An InputControl message has the form:
 *
 * `/path/to/component/InputControlType/value`
 *
 * where:
 *
 * - `/path/to/component` is a variable length path to the given component
 * - `/InputControlType` is the kind of InputControl message this is (second to last element)
 * - `/value` is the accompanying data transmitted with the InputControlType (last element, may be blank in instances of a request)
 */
export class InputControl {
    constructor(path, type, value) {
        this.path = path;
        this.type = type;
        if (Object.values(ControlState).some((cs) => cs === value)) {
            this.state = value;
        }
        else {
            this.value = value;
        }
    }
    static constructValueMessage(path, value) {
        return `${path}/${InputControlType.VALUE}/${value}`;
    }
    static constructStateMessage(path, state) {
        return `${path}/${InputControlType.STATE}/${state}`;
    }
    constructMessage() {
        if (this.value == undefined) {
            return InputControl.constructStateMessage(this.path, this.state);
        }
        else {
            return InputControl.constructValueMessage(this.path, this.value);
        }
    }
    static parseValue(rawValue) {
        if (rawValue.toLowerCase() === "true") {
            return true;
        }
        else if (rawValue.toLowerCase() === "false") {
            return false;
        }
        else if (rawValue.lastIndexOf(".") < 0) {
            return Number.parseInt(rawValue);
        }
        else {
            return Number.parseFloat(rawValue);
        }
    }
    static parseState(rawValue) {
        if (Object.values(ControlState).some((cs) => cs === rawValue)) {
            return rawValue;
        }
        else {
            LOG.error("InputControl", `unable to parse state: ${rawValue}`);
            return undefined;
        }
    }
    static parseMessage(message) {
        if (message == null || message == undefined) {
            LOG.warn("InputControl", "asked to parse a null / undefined message");
            return undefined;
        }
        if (message.type != CTType.INPUT_CONTROL) {
            LOG.warn("InputControl", "asked to parse a non-input control type message");
            return undefined;
        }
        if (message.content == null || message.content == undefined || message.content.length == 0) {
            LOG.warn("InputControl", "received an input control message with no content");
            return undefined;
        }
        const elements = rightSplit(message.content, CTMessage._INPUT_CONTROL_SEPARATOR, 2);
        if (elements.length <= 2) {
            LOG.warn("InputControl", `received an input control message with poorly formed content: ${message.content}`);
            return undefined;
        }
        const path = elements[0];
        const typeString = elements[1];
        let value;
        let type;
        if (Object.values(InputControlType).some((ict) => ict === typeString)) {
            type = typeString;
        }
        else {
            LOG.error("InputControl", `received an input control message with unknown type string: ${typeString}, full message: ${message.content}`);
            return undefined;
        }
        if (type == InputControlType.STATE) {
            value = InputControl.parseState(elements[2]);
        }
        else if (type == InputControlType.VALUE) {
            value = InputControl.parseValue(elements[2]);
        }
        else {
            // The other two types don't care about this field 
        }
        return new InputControl(path, type, value);
    }
}
export class ForwardRule {
    constructor(ruleId, sourceName, sourcePath, destName, destPath, inverse, messageDelay) {
        this.id = ruleId;
        this.sourceName = sourceName;
        this.sourcePath = sourcePath;
        this.destName = destName;
        this.destPath = destPath;
        if (inverse == undefined) {
            inverse = false;
        }
        else {
            this.inverse = inverse;
        }
        if (messageDelay == undefined) {
            messageDelay = 0;
        }
        else {
            this.messageDelay = messageDelay;
        }
    }
    /**
     * Checks whether the given rule is equivalent to the current rule.
     * @param rule the given rule
     * @returns True if the rules share the same source and destination, false otherwise
     */
    matches(rule) {
        return (this.sourceName == rule.sourceName &&
            this.sourcePath == rule.sourcePath &&
            this.destName == rule.destName &&
            this.destPath == rule.destPath);
    }
    static parseRule(ruleString) {
        let parts;
        if (ruleString.startsWith(ForwardRule.stringStart) && ruleString.endsWith(ForwardRule.stringEnd)) {
            parts = ruleString.substring(ForwardRule.stringStart.length, ruleString.length - ForwardRule.stringEnd.length).split(ForwardRule.separator);
        }
        if (parts.length < 5) {
            LOG.warn("ForwardRule", `unable to parse ${ruleString}, not enough sections`);
            return undefined;
        }
        let parsedId;
        try {
            parsedId = Number.parseInt(parts[0]);
        }
        catch (error) {
            LOG.warn("ForwardRule", `unable to parse ${ruleString}, Id not integer`);
            return undefined;
        }
        let parsedInverse = false;
        if (parts.length >= 6) {
            // Includes an inverse
            if (parts[5] == "normal") {
                parsedInverse = false;
            }
            else if (parts[5] == "inverse") {
                parsedInverse = true;
            }
            else {
                LOG.warn("ForwardRule", `unable to parse ${ruleString}, ${parts[5]} is not of 'normal' or 'inverse'`);
                return undefined;
            }
        }
        let parsedDelay = 0;
        if (parts.length >= 7) {
            try {
                parsedDelay = Number.parseFloat(parts[6]);
            }
            catch (error) {
                LOG.warn("ForwardRule", `unable to parse ${ruleString}, message delay not float`);
                return undefined;
            }
        }
        return new ForwardRule(parsedId, parts[1], parts[2], parts[3], parts[4], parsedInverse, parsedDelay);
    }
    toString() {
        return `ForwardRule(${this.id}, ${this.sourceName}, ${this.sourcePath}, ${this.destName}, ${this.destPath}, ${this.inverse ? 'inverse' : 'normal'}, ${this.messageDelay})`;
    }
}
ForwardRule.stringStart = "ForwardRule(";
ForwardRule.stringEnd = ")";
ForwardRule.separator = ", ";
