class CONFIG_PARAMETERS {
    constructor() {
        this.blockWidth = 20;
        this.blockHeight = 20;
        this.buttonWidth = 3 * this.blockWidth;
        this.buttonHeight = 3 * this.blockHeight;
        this.heartbeatInterval = 5000; // 5s
        this.controlUpdateInterval = 5000; // 5s
        this.randomIdNumber = false;
        this.id = this.randomIdNumber ? (Math.floor(Math.random() * (1999 - 1000 + 1)) + 1000).toString().substring(0, 4) : "1000";
        // DEBUG SUCCESS INFO WARNING ERROR
        this.logLevel = "WARNING";
    }
}
const CFG = new CONFIG_PARAMETERS();
export { CFG };
