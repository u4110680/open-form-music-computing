import { CFG } from "./model-d-cfg.js";

/**
 * Delay until a time has passed.
 * @param ms The time to delay until in ms since epoch
 * @returns A Promise that resolves after the time has passed
 */
export function sleepUntil(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms - Date.now()));
}

// Maps a value from one range to another
export function mapRange(value: number, inLow: number, inHigh: number, outLow: number, outHigh: number): number {
    return outLow + (outHigh - outLow) * (value - inLow) / (inHigh - inLow);
}

export function getFrequency(rootFrequency: number, semitoneDifference: number): number {
    return rootFrequency * (2 ** (semitoneDifference / 12));
}

export function rightSplit(input: string, delimiter: string, limit: number = 0): string[] {
    if (limit <=0) {
        return input.split(delimiter);
    }
    let elements: string[] = [];
    let currentInput: string = input;
    while (limit > 0) {
        const lastIndex: number = currentInput.lastIndexOf(delimiter);
        if (lastIndex < 0) {
            // No more elements left in input string
            break;
        }
        // Add next element to list
        elements.unshift(currentInput.substring(lastIndex + delimiter.length));
        // Remove element from string
        currentInput = currentInput.substring(0, lastIndex);

        limit--;
    }
    elements.unshift(currentInput);
    return elements;
}

/**
 * Randomly samples an element from the given array
 */
export function sample<T>(arr: T[]): T {
    if (arr == undefined || arr.length == 0) {
        return undefined;
    }

    return arr[Math.floor(Math.random()*arr.length)];
}

export class PixelMaths {
    static add(a: string, b: string): string {
        let result: number = parseFloat(a.replace("px", "")) + parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }

    static sub(a: string, b: string): string {
        let result: number = parseFloat(a.replace("px", "")) - parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }

    static mul(a: string, b: string): string {
        let result: number = parseFloat(a.replace("px", "")) * parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }

    static div(a: string, b: string): string {
        let result: number = parseFloat(a.replace("px", "")) / parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }
}

export interface Dictionary<Value> {
    [key: string]: Value;
}

export enum LogLevel {
    DEBUG,
    SUCCESS,
    INFO,
    WARNING,
    ERROR,
}

const logLevelMap: Map<LogLevel, string> = new Map([
    [LogLevel.DEBUG, "DEBUG"],
    [LogLevel.SUCCESS, "SUCCESS"],
    [LogLevel.INFO, "INFO"],
    [LogLevel.WARNING, "WARNING"],
    [LogLevel.ERROR, "ERROR"],
]);

function logLevelString(level: LogLevel): string {
    if (logLevelMap.has(level)) {
        return logLevelMap.get(level);
    }
    return "UNKNOWN";
}

function stringLogLevel(levelString: string): LogLevel | undefined {
    for (const [key, value] of logLevelMap) {
        if (value === levelString) {
            return key;
        }
    }
    return undefined;
}

export class Logger {
    level: LogLevel;
    store: Array<string>;
    logToConsole: boolean;
    logToDiv: boolean;
    divLog: HTMLDivElement;

    constructor(level: LogLevel = LogLevel.DEBUG, logToConsole: boolean = true, logToDiv: boolean = true, divLog: HTMLDivElement = undefined) {
        this.level = level;
        this.store = new Array();
        this.logToConsole = logToConsole;
        this.logToDiv = logToDiv;
        this.divLog = divLog;
    }

    // %Y-%m-%d_%H-%M-%S
    private static dateTimeString(date: Date): string {
        let year: number = date.getFullYear();
        let month: string = date.getMonth().toString().length > 1 ? date.getMonth().toString() : "0" + date.getMonth().toString();
        let day: string = date.getDate().toString().length > 1 ? date.getDate().toString() : "0" + date.getDate().toString();
        let hour: string = date.getHours().toString().length > 1 ? date.getHours().toString() : "0" + date.getHours().toString();
        let minute: string = date.getMinutes().toString().length > 1 ? date.getMinutes().toString() : "0" + date.getMinutes().toString();
        let second: string = date.getSeconds().toString().length > 1 ? date.getSeconds().toString() : "0" + date.getSeconds().toString();
        let ret: string = `${year}-${month}-${day}_${hour}-${minute}-${second}`;
        return ret;
    }

    private writeLog(level: LogLevel, caller: string, data: string) {
        let logTime: Date = new Date();
        let timeString: string = Logger.dateTimeString(logTime);
        let logString = `${timeString} : ${level.toString()} : ${caller} : ${data}`;
        // Add to log store
        this.store.push(logString);
        // Only log if the level makes sense
        if (level >= this.level) {
            // Log to console
            if (this.logToConsole) {
                switch(level) {
                    case LogLevel.DEBUG:
                        // console.debug(logString);
                        console.log(`${timeString} %c${caller}`, "color: magenta", data);
                        break;
                    case LogLevel.SUCCESS:
                        // console.log(logString);
                        console.log(`${timeString} %c${caller}`, "color: green", data);
                        break;
                    case LogLevel.INFO:
                        // console.log(logString);
                        console.log(`${timeString} %c${caller}`, "color: blue", data);
                        break;
                    case LogLevel.WARNING:
                        // console.warn(logString);
                        console.log(`${timeString} %c${caller}`, "color: yellow", data);
                        break;
                    case LogLevel.ERROR:
                        // console.error(logString);
                        console.log(`${timeString} %c${caller}`, "color: red", data);
                        break;
                }
            }
            // Log to div
            if (this.logToDiv) {
                let updateScrollPosition: boolean = this.divLog.scrollHeight - this.divLog.scrollTop <= this.divLog.clientHeight + 1.1; // epsilon
                this.divLog.innerHTML += `<p class="logEntry">${timeString} : <span class="log${logLevelString(level)}">${caller}</span> : ${data}</p>`;
                // Scroll to the new bottom if we were at the bottom before
                if (updateScrollPosition) {
                    this.divLog.scrollTop = this.divLog.scrollHeight;
                }
            }
        }
    }

    debug(caller: string, data: string) {
        this.writeLog(LogLevel.DEBUG, caller, data);
    }

    success(caller: string, data: string) {
        this.writeLog(LogLevel.SUCCESS, caller, data);
    }

    info(caller: string, data: string) {
        this.writeLog(LogLevel.INFO, caller, data);
    }

    warn(caller: string, data: string) {
        this.writeLog(LogLevel.WARNING, caller, data);
    }

    error(caller: string, data: string) {
        this.writeLog(LogLevel.ERROR, caller, data);
    }
}

const statusDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById("status"));

export const LOG: Logger = new Logger(stringLogLevel(CFG.logLevel), true, true, statusDiv);