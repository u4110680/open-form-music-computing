class CONFIG_PARAMETERS {

    readonly blockWidth:  number = 20;
    readonly blockHeight: number = 20;

    readonly buttonWidth:  number = 3 * this.blockWidth;
    readonly buttonHeight: number = 3 * this.blockHeight;

    readonly heartbeatInterval: number = 5000; // 5s
    readonly controlUpdateInterval: number = 5000; // 5s

    readonly randomIdNumber: boolean = false;
    readonly id: string = this.randomIdNumber ? (Math.floor(Math.random()* (1999 - 1000 + 1)) + 1000).toString().substring(0, 4) : "1000";

    // DEBUG SUCCESS INFO WARNING ERROR
    readonly logLevel: string = "WARNING";

}

const CFG: CONFIG_PARAMETERS = new CONFIG_PARAMETERS();

export { CFG };