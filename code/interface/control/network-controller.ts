import { CFG } from "./model-d-cfg.js";
import { CTMessage, CTType, ServerControl, ServerControlResponse, ServerControlType } from "./messages.js";
import { LOG } from "./utils.js";


let messageHandlers: Map<CTType, ((x: CTMessage) => any)[]> = new Map();
let connectionListeners: ((connected: boolean) => any)[] = [];

// ----- Connection vars
let configured: boolean = false;
let connected:  boolean = false;

// controller has an id of 1000
// const id: string = "1000";
let name: string;

let host: string;
let port: number;

let socket: WebSocket;

function getSocket() {
    return socket;
}
export { getSocket };

let heartbeatInterval: number;

const initServerStatusField: HTMLInputElement = (<HTMLInputElement> document.getElementById('initServerStatus'));

// ----- Colours
const connectedColour: string = "rgb(68, 207, 68)";
const disconnectedColour: string = "rgb(104, 106, 109)";
const connectingColour: string = "rgb(205, 207, 68)";
const connectionErrorColour: string = "rgb(207, 68, 68)";

// ----- Connection Status Detection
let lastConnectionUpdate: number = -1;
let connectionChecker: number = setInterval(
    () => {
        if (socket == undefined) {
            // Init hasn't run yet
        } else if (socket.readyState == 1) {
            // Normal Operation
            if (lastConnectionUpdate != socket.readyState) {
                lastConnectionUpdate = socket.readyState;
            }
        } else if (socket.readyState == 3) {
            if (lastConnectionUpdate == 1) {
                lastConnectionUpdate = socket.readyState;
                initServerStatusField.value = "Connection Closed";
                initServerStatusField.style.backgroundColor = disconnectedColour;
            }
        }
    }, 
    1000
);

// ----- FUNCTIONS
function init(hostIn: string, portIn: number, nameIn: string): void {
    if (configured) {
        LOG.error("NetworkController", "already configured");
        return;
    }

    host = hostIn;
    port = portIn;
    name = `${nameIn}#${CFG.id}`;
    LOG.info("NetworkController", `${name} connecting to ${host}:${port}`);
    initServerStatusField.value = "Connecting...";
    initServerStatusField.style.backgroundColor = connectingColour;
    configured = true;
    socket = new WebSocket(`ws://${host}:${port}`);
    socket.onopen = onConnect;
    socket.onmessage = onMessage;
    socket.onerror = onError;
    socket.onclose = onClose;
}

async function onConnect() {
    LOG.success("NetworkController", `connected to ${host}:${port}`);
    initServerStatusField.value = "Connected!";
    initServerStatusField.style.backgroundColor = connectedColour;
    connected = true;
    configured = true;
    sendHello();
    heartbeatInterval = setInterval(sendHeartbeat, CFG.heartbeatInterval);
    
    for (const listener of connectionListeners) {
        listener(connected);
    }
}

async function onMessage(message: MessageEvent) {
    LOG.debug("NetworkController", `got message ${message.data}`);
    let parsedMessage = CTMessage.parse_message(message.data);
    processMessage(parsedMessage);
}

async function onClose(event: CloseEvent) {
    LOG.info("NetworkController", `disconnected from ${this.host}:${port}`);
    LOG.debug("NetworkController", `disconnection event: ${event}`);
    clearInterval(heartbeatInterval);
    if (event.wasClean) {
        initServerStatusField.value = "Connection Closed";
        initServerStatusField.style.backgroundColor = disconnectedColour;
    }
    connected = false;
    configured = false;

    for (const listener of connectionListeners) {
        listener(connected);
    }
}

async function onError(event: Event) {
    LOG.error("NetworkController", `error from websocket: ${event}`);
    initServerStatusField.value = "Connection Error";
    initServerStatusField.style.backgroundColor = connectionErrorColour;
    connected = false;
    configured = false;
    
    for (const listener of connectionListeners) {
        listener(connected);
    }
}

function sendHello() {
    LOG.info("NetworkController", "sending hello message");
    const helloMessage: CTMessage = new CTMessage(name, CTType.NEW_CONNECTION);
    sendCTMessage(helloMessage);
}

function sendHeartbeat() {
    LOG.info("NetworkController", "sending heartbeat");
    const heartbeatMessage = new CTMessage(name, CTType.HEARTBEAT);
    sendCTMessage(heartbeatMessage);
}

function sendServerControl(serverControlMessage: ServerControl) {
    LOG.info("NetworkController", `sending server control message ${serverControlMessage.toString()}`);
    const message = new CTMessage(name, CTType.SERVER_CONTROL, serverControlMessage.toMessageString());
    sendCTMessage(message);
}

function sendCTMessage(message: CTMessage) {
    if (connected) {
        const outMessage: string = message.construct_message();
        LOG.debug("NetworkController", `sending ${outMessage}`);
        socket.send(outMessage);
    }
}

function registerConnectionListener(handlerFunction: (connected: boolean) => any) {
    connectionListeners.push(handlerFunction);
}

function registerMessageHandler(typeToHandle: CTType, handlerFunction: (x: CTMessage) => any) {
    LOG.debug("NetworkController", `registering handler for type: ${typeToHandle}`);

    if (!(typeToHandle in messageHandlers.keys())) {
        messageHandlers.set(typeToHandle, new Array<(x: CTMessage) => any>());
    }
    
    messageHandlers.get(typeToHandle).push(handlerFunction);
}

function processMessage(message: CTMessage) {
    if (message == null) {
        // TODO: Maybe do something here who knows
        LOG.error("NetworkController", "processMessage was given a null message");
        return;
    }

    if (messageHandlers.has(message.type)) {
        LOG.debug("NetworkController", `handlers registered for type: ${messageHandlers.get(message.type).length}`);
        for (const handler of messageHandlers.get(message.type)) {
            handler(message);
        }
    } else {
        LOG.debug("NetworkController", `no handler registered for type: ${message.type}`);
    }

    switch(message.type) {
        case CTType.HEARTBEAT:
            // TODO: Do we want to do more with this?
            LOG.info("NetworkController", "received heartbeat message");
            break;
        case CTType.INPUT_CONTROL:
            LOG.warn("NetworkController", `controller received input control message: ${message.content}`);
            break;
        case CTType.SERVER_CONTROL_RESPONSE:
            LOG.info("NetworkController", "received server control response message");
            let responseMessage: ServerControlResponse = ServerControlResponse.parseResponse(message.content);
            switch (responseMessage.type) {
                case ServerControlType.CONNECT:
                    break;
                case ServerControlType.DISCONNECT:
                    break;
                case ServerControlType.HELP:
                    break;
                case ServerControlType.PATHS:
                    break;
                case ServerControlType.RULES:
                    break;
                case ServerControlType.USERS:
                    break;
            }
            break;
        default:
            console.warn(`PROCESS: no behaviour implemented for type ${message.type}`);
            break;
    }
}

export const NC = {init, sendCTMessage, sendServerControl, registerMessageHandler, registerConnectionListener, connectedColour, connectionErrorColour};
