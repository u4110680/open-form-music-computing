import * as Nexus from "./NexusUI.js";
import * as Tone from "./tone.js";

import { CFG } from "./model-d-cfg.js";
import { mapRange, PixelMaths, LOG } from "./utils.js"
import { ControlEndpointDial, ControlEndpointToggle, NC } from "./network-controller.js";

class Oscillator {
    onOff: ControlEndpointToggle;
    volume: ControlEndpointDial;
    waveform: Nexus.RadioButton;
    detune: undefined | ControlEndpointDial = undefined;

    constructor(onOff?: ControlEndpointToggle, volume?: ControlEndpointDial, waveform?: Nexus.RadioButton, detune?: ControlEndpointDial) {
        this.onOff = onOff;
        this.volume = volume;
        this.waveform = waveform;
        this.detune = detune;
    }
}

class NoiseControl {
    onOff: ControlEndpointToggle;
    type: ControlEndpointToggle;
    volume: ControlEndpointDial;

    constructor(onOff?: ControlEndpointToggle, type?: ControlEndpointToggle, volume?: ControlEndpointDial) {
        this.onOff = onOff;
        this.type = type;
        this.volume = volume;
    }
}

class FilterControl {
    cutoff: ControlEndpointDial;
    emphasis: ControlEndpointDial;

    constructor(cutoff?: ControlEndpointDial, emphasis?: ControlEndpointDial) {
        this.cutoff = cutoff;
        this.emphasis = emphasis;
    }
}

class EnvelopeControl {
    attack: ControlEndpointDial;
    decay: ControlEndpointDial;
    sustain: ControlEndpointDial;
    release: ControlEndpointDial;

    constructor(attack?: ControlEndpointDial, decay?: ControlEndpointDial, sustain?: ControlEndpointDial, release?: ControlEndpointDial) {
        this.attack = attack; 
        this.decay = decay; 
        this.sustain = sustain; 
        this.release = release; 
    }
}

class OutputControl {
    onOff: ControlEndpointToggle;
    volume: ControlEndpointDial;

    constructor(onOff?: ControlEndpointToggle, volume?: ControlEndpointDial) {
        this.onOff = onOff;
        this.volume = volume;
    }
}

class ModelDSynth {
    root: HTMLDivElement;
    rootID: string;
    numOsc: number;
    oscillators: Array<Oscillator> = [];

    noise: Tone.Noise;
    synth: Tone.DuoSynth;

    noiseControl: NoiseControl;
    filterControl: FilterControl;
    filterEnvelopeControl: EnvelopeControl;
    amplitudeEnvelopeControl: EnvelopeControl;
    outputControl: OutputControl;

    synthOk: boolean = false;
    constructor(parentDiv: HTMLDivElement, parentID: string, numberOfOscillators : number = 2) {
        this.root = parentDiv;
        this.rootID = parentID;
        this.numOsc = numberOfOscillators;
    }

    createSynth(): void {
        if (this.synthOk) {
            // Synth is already started?
            LOG.warn("createSynth", "asked to rebuild synth, but its already running...");
            return;
        }
        this.synthOk = true;

        this.synth = new Tone.DuoSynth({
            volume: -50,
            voice0: {
                volume: -50,
                oscillator: {
                    type: "triangle"
                },
                envelope: {
                    attack: 0.1
                }
            },
            voice1: {
                volume: -50,
                oscillator: {
                    type: "triangle"
                },
                envelope: {
                    attack: 0.1
                }
            }
        }).toDestination();

        this.noise = new Tone.Noise("pink");
        this.noise.connect(this.synth.voice0.filter);
        this.noise.connect(this.synth.voice1.filter);

        this.applySettings();
    }

    destroySynth(): void {
        if (!this.synthOk) {
            // Synth is already dead?
            LOG.warn("destroySynth", "asked to destroy synth, but its already dead...")
            return;
        }
        LOG.debug("destroySynth", "destroying synth")
        this.synthOk = false;
        this.synth.dispose();
        this.noise.dispose();
    }

    synthReady(caller: string = "Unspecified"): boolean {
        if (this.synthOk) {
            return true;
        } else {
            LOG.warn(caller, "asked for synth but it is not ready yet")
            return false;
        }
    }

    // Apply the current settings of all dials
    applySettings() {
        for (let i = 0; i < this.oscillators.length; i++) {
            this.oscillatorToggle(i);
            this.updateWaveform(i);
        }
        this.detuneOscillator();
        
        this.toggleNoise();
        this.toggleNoiseType();
        this.noiseVolume();

        this.filterCutoff();
        this.filterEmphasis();

        this.filterEnvelopeAttack();
        this.filterEnvelopeDecay();
        this.filterEnvelopeSustain();
        this.filterEnvelopeRelease();

        this.amplitudeEnvelopeAttack();
        this.amplitudeEnvelopeDecay();
        this.amplitudeEnvelopeSustain();
        this.amplitudeEnvelopeRelease();

        this.synthToggle();
    }

    getVoice(oscIndex: number): Tone.MonoSynth {
        if (this.synthReady("getVoice")) {
            switch(oscIndex) {
                case 0:
                    return this.synth.voice0;
                case 1:
                    return this.synth.voice1;
            }
            console.log(`Asked for a voice with index ${oscIndex} but that is out of range.`)
        }
        return null;
    }

    powerToggle(turnOn: boolean) {
        if (turnOn) {
            this.createSynth();
        } else {
            this.destroySynth();
        }
    }

    oscillatorToggle(oscIndex: number, turnOn?: boolean): boolean {
        if (turnOn == undefined) {
            turnOn = this.oscillators[oscIndex].onOff.getValue();
        }
        if (this.synthReady("oscillatorToggle")) {
            if (turnOn) {
                this.setOscillatorVolume(oscIndex);
            } else {
                this.muteOscillator(oscIndex);
            }
        }
        return turnOn;
    }

    // Volume [0 - 1]
    setOscillatorVolume(oscIndex: number, volume?: number): number {
        if (volume == undefined) {
            volume = this.oscillators[oscIndex].volume.getValue();
        }
        let value: number = mapRange(volume, 0, 1, -100, 0);
        if (this.synthReady("oscillatorVolume")) {
            if (this.oscillators[oscIndex].onOff.getValue()) {
                this.getVoice(oscIndex).volume.rampTo(value);
            }
        }
        return value;
    }

    muteOscillator(oscIndex: number) {
        this.getVoice(oscIndex).volume.rampTo(-200);
    }

    updateWaveform(oscIndex: number, waveformIndex?: number) {
        if (waveformIndex == undefined) {
            waveformIndex = this.oscillators[oscIndex].waveform.active;
        }
        if (this.synthReady("updateWaveform")) {
            let oscWaveform = this.oscillators[oscIndex].waveform;
            if (waveformIndex < 0) {
                // Inactive
                oscWaveform.active = oscWaveform.lastActive
            } else {
                oscWaveform.lastActive = waveformIndex;
                switch (waveformIndex) {
                    case 0:
                        this.getVoice(oscIndex).oscillator.type = "sine";
                        break;
                    case 1:
                        this.getVoice(oscIndex).oscillator.type = "square";
                        break;
                    case 2:
                        this.getVoice(oscIndex).oscillator.type = "sawtooth";
                        break;
                    case 3:
                        this.getVoice(oscIndex).oscillator.type = "triangle";
                        break;
                    default:
                        LOG.error("updateWaveform",`Osc# ${oscIndex} requested unknown oscillator type: ${waveformIndex}.`);
                }
            }
        }
    }

    // Input = [0 - 1], output = [0.5 - 2], non-linear, 0 -> 0.5 = 0.5 -> 1, 0.5 -> 1 = 1 -> 2
    detuneOscillator(value?: number){
        if (value == undefined) {
            value = this.oscillators[1].detune.getValue();
        }
        let detuneValue: number;
        if (value <= 0.5) {
            detuneValue = 0.5 + value;
        } else {
            detuneValue = 2 * value;
        }
        if (this.synthReady("detuneOscillator")) {
            this.synth.harmonicity.rampTo(detuneValue);
        }
        return detuneValue;
    }

    toggleNoise(on?: boolean): boolean {
        if (on == undefined) {
            on = this.noiseControl.onOff.getValue();
        }
        if (this.synthReady("toggleNoise")) {
            if (on) {
                this.noise.start();
            } else {
                this.noise.stop();
            }
        }
        return on;
    }

    toggleNoiseType(whiteNoise?: boolean): boolean {
        if (whiteNoise == undefined) {
            whiteNoise = this.noiseControl.type.getValue();
        }
        if (this.synthReady("toggleNoiseType")) {
            if (whiteNoise) {
                this.noise.type = "white";
            } else {
                this.noise.type = "pink";
            }
        }
        return whiteNoise;
    }

    noiseVolume(volume?: number): number {
        if (volume == undefined) {
            volume = this.noiseControl.volume.getValue();
        }
        let value: number = mapRange(volume, 0, 1, -100, 0);
        if (this.synthReady("noiseVolume")) {
            this.noise.volume.rampTo(value);
        }
        return value;
    }

    filterCutoff(cutoffFrequency?: number): number {
        if (cutoffFrequency == undefined) {
            cutoffFrequency = this.filterControl.cutoff.getValue();
        }
        let value: number = mapRange(cutoffFrequency, 0, 1, 8, 12000);
        if (this.synthReady("filterCutoff")) {
            this.synth.voice0.filterEnvelope.baseFrequency = value;
            this.synth.voice1.filterEnvelope.baseFrequency = value;
        }
        return value;
    }

    filterEmphasis(emphasis?: number): number {
        if (emphasis == undefined) {
            emphasis = this.filterControl.emphasis.getValue();
        }
        let value: number = mapRange(emphasis, 0, 1, 0, 10);
        if (this.synthReady("filterEmphasis")) {
            this.synth.voice0.filter.Q.rampTo(value);
            this.synth.voice1.filter.Q.rampTo(value);
        }
        return value;
    }

    filterEnvelopeAttack(attackDuration?: number): number {
        if (attackDuration == undefined) {
            attackDuration = this.filterEnvelopeControl.attack.getValue();
        }
        let value: number = mapRange(attackDuration, 0, 1, 0, 10);
        if (this.synthReady("filterEnvelopeAttack")) {
            this.synth.voice0.filterEnvelope.attack = value;
            this.synth.voice1.filterEnvelope.attack = value;
        }
        return value;
    }

    filterEnvelopeDecay(decayDuration?: number): number {
        if (decayDuration == undefined) {
            decayDuration = this.filterEnvelopeControl.decay.getValue();
        }
        let value: number = mapRange(decayDuration, 0, 1, 0, 10);
        if (this.synthReady("filterEnvelopeDecay")) {
            this.synth.voice0.filterEnvelope.decay = value;
            this.synth.voice1.filterEnvelope.decay = value;
        }
        return value;
    }

    filterEnvelopeSustain(sustainLevel?: number): number {
        if (sustainLevel == undefined) {
            sustainLevel = this.filterEnvelopeControl.sustain.getValue();
        }
        if (this.synthReady("filterEnvelopeSustain")) {
            this.synth.voice0.filterEnvelope.sustain = sustainLevel;
            this.synth.voice1.filterEnvelope.sustain = sustainLevel;
        }
        return sustainLevel;
    }

    filterEnvelopeRelease(releaseDuration?: number): number {
        if (releaseDuration == undefined) {
            releaseDuration = this.filterEnvelopeControl.release.getValue();
        }
        let value: number = mapRange(releaseDuration, 0, 1, 0, 10);
        if (this.synthReady("filterEnvelopeRelease")) {
            this.synth.voice0.filterEnvelope.release = value;
            this.synth.voice1.filterEnvelope.release = value;
        }
        return value;
    }

    amplitudeEnvelopeAttack(attackDuration?: number): number {
        if (attackDuration == undefined) {
            attackDuration = this.amplitudeEnvelopeControl.attack.getValue();
        }
        let value: number = mapRange(attackDuration, 0, 1, 0, 10);
        if (this.synthReady("amplitudeEnvelopeAttack")) {
            this.synth.voice0.envelope.attack = value;
            this.synth.voice1.envelope.attack = value;
        }
        return value;
    }

    amplitudeEnvelopeDecay(decayDuration?: number): number {
        if (decayDuration == undefined) {
            decayDuration = this.amplitudeEnvelopeControl.decay.getValue();
        }
        let value: number = mapRange(decayDuration, 0, 1, 0, 10);
        if (this.synthReady("amplitudeEnvelopeDecay")) {
            this.synth.voice0.envelope.decay = value;
            this.synth.voice1.envelope.decay = value;
        }
        return value;
    }

    amplitudeEnvelopeSustain(sustainLevel?: number): number {
        if (sustainLevel == undefined) {
            sustainLevel = this.amplitudeEnvelopeControl.sustain.getValue();
        }
        if (this.synthReady("amplitudeEnvelopeSustain")) {
            this.synth.voice0.envelope.sustain = sustainLevel;
            this.synth.voice1.envelope.sustain = sustainLevel;
        }
        return sustainLevel;
    }

    amplitudeEnvelopeRelease(releaseDuration?: number): number {
        if (releaseDuration == undefined) {
            releaseDuration = this.amplitudeEnvelopeControl.release.getValue();
        }
        let value: number = mapRange(releaseDuration, 0, 1, 0, 10);
        if (this.synthReady("amplitudeEnvelopeRelease")) {
            this.synth.voice0.envelope.release = value;
            this.synth.voice1.envelope.release = value;
        }
        return value;
    }

    synthToggle(on?: boolean): boolean {
        if (on == undefined) {
            on = this.outputControl.onOff.getValue();
        }
        if (this.synthReady("synthToggle")) {
            if (on) {
                this.synthVolume();
            } else {
                this.synth.volume.rampTo(-200);
            }
        }
        return on;
    }

    synthVolume(volume?: number): number {
        if (volume == undefined) {
            volume = this.outputControl.volume.getValue();
        }
        let value: number = mapRange(volume, 0, 1, -100, 0);
        if (this.synthReady("synthVolume")) {
            if (this.outputControl.onOff.getValue()) {
                this.synth.volume.rampTo(value);
            }
        }
        return value;
    }

    create(): void {
        //  ---------- Oscillators
        for (let i: number = 0; i < this.numOsc; i++) {
            let oscillatorObj: Oscillator = new Oscillator();
            this.oscillators.push(oscillatorObj);
            // ----- Toggle Switch
            let onOffToggle: ControlEndpointToggle = new ControlEndpointToggle(
                `/synth/osc${i}/onOff`,
                `#${this.rootID}Osc${i}OnOff`,
                {},
                ["state", true],
                (on) => this.oscillatorToggle(i, on)
            );
            onOffToggle.create();
            NC.registerComponent(onOffToggle);
            oscillatorObj.onOff = onOffToggle;

            // ----- Volume Dial
            let volumeComponent: ControlEndpointDial = new ControlEndpointDial(
                `/synth/osc${i}/volume`,
                `#${this.rootID}Osc${i}Volume`,
                `#${this.rootID}Osc${i}VolumeValue`,
                {"min": 0, "max": 1},
                ["value", 0.5],
                (value) => {return this.setOscillatorVolume(i, value)},
                "dB"
            );
            volumeComponent.create();
            NC.registerComponent(volumeComponent);
            oscillatorObj.volume = volumeComponent;

            // ----- Waveform Selector
            const numWaveformButtons : number = 4;
            const defaultWaveform : number = 3;
            const waveformLabels : Array<string> = ["∿", "⎍", "	⌁", "∧"];
            let oscWaveform : Nexus.RadioButton = Nexus.Add.RadioButton(`#${this.rootID}Osc${i}Waveform`, {
                "size": [numWaveformButtons * CFG.blockWidth, CFG.blockHeight],
                "numberOfButtons": numWaveformButtons,
                "active": defaultWaveform
            });
            oscWaveform["lastActive"] = defaultWaveform;
            oscWaveform.on("change", (waveFormIx) => this.updateWaveform(i, waveFormIx));

            let waveformCircles : NodeListOf<Element> = document.querySelectorAll(`#${this.rootID}Osc${i}Waveform div div span svg circle`);
            for (let j = 0; j < waveformCircles.length; j++) {
                const circle = waveformCircles.item(j);
                circle.insertAdjacentHTML("afterend", `<text x="50%" y="50%" dominant-baseline="central" text-anchor="middle" pointer-events="none" font-size="${CFG.buttonWidth * 0.5}px">${waveformLabels[j]}</text>`);
            }
            oscillatorObj.waveform = oscWaveform;

            if (i > 0) {
                // ----- Detune Dial
                let detuneComponent: ControlEndpointDial = new ControlEndpointDial(
                    `/synth/osc${i}/detune`,
                    `#${this.rootID}Osc${i}Detune`,
                    `#${this.rootID}Osc${i}DetuneValue`,
                    {"min": 0, "max": 1},
                    ["value", 0.5],
                    (value) => {return this.detuneOscillator(value)},
                    "oct"
                );
                detuneComponent.create();
                NC.registerComponent(detuneComponent);
                oscillatorObj.detune = detuneComponent;
            }
        }

        // ---------- Noise
        // ----- On Off Switch
        let noiseOnOffToggle: ControlEndpointToggle = new ControlEndpointToggle(
            `/synth/noise/onOff`,
            `#${this.rootID}NoiseOnOff`,
            {},
            ["state", false],
            (on) => this.toggleNoise(on)
        );
        noiseOnOffToggle.create();
        NC.registerComponent(noiseOnOffToggle);

        // ----- Type Toggle Switch
        let noiseTypeToggle: ControlEndpointToggle = new ControlEndpointToggle(
            `/synth/noise/type`,
            `#${this.rootID}NoiseType`,
            {},
            ["state", false],
            (white) => this.toggleNoiseType(white)
        );
        noiseTypeToggle.create();
        NC.registerComponent(noiseTypeToggle);

        // ----- Volume Dial
        let noiseVolumeComponent: ControlEndpointDial = new ControlEndpointDial(
            "/synth/noise/volume",
            `#${this.rootID}NoiseVolume`,
            `#${this.rootID}NoiseVolumeValue`,
            {"min": 0, "max": 1},
            ["value", 0],
            (value) => {return this.noiseVolume(value)},
            "dB"
        );
        noiseVolumeComponent.create();
        NC.registerComponent(noiseVolumeComponent);

        // ---------- Filter
        // ----- Cutoff Frequency
        let cutoffFrequencyFilterDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/filter/cutoff",
            `#${this.rootID}FilterCutoff`,
            `#${this.rootID}FilterCutoffValue`,
            {"min": 0, "max": 1},
            ["value", 0.3],
            (value) => {return this.filterCutoff(value)},
            "hz"
        );
        cutoffFrequencyFilterDial.create();
        NC.registerComponent(cutoffFrequencyFilterDial);

        // ----- Emphasis (Q)
        let emphasisFilterDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/filter/emphasis",
            `#${this.rootID}FilterEmphasis`,
            `#${this.rootID}FilterEmphasisValue`,
            {"min": 0, "max": 1},
            ["value", 0.1],
            (value) => {return this.filterEmphasis(value)}
        );
        emphasisFilterDial.create();
        NC.registerComponent(emphasisFilterDial);

        // ---------- Filter Envelope
        // ----- Attack Dial
        let attackFilterEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/filterEnvelope/attack",
            `#${this.rootID}EnvelopeAttack`,
            `#${this.rootID}EnvelopeAttackValue`,
            {"min": 0, "max": 1},
            ["value", 0.01],
            (value) => {return this.filterEnvelopeAttack(value)},
            "s"
        );
        attackFilterEnvelopeDial.create();
        NC.registerComponent(attackFilterEnvelopeDial);

        // ----- Decay Dial
        let decayFilterEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/filterEnvelope/decay",
            `#${this.rootID}EnvelopeDecay`,
            `#${this.rootID}EnvelopeDecayValue`,
            {"min": 0, "max": 1},
            ["value", 0.1],
            (value) => {return this.filterEnvelopeDecay(value)},
            "s"
        );
        decayFilterEnvelopeDial.create();
        NC.registerComponent(decayFilterEnvelopeDial);

        // ----- Sustain Dial
        let sustainFilterEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/filterEnvelope/sustain",
            `#${this.rootID}EnvelopeSustain`,
            `#${this.rootID}EnvelopeSustainValue`,
            {"min": 0, "max": 1},
            ["value", 0.5],
            (value) => {return this.filterEnvelopeSustain(value)}
        );
        sustainFilterEnvelopeDial.create();
        NC.registerComponent(sustainFilterEnvelopeDial);

        // ----- Release Dial
        let releaseFilterEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/filterEnvelope/release",
            `#${this.rootID}EnvelopeRelease`,
            `#${this.rootID}EnvelopeReleaseValue`,
            {"min": 0, "max": 1},
            ["value", 0.5],
            (value) => {return this.filterEnvelopeRelease(value)},
            "s"
        );
        releaseFilterEnvelopeDial.create();
        NC.registerComponent(releaseFilterEnvelopeDial);

        // ---------- Loudness Contour
        // ----- Attack Dial
        let attackAmplitudeEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/amplitudeEnvelope/attack",
            `#${this.rootID}LoudnessContourAttack`,
            `#${this.rootID}LoudnessContourAttackValue`,
            {"min": 0, "max": 1},
            ["value", 0.01],
            (value) => {return this.amplitudeEnvelopeAttack(value)},
            "s"
        );
        attackAmplitudeEnvelopeDial.create();
        NC.registerComponent(attackAmplitudeEnvelopeDial);

        // ----- Decay Dial
        let decayAmplitudeEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/amplitudeEnvelope/decay",
            `#${this.rootID}LoudnessContourDecay`,
            `#${this.rootID}LoudnessContourDecayValue`,
            {"min": 0, "max": 1},
            ["value", 0.1],
            (value) => {return this.amplitudeEnvelopeDecay(value)},
            "s"
        );
        decayAmplitudeEnvelopeDial.create();
        NC.registerComponent(decayAmplitudeEnvelopeDial);

        // ----- Sustain Dial
        let sustainAmplitudeEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/amplitudeEnvelope/sustain",
            `#${this.rootID}LoudnessContourSustain`,
            `#${this.rootID}LoudnessContourSustainValue`,
            {"min": 0, "max": 1},
            ["value", 0.5],
            (value) => {return this.amplitudeEnvelopeSustain(value)}
        );
        sustainAmplitudeEnvelopeDial.create();
        NC.registerComponent(sustainAmplitudeEnvelopeDial);

        // ----- Release Dial
        let releaseAmplitudeEnvelopeDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/amplitudeEnvelope/release",
            `#${this.rootID}LoudnessContourRelease`,
            `#${this.rootID}LoudnessContourReleaseValue`,
            {"min": 0, "max": 1},
            ["value", 0.1],
            (value) => {return this.amplitudeEnvelopeRelease(value)},
            "s"
        );
        releaseAmplitudeEnvelopeDial.create();
        NC.registerComponent(releaseAmplitudeEnvelopeDial);

        // --------- Output
        // ----- OnOff Toggle
        let onOffOutputToggle: ControlEndpointToggle = new ControlEndpointToggle(
            `/synth/output/onOff`,
            `#${this.rootID}OutputOnOff`,
            {},
            ["state", true],
            (on) => this.synthToggle(on)
        );
        onOffOutputToggle.create();
        NC.registerComponent(onOffOutputToggle);

        // ----- Volume
        let volumeOutputDial: ControlEndpointDial = new ControlEndpointDial(
            "/synth/output/volume",
            `#${this.rootID}OutputVolume`,
            `#${this.rootID}OutputVolumeValue`,
            {"min": 0, "max": 1},
            ["value", 0.3],
            (value) => {return this.synthVolume(value)},
            "dB"
        );
        volumeOutputDial.create();
        NC.registerComponent(volumeOutputDial);

        this.noiseControl = new NoiseControl(
            noiseOnOffToggle, 
            noiseTypeToggle, 
            noiseVolumeComponent
        );
        this.filterControl = new FilterControl(
            cutoffFrequencyFilterDial,
            emphasisFilterDial
        )
        this.filterEnvelopeControl = new EnvelopeControl(
            attackFilterEnvelopeDial,
            decayFilterEnvelopeDial,
            sustainFilterEnvelopeDial,
            releaseFilterEnvelopeDial
        );
        this.amplitudeEnvelopeControl = new EnvelopeControl(
            attackAmplitudeEnvelopeDial,
            decayAmplitudeEnvelopeDial,
            sustainAmplitudeEnvelopeDial,
            releaseAmplitudeEnvelopeDial
        )
        this.outputControl = new OutputControl(
            onOffOutputToggle,
            volumeOutputDial
        )
    }
}

export { ModelDSynth };