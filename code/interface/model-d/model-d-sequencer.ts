import * as Nexus from "./NexusUI.js";
// import * as Tone from "./tone.js";

import { CFG } from "./model-d-cfg.js";
import { mapRange, getFrequency, LOG } from "./utils.js";
import { ControlEndpointDial, ControlEndpointToggle, NC } from "./network-controller.js";

class ModelDSequencer {
    root: HTMLDivElement;
    rootID: string;

    configured: boolean = false;

    sequencer: Nexus.Sequencer;
    onOffToggle: ControlEndpointToggle;
    tempo: ControlEndpointDial;
    octave: ControlEndpointDial;
    stepMode: Nexus.RadioButton;

    minOctaveDifference : number = 0;
    maxOctaveDifference : number = 7;
    octaveStep : number = 1 / ((this.maxOctaveDifference) - this.minOctaveDifference);
    startingOctave : number = 4 * (this.octaveStep);

    stepListeners: Map<string, Function> = new Map();
    changeListeners: Map<string, Function> = new Map();
    stepId: number = 0;
    changeId: number = 0;

    numRows: number;
    numCols: number;

    constructor(parentDiv: HTMLDivElement, parentID: string, numberOfColumns:number = 16, numberOfRows: number = 12) {
        this.root = parentDiv;
        this.rootID = parentID;
        this.numCols = numberOfColumns;
        this.numRows = numberOfRows;
    }

    getBPM(value?: number): number {
        const minBPM: number = 1;
        const maxBPM: number = 200;
        if (value == undefined) {
            value = this.tempo.getValue();
        }
        return mapRange(value, 0, 1, minBPM, maxBPM);
    }

    getTempo(value?: number): number {
        const bpm: number = this.getBPM(value);
        const msInterval: number = 60000 / bpm;
        return msInterval;
    }

    setTempo(value: number): number {
        const msInterval = this.getTempo(value);
        this.sequencer.interval.ms(msInterval);
        return this.getBPM(value);
    }

    // Returns the octave from the internal value which is 0 - 1
    getOctave(value?: number): number {
        if (value == undefined) {
            value = this.octave.getValue();
        }
        return Math.round(mapRange(value, 0, 1, this.minOctaveDifference, this.maxOctaveDifference));
    }

    getFrequencies(step: Array<number>): Array<number> {
        let frequencies: Array<number> = [];
        const referenceFrequency: number = 261.63; // C4, default bottom note

        for (let i = 0; i < step.length; i++) {
            if (step[i] == 1) {
                const semitoneDifference: number = ((this.getOctave() - 4) * 12) + i;
                const freq: number = getFrequency(referenceFrequency, semitoneDifference);
                frequencies.push(freq);
            }
        }

        return frequencies;
    }

    powerToggle(on: boolean): boolean {
        if (this.configured) {
            if (on) {
                LOG.info("seqOnOff", "starting sequencer");
                this.sequencer.start(this.getTempo());
            } else {
                LOG.info("seqOnOff", "stopping sequencer");
                this.sequencer.stop();
            }
            return on;
        } else {
            return false;
        }
    }

    updateStepMode(stepIndex?: number): void {
        if (this.configured) {
            if (stepIndex == undefined) {
                stepIndex = this.stepMode.active;
            }
            if (stepIndex < 0) {
                this.stepMode.active = this.stepMode.lastActive;
            } else {
                this.stepMode.lastActive = stepIndex;
                switch (stepIndex) {
                    case 0:
                        this.sequencer.stepper.mode = "up";
                        break;
                    case 1:
                        this.sequencer.stepper.mode = "down";
                        break;
                    case 2:
                        this.sequencer.stepper.mode = "drunk";
                        break;
                    case 3:
                        this.sequencer.stepper.mode = "random";
                        break;
                    default:
                        LOG.error("updateStepMode",`Sequencer requested unknown step mode: ${stepIndex}.`);
                }
            }
        }
    }

    create() {
        // ----- Note Grid
        this.sequencer = Nexus.Add.Sequencer(`#${this.rootID}SeqNoteGrid`, {
            "size": [CFG.sequencerWidth, CFG.sequencerHeight],
            "mode": "toggle",
            "rows": this.numRows,
            "columns": this.numCols,
        });
        this.sequencer.on("step", (value) => {
            this.stepListeners.forEach((f: Function, key: string, map: Map<string, Function>) => {
                f(value);
            });
        });
        this.sequencer.on("change", (value) => {
            this.changeListeners.forEach((f: Function, key: string, map: Map<string, Function>) => {
                f(value);
            });
        });
        this.sequencer.colorize("mediumLight", "#fffbbd");

        // ----- Tempo
        this.tempo = new ControlEndpointDial(
            "/sequencer/tempo",
            `#${this.rootID}SeqTempo`,
            `#${this.rootID}SeqTempoValue`,
            {"min": 0, "max": 1},
            ["value", 0.7],
            (value) => {return this.setTempo(value)},
            "bpm"
        );
        this.tempo.create();
        NC.registerComponent(this.tempo);

        // ----- OnOff Toggle
        this.onOffToggle = new ControlEndpointToggle(
            `/sequencer/onOff`,
            `#${this.rootID}SeqOnOff`,
            {},
            ["state", false],
            (on) => this.powerToggle(on)
        );
        this.onOffToggle.create();
        NC.registerComponent(this.onOffToggle);

        // ----- Octave
        this.octave = new ControlEndpointDial(
            "/sequencer/octave",
            `#${this.rootID}SeqOctave`,
            `#${this.rootID}SeqOctaveValue`,
            {"min": 0, "max": 1, "step": this.octaveStep},
            ["value", this.startingOctave],
            (value) => {return this.getOctave(value)},
            "oct"
        );
        this.octave.create();
        NC.registerComponent(this.octave);

        // ------ Stepper Control
        // this.stepControl = 
        const numStepModeButtons: number = 4;
        const defaultStepMode: number = 0;
        const waveformLabels: Array<string> = ["F", "B", "D", "R"];
        let stepModeControl: Nexus.RadioButton = Nexus.Add.RadioButton(
            `#${this.rootID}SeqStepMode`, 
            {
                "size": [numStepModeButtons * CFG.blockWidth, CFG.blockHeight],
                "numberOfButtons": numStepModeButtons,
                "active": defaultStepMode
            }
        );
        stepModeControl["lastActive"] = defaultStepMode;
        stepModeControl.on("change", (stepIndex) => this.updateStepMode(stepIndex));

        let stepModeCircles : NodeListOf<Element> = document.querySelectorAll(`#${this.rootID}SeqStepMode div div span svg circle`);
        for (let j = 0; j < stepModeCircles.length; j++) {
            const circle = stepModeCircles.item(j);
            circle.insertAdjacentHTML("afterend", `<text x="50%" y="50%" dominant-baseline="central" text-anchor="middle" pointer-events="none" font-size="${CFG.buttonWidth * 0.3}px">${waveformLabels[j]}</text>`);
        }
        this.stepMode = stepModeControl;

        this.configured = true;
    }

    registerStepListener(f: Function): string {
        console.log("Registering Step Listener");
        let id = `step-${this.stepId}`;
        this.stepId = this.stepId + 1;

        this.stepListeners.set(id, f);
        return id;
    }

    registerChangeListener(f: Function): string {
        let id = `change-${this.changeId}`;
        this.changeId = this.stepId + 1;

        this.changeListeners.set(id, f);
        return id;
    }

    removeStepListener(key: string) {
        this.stepListeners.delete(key);
    }

    removeChangeListener(key: string) {
        this.changeListeners.delete(key);
    }
}

export { ModelDSequencer };