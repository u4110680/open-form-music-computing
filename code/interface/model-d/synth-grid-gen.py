components = {
    "dial": {
        "width": 3,
        "height": 3
    },
    "toggle": {
        "width": 3,
        "height": 1
    },
    "button": {
        "width": 1,
        "height": 1
    },
    "oscLabel": {
        "width": 13,
        "height": 1
    },
    "envelopeLabel": {
        "width": 12,
        "height": 1
    },
    "stdLabel": {
        "width": 3,
        "height": 1
    },
    "valueField": {
        "width": 3,
        "height": 1
    },
    "valueReset": {
        "width": 3,
        "height": 1
    },
}

rows = {
    1: [
        [".modelDSynth .Osc0Heading", "oscLabel"],
        [".NoiseHeading", "stdLabel"],
        [".FilterHeading", "stdLabel"],
        [".EnvelopeHeading", "envelopeLabel"],
        [".OutputHeading", "stdLabel"]
    ],
    2: [
        [".modelDSynth .Osc0OnOffHeading", "stdLabel"],
        [".Osc0VolumeHeading", "stdLabel"],
        [".Osc0WaveformHeading", {"width": 4, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        [".NoiseOnOffHeading", "stdLabel"],
        [".FilterCutoffHeading", "stdLabel"],
        [".EnvelopeAttackHeading", "stdLabel"],
        [".EnvelopeDecayHeading", "stdLabel"],
        [".EnvelopeSustainHeading", "stdLabel"],
        [".EnvelopeReleaseHeading", "stdLabel"],
        [".OutputOnOffHeading", "stdLabel"],
    ],
    3: [
        ["GAP", {"width": 3, "height": 1}],
        [".Osc0Volume", "dial"],
        [".Osc0Waveform", {"width": 4, "height": 3}],
        ["GAP", {"width": 3, "height": 1}],
        [".NoiseOnOff", "stdLabel"],
        [".FilterCutoff", "dial"],
        [".EnvelopeAttack", "dial"],
        [".EnvelopeDecay", "dial"],
        [".EnvelopeSustain", "dial"],
        [".EnvelopeRelease", "dial"],
    ],
    4: [
        [".Osc0OnOff", "toggle"],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 4, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        [".NoiseTypeHeading", "stdLabel"],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        [".OutputOnOff", "toggle"],
    ],
    5: [
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 4, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        [".NoiseType", "toggle"],
    ],
    6: [
        ["GAP", {"width": 3, "height": 1}],
        [".Osc0VolumeValue", "valueReset"],
        ["GAP", {"width": 4, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        [".FilterCutoffValue", "valueReset"],
        [".EnvelopeAttackValue", "valueReset"],
        [".EnvelopeDecayValue", "valueReset"],
        [".EnvelopeSustainValue", "valueReset"],
        [".EnvelopeReleaseValue", "valueReset"],
    ],
    7: [
        [".Osc1Heading", "oscLabel"],
        ["GAP", {"width": 3, "height": 1}],
        ["GAP", {"width": 3, "height": 1}],
        [".LoudnessContourHeading", "envelopeLabel"],
    ],
    8: [
        [".Osc1OnOffHeading", "stdLabel"],
        [".Osc1VolumeHeading", "stdLabel"],
        [".Osc1WaveformHeading", {"width": 4, "height": 1}],
        [".Osc1DetuneHeading", "stdLabel"],
        [".NoiseVolumeHeading", "stdLabel"],
        [".FilterEmphasisHeading", "stdLabel"],
        [".LoudnessContourAttackHeading", "stdLabel"],
        [".LoudnessContourDecayHeading", "stdLabel"],
        [".LoudnessContourSustainHeading", "stdLabel"],
        [".LoudnessContourReleaseHeading", "stdLabel"],
        [".OutputVolumeHeading", "stdLabel"],
    ],
    9: [
        ["GAP", {"width": 3, "height": 1}],
        [".Osc1Volume", "dial"],
        [".Osc1Waveform", {"width": 4, "height": 3}],
        [".Osc1Detune", "dial"],
        [".NoiseVolume", "dial"],
        [".FilterEmphasis", "dial"],
        [".LoudnessContourAttack", "dial"],
        [".LoudnessContourDecay", "dial"],
        [".LoudnessContourSustain", "dial"],
        [".LoudnessContourRelease", "dial"],
        [".OutputVolume", "dial"],
    ],
    10: [
        [".Osc1OnOff", "toggle"],
    ],
    11: [],
    12: [
        ["GAP", {"width": 3, "height": 1}],
        [".Osc1VolumeValue", "valueReset"],
        ["GAP", {"width": 4, "height": 1}],
        [".Osc1DetuneValue", "valueReset"],
        [".NoiseVolumeValue", "valueReset"],
        [".FilterEmphasisValue", "valueReset"],
        [".LoudnessContourAttackValue", "valueReset"],
        [".LoudnessContourDecayValue", "valueReset"],
        [".LoudnessContourSustainValue", "valueReset"],
        [".LoudnessContourReleaseValue", "valueReset"],
        [".OutputVolumeValue", "valueReset"],
    ]
}

surrounds = {
    ".modelDSynth .Osc0Heading": [".modelDSynth .Osc0Surround", {"width": 13, "height": 6}],
    ".Osc1Heading": [".modelDSynth .Osc1Surround", {"width": 13, "height": 6}],
    ".NoiseHeading": [".modelDSynth .NoiseSurround", {"width": 3, "height": 12}],
    ".FilterHeading": [".modelDSynth .FilterSurround", {"width": 3, "height": 12}],
    ".EnvelopeHeading": [".modelDSynth .EnvelopeSurround", {"width": 12, "height": 6}],
    ".LoudnessContourHeading": [".modelDSynth .LoudnessContourSurround", {"width": 12, "height": 6}],
    ".OutputHeading": [".modelDSynth .OutputSurround", {"width": 3, "height": 12}],
}
maxRow = 0
maxCol = 0

cssOutputs = {}
for row in sorted(rows):
    column = 1
    for item in rows[row]:
        if item[0] == "GAP":
            column = column + item[1]["width"]
        else:
            if isinstance(item[1], str):
                width = components[item[1]]["width"]
                height = components[item[1]]["height"]
            elif isinstance(item[1], dict):
                width = item[1]["width"]
                height = item[1]["height"]

            if width > 1:
                columnSpan = f" / span {width}"
            else:
                columnSpan = ""

            if height > 1:
                rowSpan = f" / span {height}"
            else:
                rowSpan = ""


            cssOutputs[item[0]] = f"""{item[0]} {{
    grid-column: {column}{columnSpan};
    grid-row: {row}{rowSpan};
}}
"""

            if item[0] in surrounds:
                surround = surrounds[item[0]]
                # Also has a surround at the same col / row
                if isinstance(surround[1], str):
                    surrWidth = components[surround[1]]["width"]
                    surrHeight = components[surround[1]]["height"]
                elif isinstance(surround[1], dict):
                    surrWidth = surround[1]["width"]
                    surrHeight = surround[1]["height"]

                if surrWidth > 1:
                    surrColumnSpan = f" / span {surrWidth}"
                else:
                    surrColumnSpan = ""

                if surrHeight > 1:
                    surrRowSpan = f" / span {surrHeight}"
                else:
                    surrRowSpan = ""
                cssOutputs[surround[0]] = f"""{surround[0]} {{
    grid-column: {column}{surrColumnSpan};
    grid-row: {row}{surrRowSpan};
}}
"""

            if row + height > maxRow:
                maxRow = row + height
            if column + width > maxCol:
                maxCol = column + width

            column = column + width

outputTemplate = """/* Oscillators */
/* OSC 0 */
.modelDSynth .Osc0Surround
.modelDSynth .Osc0Heading
.modelDSynth .Osc0OnOffHeading
.Osc0OnOff
.Osc0VolumeHeading
.Osc0Volume
.Osc0VolumeValue
.Osc0WaveformHeading
.Osc0Waveform

/* OSC 1 */
.modelDSynth .Osc1Surround
.Osc1Heading
.Osc1OnOffHeading
.Osc1OnOff
.Osc1VolumeHeading
.Osc1Volume
.Osc1VolumeValue
.Osc1WaveformHeading
.Osc1Waveform
.Osc1DetuneHeading
.Osc1Detune
.Osc1DetuneValue

/* Noise */
.modelDSynth .NoiseSurround
.NoiseHeading
.NoiseOnOffHeading
.NoiseOnOff
.NoiseTypeHeading
.NoiseType
.NoiseVolumeHeading
.NoiseVolume
.NoiseVolumeValue

/* Filter */
.modelDSynth .FilterSurround
.FilterHeading
.FilterCutoffHeading
.FilterCutoff
.FilterCutoffValue
.FilterEmphasisHeading
.FilterEmphasis
.FilterEmphasisValue

/* Envelope */
.modelDSynth .EnvelopeSurround
.EnvelopeHeading
.EnvelopeAttackHeading
.EnvelopeAttack
.EnvelopeAttackValue
.EnvelopeDecayHeading
.EnvelopeDecay
.EnvelopeDecayValue
.EnvelopeSustainHeading
.EnvelopeSustain
.EnvelopeSustainValue
.EnvelopeReleaseHeading
.EnvelopeRelease
.EnvelopeReleaseValue

/* Loudness */
.modelDSynth .LoudnessContourSurround
.LoudnessContourHeading
.LoudnessContourAttackHeading
.LoudnessContourAttack
.LoudnessContourAttackValue
.LoudnessContourDecayHeading
.LoudnessContourDecay
.LoudnessContourDecayValue
.LoudnessContourSustainHeading
.LoudnessContourSustain
.LoudnessContourSustainValue
.LoudnessContourReleaseHeading
.LoudnessContourRelease
.LoudnessContourReleaseValue

/* Output */
.modelDSynth .OutputSurround
.OutputHeading
.OutputOnOffHeading
.OutputOnOff
.OutputVolumeHeading
.OutputVolume
.OutputVolumeValue
"""

for key in cssOutputs:
    if f"{key}\n" in outputTemplate:
        outputTemplate = outputTemplate.replace(f"{key}\n", cssOutputs[key], 1)
    else:
        print(f"{key} not in template")
        outputTemplate = outputTemplate + f"\n{cssOutputs[key]}"

print(outputTemplate)
print()
print("rows:", maxRow, "columns:", maxCol)