class CONFIG_PARAMETERS {
    readonly blockWidth:  number = 20;
    readonly blockHeight: number = 20;

    readonly dialWidth:  number = 3 * (this.blockWidth + 4);
    readonly dialHeight: number = 3 * (this.blockHeight + 4);

    readonly buttonWidth:  number = 3 * this.blockWidth;
    readonly buttonHeight: number = 3 * this.blockHeight;

    readonly resetWidth:  number = 3 * this.blockWidth;
    readonly resetHeight: number = 1 * this.blockHeight;

    readonly toggleWidth:  number = 3 * this.blockWidth;
    readonly toggleHeight: number = this.blockHeight;

    readonly sequencerWidth: number = 800;
    readonly sequencerHeight: number = 400;

    readonly valueResetFontSize:   string = "13px";
    readonly valueResetBorderSize: string = "2px";

    readonly heartbeatInterval: number = 5000; // 5s

    readonly randomIdNumber: boolean = false;
    readonly id: string = this.randomIdNumber ? (Math.floor(Math.random()* (9999 - 2000 + 1)) + 2000).toString().substring(0, 4) : "2000";

    // DEBUG SUCCESS INFO WARNING ERROR
    readonly logLevel: string = "WARNING";
}

const CFG: CONFIG_PARAMETERS = new CONFIG_PARAMETERS();

export { CFG };