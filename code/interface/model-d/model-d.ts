import * as Nexus from "./NexusUI.js";
import * as Tone from "./tone.js";

import { ModelDSynth } from "./model-d-synth.js";
import { ModelDSequencer } from "./model-d-sequencer.js";
import { LOG } from "./utils.js";
import { NC, getSocket } from "./network-controller.js";


// HTML ELEMENTS
const initSettingsDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById('initSettings'));
const initSettingsWrapperDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById('initSettingsWrapper'));
const initSettingsShowHideDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById('initSettingsShowHide'));
initSettingsShowHideDiv["show"] = true;

const statusDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById("status"));
const statusWrapperDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById("statusWrapper"));
const statusShowHideDiv: HTMLDivElement = (<HTMLDivElement> document.getElementById("statusShowHide"));
statusShowHideDiv["show"] = false;

const audioContextDiv : HTMLDivElement = (<HTMLDivElement> document.getElementById('audioContext'));
const audioContextTextDiv : HTMLDivElement = (<HTMLDivElement> document.getElementById('audioContextText'));
const audioContext : Nexus.Button = Nexus.Add.Button("#audioContextButton", {
    "size": [300, 300],
    "mode": "toggle",
});

const localRackID : string = "localRack";
const localRackDiv : HTMLDivElement = (<HTMLDivElement> document.getElementById(`${localRackID}`));
const localRackSynthDiv : HTMLDivElement = (<HTMLDivElement> document.getElementById(`${localRackID}Synth`));
const localRackSequencerDiv : HTMLDivElement = (<HTMLDivElement> document.getElementById(`${localRackID}Sequencer`));

// main
// initSettingsDiv.style.display = "none";
// audioContextDiv.style.display = "none";

// ----- Start Audio Context Handling
audioContext.on("change", async (buttonState) => {
    if (buttonState) {
        audioContextTextDiv.innerHTML = "Audio Loading...";
        Nexus.context = Tone.context._context._nativeAudioContext;
        LOG.debug("audioContext", `Nexus context state: ${Nexus.context.state}`);
        await Tone.start();
        LOG.info("audioContext", "audio is ready");
        LOG.debug("audioContext", `Nexus context state: ${Nexus.context.state}`);
        audioContextTextDiv.innerHTML = "Audio Ready!"
        setTimeout(() => {
            Tone.Transport.start();
            setup();
        }, 1000);
        setTimeout(() => {
            audioContextDiv.style.display = "none";
        }, 2000);
    }
});

// ----- Server Setting Div
document.getElementById('configSubmit')?.addEventListener('click', async () => {
    let host: string = (<HTMLInputElement> document.getElementById('address')).value;
    if (host == null || host == undefined || host == "") {
        host = (<HTMLInputElement> document.getElementById('address')).placeholder;
    }
    
    let port_str: string = (<HTMLInputElement>document.getElementById('port')).value;
    if (port_str == null || port_str == undefined || port_str == "") {
        port_str = (<HTMLInputElement>document.getElementById('port')).placeholder;
    }

    let name: string = (<HTMLInputElement>document.getElementById('name')).value;
    if (name == null || name == undefined || name.trim().length <= 0 || name.length > 10) {
        LOG.error("NetworkConfig", "name must be 1-10 characters");
        return;
    }

    let port: number = null;
    try {
        port = Number.parseInt(port_str);
        if (Number.isNaN(port) || port < 1 || port > 65535) {
            throw "Port Out Of Range (1 - 65535)";
        }
    } catch(err) {
        LOG.error("NetworkConfig", "port value error: " + err);
        return;
    }

    NC.init(host, port, name);
});

// ----- Status show/hide
statusShowHideDiv.addEventListener("click", async () => {
    statusShowHideDiv["show"] = !statusShowHideDiv["show"];
    if (statusShowHideDiv["show"]) {
        statusWrapperDiv.style.bottom = "0px";
        statusShowHideDiv.getElementsByTagName("span")[0].textContent = "HIDE";
    } else {
        statusWrapperDiv.style.bottom = "-110px";
        statusShowHideDiv.getElementsByTagName("span")[0].textContent = "SHOW";
    }
});

// ----- Init settings show/hide
initSettingsShowHideDiv.addEventListener("click", async () => {
    initSettingsShowHideDiv["show"] = !initSettingsShowHideDiv["show"];
    if (initSettingsShowHideDiv["show"]) {
        initSettingsWrapperDiv.style.top = "0px";
        initSettingsShowHideDiv.getElementsByTagName("span")[0].textContent = "HIDE";
    } else {
        initSettingsWrapperDiv.style.top = "-140px";
        initSettingsShowHideDiv.getElementsByTagName("span")[0].textContent = "SHOW";
    }
});

// ----- Rack
const LOCALSYNTH: ModelDSynth = new ModelDSynth(localRackSynthDiv, localRackID);
const LOCALSEQUENCER: ModelDSequencer = new ModelDSequencer(localRackSequencerDiv, localRackID, 16, 12);

async function setup() {
    LOCALSYNTH.create();
    LOCALSYNTH.powerToggle(true);
    LOCALSEQUENCER.create();
    // LOCALSEQUENCER.registerChangeListener((change) => {
    //     console.log("change", change);
    // });
    LOCALSEQUENCER.registerStepListener((step) => {
        let frequencies: Array<number> = LOCALSEQUENCER.getFrequencies(step);
        if (frequencies.length > 0) {
            // Only use the last element for now because the synth is single voice
            const frequency: number = frequencies.pop();
            const duration: number = (LOCALSEQUENCER.getTempo()) / 1000;
            if (LOCALSYNTH.synthReady("LS-stepListener")) {
                LOCALSYNTH.synth.triggerAttackRelease(frequency, duration);
            }
        }
    });
}

// Export things to the browser to be able to debug in console
const MODULEEXPORTS = {LOCALSYNTH, LOCALSEQUENCER, LOG, NC, initSettingsWrapperDiv, getSocket};
// @ts-ignore
window.mod = MODULEEXPORTS;