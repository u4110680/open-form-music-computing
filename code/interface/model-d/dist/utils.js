import { CFG } from "./model-d-cfg.js";
// Maps a value from one range to another
export function mapRange(value, inLow, inHigh, outLow, outHigh) {
    return outLow + (outHigh - outLow) * (value - inLow) / (inHigh - inLow);
}
export function getFrequency(rootFrequency, semitoneDifference) {
    return rootFrequency * (2 ** (semitoneDifference / 12));
}
export function rightSplit(input, delimiter, limit = 0) {
    if (limit <= 0) {
        return input.split(delimiter);
    }
    let elements = [];
    let currentInput = input;
    while (limit > 0) {
        const lastIndex = currentInput.lastIndexOf(delimiter);
        if (lastIndex < 0) {
            // No more elements left in input string
            break;
        }
        // Add next element to list
        elements.unshift(currentInput.substring(lastIndex + delimiter.length));
        // Remove element from string
        currentInput = currentInput.substring(0, lastIndex);
        limit--;
    }
    elements.unshift(currentInput);
    return elements;
}
export class PixelMaths {
    static add(a, b) {
        let result = parseFloat(a.replace("px", "")) + parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }
    static sub(a, b) {
        let result = parseFloat(a.replace("px", "")) - parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }
    static mul(a, b) {
        let result = parseFloat(a.replace("px", "")) * parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }
    static div(a, b) {
        let result = parseFloat(a.replace("px", "")) / parseFloat(b.replace("px", ""));
        return `${result.toFixed(2)}px`;
    }
}
export var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["DEBUG"] = 0] = "DEBUG";
    LogLevel[LogLevel["SUCCESS"] = 1] = "SUCCESS";
    LogLevel[LogLevel["INFO"] = 2] = "INFO";
    LogLevel[LogLevel["WARNING"] = 3] = "WARNING";
    LogLevel[LogLevel["ERROR"] = 4] = "ERROR";
})(LogLevel || (LogLevel = {}));
const logLevelMap = new Map([
    [LogLevel.DEBUG, "DEBUG"],
    [LogLevel.SUCCESS, "SUCCESS"],
    [LogLevel.INFO, "INFO"],
    [LogLevel.WARNING, "WARNING"],
    [LogLevel.ERROR, "ERROR"],
]);
function logLevelString(level) {
    if (logLevelMap.has(level)) {
        return logLevelMap.get(level);
    }
    return "UNKNOWN";
}
function stringLogLevel(levelString) {
    for (const [key, value] of logLevelMap) {
        if (value === levelString) {
            return key;
        }
    }
    return undefined;
}
export class Logger {
    constructor(level = LogLevel.DEBUG, logToConsole = true, logToDiv = true, divLog = undefined) {
        this.level = level;
        this.store = new Array();
        this.logToConsole = logToConsole;
        this.logToDiv = logToDiv;
        this.divLog = divLog;
    }
    // %Y-%m-%d_%H-%M-%S
    static dateTimeString(date) {
        let year = date.getFullYear();
        let month = date.getMonth().toString().length > 1 ? date.getMonth().toString() : "0" + date.getMonth().toString();
        let day = date.getDate().toString().length > 1 ? date.getDate().toString() : "0" + date.getDate().toString();
        let hour = date.getHours().toString().length > 1 ? date.getHours().toString() : "0" + date.getHours().toString();
        let minute = date.getMinutes().toString().length > 1 ? date.getMinutes().toString() : "0" + date.getMinutes().toString();
        let second = date.getSeconds().toString().length > 1 ? date.getSeconds().toString() : "0" + date.getSeconds().toString();
        let ret = `${year}-${month}-${day}_${hour}-${minute}-${second}`;
        return ret;
    }
    writeLog(level, caller, data) {
        let logTime = new Date();
        let timeString = Logger.dateTimeString(logTime);
        let logString = `${timeString} : ${level.toString()} : ${caller} : ${data}`;
        // Add to log store
        this.store.push(logString);
        // Only log if the level makes sense
        if (level >= this.level) {
            // Log to console
            if (this.logToConsole) {
                switch (level) {
                    case LogLevel.DEBUG:
                        // console.debug(logString);
                        console.log(`${timeString} %c${caller}`, "color: magenta", data);
                        break;
                    case LogLevel.SUCCESS:
                        // console.log(logString);
                        console.log(`${timeString} %c${caller}`, "color: green", data);
                        break;
                    case LogLevel.INFO:
                        // console.log(logString);
                        console.log(`${timeString} %c${caller}`, "color: blue", data);
                        break;
                    case LogLevel.WARNING:
                        // console.warn(logString);
                        console.log(`${timeString} %c${caller}`, "color: yellow", data);
                        break;
                    case LogLevel.ERROR:
                        // console.error(logString);
                        console.log(`${timeString} %c${caller}`, "color: red", data);
                        break;
                }
            }
            // Log to div
            if (this.logToDiv) {
                let updateScrollPosition = this.divLog.scrollHeight - this.divLog.scrollTop <= this.divLog.clientHeight + 1.1; // epsilon
                this.divLog.innerHTML += `<p class="logEntry">${timeString} : <span class="log${logLevelString(level)}">${caller}</span> : ${data}</p>`;
                // Scroll to the new bottom if we were at the bottom before
                if (updateScrollPosition) {
                    this.divLog.scrollTop = this.divLog.scrollHeight;
                }
            }
        }
    }
    debug(caller, data) {
        this.writeLog(LogLevel.DEBUG, caller, data);
    }
    success(caller, data) {
        this.writeLog(LogLevel.SUCCESS, caller, data);
    }
    info(caller, data) {
        this.writeLog(LogLevel.INFO, caller, data);
    }
    warn(caller, data) {
        this.writeLog(LogLevel.WARNING, caller, data);
    }
    error(caller, data) {
        this.writeLog(LogLevel.ERROR, caller, data);
    }
}
const statusDiv = document.getElementById("status");
export const LOG = new Logger(stringLogLevel(CFG.logLevel), true, true, statusDiv);
