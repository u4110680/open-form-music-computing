class CONFIG_PARAMETERS {
    constructor() {
        this.blockWidth = 20;
        this.blockHeight = 20;
        this.dialWidth = 3 * (this.blockWidth + 4);
        this.dialHeight = 3 * (this.blockHeight + 4);
        this.buttonWidth = 3 * this.blockWidth;
        this.buttonHeight = 3 * this.blockHeight;
        this.resetWidth = 3 * this.blockWidth;
        this.resetHeight = 1 * this.blockHeight;
        this.toggleWidth = 3 * this.blockWidth;
        this.toggleHeight = this.blockHeight;
        this.sequencerWidth = 800;
        this.sequencerHeight = 400;
        this.valueResetFontSize = "13px";
        this.valueResetBorderSize = "2px";
        this.heartbeatInterval = 5000; // 5s
        this.randomIdNumber = false;
        this.id = this.randomIdNumber ? (Math.floor(Math.random() * (9999 - 2000 + 1)) + 2000).toString().substring(0, 4) : "2000";
        // DEBUG SUCCESS INFO WARNING ERROR
        this.logLevel = "WARNING";
    }
}
const CFG = new CONFIG_PARAMETERS();
export { CFG };
