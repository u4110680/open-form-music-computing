// import * as Nexus from "./NexusUI.js";
import { CFG } from "./model-d-cfg.js";
import { ControlState, CTMessage, CTType, InputControl, InputControlType } from "./messages.js";
import { LOG, PixelMaths } from "./utils.js";
// ----- Connection vars
let configured = false;
let connected = false;
// NAME: string = null;
// const id: string = CFG.randomIdNumber ? (Math.floor(Math.random()* (9999 - 1000 + 1)) + 1000).toString().substring(0, 4) : "1111";
let name;
let host;
let port;
let socket;
function getSocket() {
    return socket;
}
export { getSocket };
let heartbeatInterval;
const initServerStatusField = document.getElementById('initServerStatus');
// ----- Colours
const connectedColour = "rgb(68, 207, 68)";
const disconnectedColour = "rgb(104, 106, 109)";
const connectingColour = "rgb(205, 207, 68)";
const connectionErrorColour = "rgb(207, 68, 68)";
// ----- Component vars
let components = {};
// ----- Connection Status Detection
let lastConnectionUpdate = -1;
let connectionChecker = setInterval(() => {
    if (socket == undefined) {
        // Init hasn't run yet
    }
    else if (socket.readyState == 1) {
        // Normal Operation
        if (lastConnectionUpdate != socket.readyState) {
            lastConnectionUpdate = socket.readyState;
        }
    }
    else if (socket.readyState == 3) {
        if (lastConnectionUpdate == 1) {
            lastConnectionUpdate = socket.readyState;
            initServerStatusField.value = "Connection Closed";
            initServerStatusField.style.backgroundColor = disconnectedColour;
        }
    }
}, 1000);
// ----- FUNCTIONS
function init(hostIn, portIn, nameIn) {
    if (configured) {
        LOG.error("NetworkController", "already configured");
        return;
    }
    host = hostIn;
    port = portIn;
    name = `${nameIn}#${CFG.id}`;
    LOG.info("NetworkController", `${name} connecting to ${host}:${port}`);
    initServerStatusField.value = "Connecting...";
    initServerStatusField.style.backgroundColor = connectingColour;
    configured = true;
    socket = new WebSocket(`ws://${host}:${port}`);
    socket.onopen = onConnect;
    socket.onmessage = onMessage;
    socket.onerror = onError;
    socket.onclose = onClose;
}
async function onConnect() {
    LOG.success("NetworkController", `connected to ${host}:${port}`);
    initServerStatusField.value = "Connected!";
    initServerStatusField.style.backgroundColor = connectedColour;
    connected = true;
    configured = true;
    sendHello();
    heartbeatInterval = setInterval(sendHeartbeat, CFG.heartbeatInterval);
    // setInterval(() => {console.log(SOCKET.readyState)}, 10000);
}
async function onMessage(message) {
    LOG.debug("NetworkController", `got message ${message.data}`);
    let parsedMessage = CTMessage.parse_message(message.data);
    processMessage(parsedMessage);
}
async function onClose(event) {
    LOG.info("NetworkController", `disconnected from ${this.host}:${port}`);
    LOG.debug("NetworkController", `disconnection event: ${event}`);
    clearInterval(heartbeatInterval);
    if (event.wasClean) {
        initServerStatusField.value = "Connection Closed";
        initServerStatusField.style.backgroundColor = disconnectedColour;
    }
    connected = false;
    configured = false;
}
async function onError(event) {
    LOG.error("NetworkController", `error from websocket: ${event}`);
    initServerStatusField.value = "Connection Error";
    initServerStatusField.style.backgroundColor = connectionErrorColour;
    connected = false;
    configured = false;
}
function sendHello() {
    LOG.info("NetworkController", "sending hello message");
    const helloMessage = new CTMessage(name, CTType.NEW_CONNECTION);
    sendCTMessage(helloMessage);
}
function sendHeartbeat() {
    LOG.info("NetworkController", "sending heartbeat");
    const heartbeatMessage = new CTMessage(name, CTType.HEARTBEAT);
    sendCTMessage(heartbeatMessage);
}
function sendControlValue(path, value) {
    LOG.info("NetworkController", `sending value message from ${path} (${value})`);
    const valueMessge = new CTMessage(name, CTType.INPUT_CONTROL, InputControl.constructValueMessage(path, value));
    sendCTMessage(valueMessge);
}
function sendControlState(path, state) {
    LOG.info("NetworkController", `sending state message from ${path} (${state})`);
    const stateMessge = new CTMessage(name, CTType.INPUT_CONTROL, InputControl.constructStateMessage(path, state));
    sendCTMessage(stateMessge);
}
function sendCTMessage(message) {
    if (connected) {
        const outMessage = message.construct_message();
        LOG.debug("NetworkController", `sending ${outMessage}`);
        socket.send(outMessage);
    }
}
function processMessage(message) {
    if (message == null) {
        // TODO: Maybe do something here who knows
        LOG.error("NetworkController", "processMessage was given a null message");
        return;
    }
    switch (message.type) {
        case CTType.HEARTBEAT:
            // TODO: Do we want to do more with this?
            LOG.info("NetworkController", "received heartbeat message");
            break;
        case CTType.INPUT_CONTROL:
            LOG.info("NetworkController", `received input control message: ${message.content}`);
            // Remove the starting message separator and split on the separator
            const inputMessage = InputControl.parseMessage(message);
            if (inputMessage.path in components) {
                const component = components[inputMessage.path];
                if (inputMessage.type == InputControlType.VALUE) {
                    component.networkValueUpdate(inputMessage.value);
                }
                else if (inputMessage.type == InputControlType.STATE) {
                    component.networkStateUpdate(inputMessage.state);
                }
                else if (inputMessage.type == InputControlType.REQUEST_VALUE) {
                    component.sendValue();
                }
                else if (inputMessage.type == InputControlType.REQUEST_STATE) {
                    component.sendState();
                }
            }
            else {
                LOG.warn("NetworkController", `received input control message with unknown path: ${inputMessage.path}`);
            }
            break;
        default:
            console.warn(`PROCESS: no behaviour implemented for type ${message.type}`);
            break;
    }
}
function registerComponent(component) {
    components[component.getAddress()] = component;
    // component.registerSenders(sendControlValue, sendControlState);
}
export const NC = { init, sendCTMessage, sendControlState, sendControlValue, registerComponent };
// export class ControlEndpoint<TComponent extends Nexus.Dial | Nexus.Button | Nexus.Toggle | Nexus.RadioButton, ValueType extends number | boolean> {
export class ControlEndpoint {
    constructor(address, componentDivId, config, defaultValue) {
        this.state = ControlState.BOTH;
        this.address = address;
        this.componentDivId = componentDivId;
        this.config = config;
        this.defaultKey = defaultValue[0];
        this.defaultValue = defaultValue[1];
    }
    /* Resets the given component back to its default config. */
    reset() {
        // Reset settings
        for (const key in this.config) {
            this.component[key] = this.config[key];
        }
        // Reset value
        this.component[this.defaultKey] = this.defaultValue;
    }
    ;
    fullConfig() {
        let fullConf = {};
        for (const key in this.config) {
            fullConf[key] = this.config[key];
        }
        fullConf["size"] = this.size;
        fullConf[this.defaultKey] = this.defaultValue;
        return fullConf;
    }
    resetValue() {
        let validReset = this.userUpdate(this.defaultValue);
        if (validReset) {
            this.updateComponent(this.defaultValue);
        }
    }
    ;
    getAddress() {
        return this.address;
    }
    networkStateUpdate(state) {
        this.state = state;
        this.colourize(state);
    }
    getNetworkState() {
        return this.state;
    }
    networkValueUpdate(value) {
        if (this.state == ControlState.BOTH || this.state == ControlState.NETWORK) {
            if ("step" in this.config) {
                const multiplicationFactor = Math.round(value / this.config["step"]);
                const newValue = this.config["step"] * multiplicationFactor;
                LOG.debug("NetworkController", `${this.address} step update from ${value} to ${newValue}`);
                value = newValue;
            }
            this.setValue(value);
            this.updateComponent(value);
        }
    }
    ;
    userUpdate(value) {
        if (this.state == ControlState.BOTH || this.state == ControlState.LOCAL || this.state == ControlState.DISCONNECTED) {
            this.setValue(value);
            this.sendValue(value);
            return true;
        }
        else {
            this.updateComponent(this.currentValue);
            return false;
        }
    }
    updateComponent(value) {
        this.component["_value"]["value"] = value;
        this.component.render();
    }
    sendValue(value) {
        if (this.state != ControlState.DISCONNECTED) {
            if (value == undefined) {
                value = this.getValue();
            }
            NC.sendControlValue(this.address, value);
        }
    }
    sendState(state) {
        if (state == undefined) {
            state = this.state;
        }
        NC.sendControlState(this.address, state);
    }
    colourize(state) {
        if (state == undefined) {
            state = this.state;
        }
        this.component.colorize("accent", ControlEndpoint.colourMap.get(state).get("accent"));
        this.component.colorize("fill", ControlEndpoint.colourMap.get(state).get("fill"));
    }
}
ControlEndpoint.colourMap = new Map([
    [ControlState.DISABLED, new Map([
            ["fill", "#707070"],
            ["accent", "#242423"]
        ])],
    [ControlState.DISCONNECTED, new Map([
            ["fill", "#EBEBEB"],
            ["accent", "#805D93"]
        ])],
    [ControlState.BOTH, new Map([
            ["fill", "#EBEBEB"],
            ["accent", "#22BBBB"]
        ])],
    [ControlState.LOCAL, new Map([
            ["fill", "#EBEBEB"],
            ["accent", "#FF934F"]
        ])],
    [ControlState.NETWORK, new Map([
            ["fill", "#EBEBEB"],
            ["accent", "#98CE00"]
        ])],
]);
export class ControlEndpointToggle extends ControlEndpoint {
    constructor(address, componentDivId, config, defaultValue, updateFunction) {
        super(address, componentDivId, config, defaultValue);
        this.size = [CFG.toggleWidth, CFG.toggleHeight];
        this.defaultKey = defaultValue[0];
        this.defaultValue = defaultValue[1];
        this.currentValue = this.defaultValue;
        this.lastValidValue = this.defaultValue;
        this.update = updateFunction;
    }
    create() {
        this.component = Nexus.Add.Toggle(this.componentDivId, this.fullConfig());
        this.component.on("change", (value) => this.userUpdate(value));
        this.setValue(this.defaultValue);
        this.colourize();
    }
    getValue() {
        return this.currentValue;
    }
    setValue(value) {
        // TODO: Need to fix this issue where we have a cyclic dependency due to a related toggle and dial
        this.currentValue = value;
        this.currentValue = this.update(value);
        return this.currentValue;
    }
}
export class ControlEndpointDial extends ControlEndpoint {
    constructor(address, componentDivId, componentValueDivId, config, defaultValue, updateFunction, unit) {
        super(address, componentDivId, config, defaultValue);
        // Unit of Measure
        this.unit = "";
        this.textValueDivId = componentValueDivId;
        if (this.textValueDivId != undefined) {
        }
        this.size = [CFG.dialWidth, CFG.dialHeight];
        this.currentValue = this.defaultValue;
        this.currentDisplayedValue = NaN;
        this.lastValidValue = this.defaultValue;
        this.update = updateFunction;
        if (unit != undefined) {
            this.unit = unit;
        }
    }
    create() {
        this.component = Nexus.Add.Dial(this.componentDivId, this.fullConfig());
        this.component.on("change", (value) => this.userUpdate(value));
        this.textValueButton = Nexus.Add.TextButton(this.textValueDivId, {
            "size": [CFG.resetWidth, CFG.resetHeight],
            "mode": "aftertouch",
            "text": "",
            "alternateText": "RESET"
        });
        this.textValueButton["mode"] = "aftertouch";
        this.updateValueButton();
        this.textValueButton.on("change", (touchDetails) => {
            if (touchDetails["state"] == false) {
                // Press released
                if (touchDetails["x"] > 0 && touchDetails["x"] < 1 &&
                    touchDetails["y"] > 0 && touchDetails["y"] < 1) {
                    // Press was released inside the button (didn't drag finger away)
                    this.resetValue();
                }
            }
        });
        this.setValue(this.defaultValue);
        this.colourize();
    }
    getValue() {
        return this.currentValue;
    }
    getFormattedValue() {
        return this.currentDisplayedValue;
    }
    setValue(value) {
        this.currentValue = value;
        this.currentDisplayedValue = this.update(value);
        this.updateValueButton();
        return this.currentDisplayedValue;
    }
    updateValueButton() {
        // Turns out they like to reset the styling every time the text updates...
        this.textValueButton.text = this.currentDisplayedValue.toFixed(1) + this.unit;
        this.textValueButton["textElement"]["style"]["font-size"] = CFG.valueResetFontSize;
        this.textValueButton["textElement"]["style"]["padding"] = "0px";
        this.textValueButton["textElement"]["style"]["border"] = `${CFG.valueResetBorderSize} solid ${ControlEndpoint.colourMap.get(this.state).get("accent")}`;
        this.textValueButton["textElement"]["style"]["line-height"] = PixelMaths.sub(this.textValueButton["textElement"]["style"]["height"], CFG.valueResetBorderSize);
    }
    colourize(state) {
        if (state == undefined) {
            state = this.state;
        }
        super.colourize(state);
        this.textValueButton.colorize("accent", ControlEndpoint.colourMap.get(state).get("accent"));
        this.textValueButton["textElement"]["style"]["border"] = `${CFG.valueResetBorderSize} solid ${ControlEndpoint.colourMap.get(state).get("accent")}`;
        this.textValueButton.colorize("fill", ControlEndpoint.colourMap.get(state).get("fill"));
    }
}
