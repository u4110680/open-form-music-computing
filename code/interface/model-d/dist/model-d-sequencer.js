// import * as Nexus from "./NexusUI.js";
// // import * as Tone from "./tone.js";
import { CFG } from "./model-d-cfg.js";
import { mapRange, getFrequency, LOG } from "./utils.js";
import { ControlEndpointDial, ControlEndpointToggle, NC } from "./network-controller.js";
class ModelDSequencer {
    constructor(parentDiv, parentID, numberOfColumns = 16, numberOfRows = 12) {
        this.configured = false;
        this.minOctaveDifference = 0;
        this.maxOctaveDifference = 7;
        this.octaveStep = 1 / ((this.maxOctaveDifference) - this.minOctaveDifference);
        this.startingOctave = 4 * (this.octaveStep);
        this.stepListeners = new Map();
        this.changeListeners = new Map();
        this.stepId = 0;
        this.changeId = 0;
        this.root = parentDiv;
        this.rootID = parentID;
        this.numCols = numberOfColumns;
        this.numRows = numberOfRows;
    }
    getBPM(value) {
        const minBPM = 1;
        const maxBPM = 200;
        if (value == undefined) {
            value = this.tempo.getValue();
        }
        return mapRange(value, 0, 1, minBPM, maxBPM);
    }
    getTempo(value) {
        const bpm = this.getBPM(value);
        const msInterval = 60000 / bpm;
        return msInterval;
    }
    setTempo(value) {
        const msInterval = this.getTempo(value);
        this.sequencer.interval.ms(msInterval);
        return this.getBPM(value);
    }
    // Returns the octave from the internal value which is 0 - 1
    getOctave(value) {
        if (value == undefined) {
            value = this.octave.getValue();
        }
        return Math.round(mapRange(value, 0, 1, this.minOctaveDifference, this.maxOctaveDifference));
    }
    getFrequencies(step) {
        let frequencies = [];
        const referenceFrequency = 261.63; // C4, default bottom note
        for (let i = 0; i < step.length; i++) {
            if (step[i] == 1) {
                const semitoneDifference = ((this.getOctave() - 4) * 12) + i;
                const freq = getFrequency(referenceFrequency, semitoneDifference);
                frequencies.push(freq);
            }
        }
        return frequencies;
    }
    powerToggle(on) {
        if (this.configured) {
            if (on) {
                LOG.info("seqOnOff", "starting sequencer");
                this.sequencer.start(this.getTempo());
            }
            else {
                LOG.info("seqOnOff", "stopping sequencer");
                this.sequencer.stop();
            }
            return on;
        }
        else {
            return false;
        }
    }
    updateStepMode(stepIndex) {
        if (this.configured) {
            if (stepIndex == undefined) {
                stepIndex = this.stepMode.active;
            }
            if (stepIndex < 0) {
                this.stepMode.active = this.stepMode.lastActive;
            }
            else {
                this.stepMode.lastActive = stepIndex;
                switch (stepIndex) {
                    case 0:
                        this.sequencer.stepper.mode = "up";
                        break;
                    case 1:
                        this.sequencer.stepper.mode = "down";
                        break;
                    case 2:
                        this.sequencer.stepper.mode = "drunk";
                        break;
                    case 3:
                        this.sequencer.stepper.mode = "random";
                        break;
                    default:
                        LOG.error("updateStepMode", `Sequencer requested unknown step mode: ${stepIndex}.`);
                }
            }
        }
    }
    create() {
        // ----- Note Grid
        this.sequencer = Nexus.Add.Sequencer(`#${this.rootID}SeqNoteGrid`, {
            "size": [CFG.sequencerWidth, CFG.sequencerHeight],
            "mode": "toggle",
            "rows": this.numRows,
            "columns": this.numCols,
        });
        this.sequencer.on("step", (value) => {
            this.stepListeners.forEach((f, key, map) => {
                f(value);
            });
        });
        this.sequencer.on("change", (value) => {
            this.changeListeners.forEach((f, key, map) => {
                f(value);
            });
        });
        this.sequencer.colorize("mediumLight", "#fffbbd");
        // ----- Tempo
        this.tempo = new ControlEndpointDial("/sequencer/tempo", `#${this.rootID}SeqTempo`, `#${this.rootID}SeqTempoValue`, { "min": 0, "max": 1 }, ["value", 0.7], (value) => { return this.setTempo(value); }, "bpm");
        this.tempo.create();
        NC.registerComponent(this.tempo);
        // ----- OnOff Toggle
        this.onOffToggle = new ControlEndpointToggle(`/sequencer/onOff`, `#${this.rootID}SeqOnOff`, {}, ["state", false], (on) => this.powerToggle(on));
        this.onOffToggle.create();
        NC.registerComponent(this.onOffToggle);
        // ----- Octave
        this.octave = new ControlEndpointDial("/sequencer/octave", `#${this.rootID}SeqOctave`, `#${this.rootID}SeqOctaveValue`, { "min": 0, "max": 1, "step": this.octaveStep }, ["value", this.startingOctave], (value) => { return this.getOctave(value); }, "oct");
        this.octave.create();
        NC.registerComponent(this.octave);
        // ------ Stepper Control
        // this.stepControl = 
        const numStepModeButtons = 4;
        const defaultStepMode = 0;
        const waveformLabels = ["F", "B", "D", "R"];
        let stepModeControl = Nexus.Add.RadioButton(`#${this.rootID}SeqStepMode`, {
            "size": [numStepModeButtons * CFG.blockWidth, CFG.blockHeight],
            "numberOfButtons": numStepModeButtons,
            "active": defaultStepMode
        });
        stepModeControl["lastActive"] = defaultStepMode;
        stepModeControl.on("change", (stepIndex) => this.updateStepMode(stepIndex));
        let stepModeCircles = document.querySelectorAll(`#${this.rootID}SeqStepMode div div span svg circle`);
        for (let j = 0; j < stepModeCircles.length; j++) {
            const circle = stepModeCircles.item(j);
            circle.insertAdjacentHTML("afterend", `<text x="50%" y="50%" dominant-baseline="central" text-anchor="middle" pointer-events="none" font-size="${CFG.buttonWidth * 0.3}px">${waveformLabels[j]}</text>`);
        }
        this.stepMode = stepModeControl;
        this.configured = true;
    }
    registerStepListener(f) {
        console.log("Registering Step Listener");
        let id = `step-${this.stepId}`;
        this.stepId = this.stepId + 1;
        this.stepListeners.set(id, f);
        return id;
    }
    registerChangeListener(f) {
        let id = `change-${this.changeId}`;
        this.changeId = this.stepId + 1;
        this.changeListeners.set(id, f);
        return id;
    }
    removeStepListener(key) {
        this.stepListeners.delete(key);
    }
    removeChangeListener(key) {
        this.changeListeners.delete(key);
    }
}
export { ModelDSequencer };
