import { LOG, rightSplit } from "./utils.js";
// Message functions
export var CTType;
(function (CTType) {
    CTType["NEW_CONNECTION"] = "NEW_CONNECTION";
    CTType["END_CONNECTION"] = "END_CONNECTION";
    CTType["NEW_PLAYER"] = "NEW_PLAYER";
    CTType["REMOVE_PLAYER"] = "REMOVE_PLAYER";
    CTType["INPUT_CONTROL"] = "INPUT_CONTROL";
    CTType["SERVER_CONTROL"] = "SERVER_CONTROL";
    CTType["GLOBAL_MESSAGE"] = "GLOBAL_MESSAGE";
    CTType["PRIVATE_MESSAGE"] = "PRIVATE_MESSAGE";
    CTType["HEARTBEAT"] = "HEARTBEAT";
    CTType["OTHER"] = "OTHER";
})(CTType || (CTType = {}));
export class CTMessage {
    constructor(sender, type, content) {
        this.sender = sender;
        this.type = type;
        this.content = content;
    }
    construct_message() {
        let ret = `${CTMessage._MESSAGE_START}${this.sender}${CTMessage._MESSAGE_SEPARATOR}${this.type.toString()}`;
        if (this.content != null) {
            ret += `${CTMessage._MESSAGE_SEPARATOR}${this.content}`;
        }
        ret += `${CTMessage._MESSAGE_END}`;
        return ret;
    }
    static parse_message(raw_message) {
        let sender;
        let messageType;
        let content = undefined;
        let ret = undefined;
        if (raw_message.startsWith(CTMessage._MESSAGE_START) &&
            raw_message.endsWith(CTMessage._MESSAGE_END)) {
            try {
                let temp_string = raw_message.substring(CTMessage._MESSAGE_START.length);
                temp_string = temp_string.substring(0, temp_string.length - CTMessage._MESSAGE_END.length);
                let entries = temp_string.split(CTMessage._MESSAGE_SEPARATOR);
                if (entries.length < 2) {
                    throw new Error("Message does not contain at least sender and type.");
                }
                sender = entries[0];
                messageType = entries[1];
                if (entries.length > 2) {
                    content = entries[2];
                }
                ret = new CTMessage(sender, messageType, content);
            }
            catch (err) {
                console.error(`PARSER: error parsing message '${raw_message}' - ${err}`);
            }
        }
        else {
            console.warn(`PARSER: message '${raw_message}' does not conform to CTMessage standard`);
        }
        return ret;
    }
}
CTMessage._MESSAGE_START = "[[";
CTMessage._MESSAGE_SEPARATOR = "%";
CTMessage._MESSAGE_END = "]]";
CTMessage._INPUT_CONTROL_SEPARATOR = "/";
export var ControlState;
(function (ControlState) {
    ControlState["DISABLED"] = "disabled";
    ControlState["DISCONNECTED"] = "disconnected";
    ControlState["LOCAL"] = "local";
    ControlState["NETWORK"] = "network";
    ControlState["BOTH"] = "both";
})(ControlState || (ControlState = {}));
/**
 * A specific type of CTMessage, the subtypes of this message are:
 * - VALUE: An update to the value of the given component from the server
 * - STATE: An update to the state of the given component from the server
 * - REQUEST_VALUE: A request for the current value of the component to be sent to the server
 * - REQUEST_STATE: A request for the current state of the component to be sent to the server
 */
export var InputControlType;
(function (InputControlType) {
    InputControlType["VALUE"] = "value";
    InputControlType["STATE"] = "state";
    InputControlType["REQUEST_VALUE"] = "request_value";
    InputControlType["REQUEST_STATE"] = "request_state";
})(InputControlType || (InputControlType = {}));
/**
 * An InputControl message has the form:
 *
 * `/path/to/component/InputControlType/value`
 *
 * where:
 *
 * - `/path/to/component` is a variable length path to the given component
 * - `/InputControlType` is the kind of InputControl message this is (second to last element)
 * - `/value` is the accompanying data transmitted with the InputControlType (last element, may be blank in instances of a request)
 */
export class InputControl {
    constructor(path, type, value) {
        this.path = path;
        this.type = type;
        if (Object.values(ControlState).some((cs) => cs === value)) {
            this.state = value;
        }
        else {
            this.value = value;
        }
    }
    static constructValueMessage(path, value) {
        return `${path}/${InputControlType.VALUE}/${value}`;
    }
    static constructStateMessage(path, state) {
        return `${path}/${InputControlType.STATE}/${state}`;
    }
    constructMessage() {
        if (this.value == undefined) {
            return InputControl.constructStateMessage(this.path, this.state);
        }
        else {
            return InputControl.constructValueMessage(this.path, this.value);
        }
    }
    static parseValue(rawValue) {
        if (rawValue.toLowerCase() === "true") {
            return true;
        }
        else if (rawValue.toLowerCase() === "false") {
            return false;
        }
        else if (rawValue.lastIndexOf(".") < 0) {
            return Number.parseInt(rawValue);
        }
        else {
            return Number.parseFloat(rawValue);
        }
    }
    static parseState(rawValue) {
        if (Object.values(ControlState).some((cs) => cs === rawValue)) {
            return rawValue;
        }
        else {
            LOG.error("InputControl", `unable to parse state: ${rawValue}`);
            return undefined;
        }
    }
    static parseMessage(message) {
        if (message == null || message == undefined) {
            LOG.warn("InputControl", "asked to parse a null / undefined message");
            return undefined;
        }
        if (message.type != CTType.INPUT_CONTROL) {
            LOG.warn("InputControl", "asked to parse a non-input control type message");
            return undefined;
        }
        if (message.content == null || message.content == undefined || message.content.length == 0) {
            LOG.warn("InputControl", "received an input control message with no content");
            return undefined;
        }
        const elements = rightSplit(message.content, CTMessage._INPUT_CONTROL_SEPARATOR, 2);
        if (elements.length <= 2) {
            LOG.warn("InputControl", `received an input control message with poorly formed content: ${message.content}`);
            return undefined;
        }
        const path = elements[0];
        const typeString = elements[1];
        let value;
        let type;
        if (Object.values(InputControlType).some((ict) => ict === typeString)) {
            type = typeString;
        }
        else {
            LOG.error("InputControl", `received an input control message with unknown type string: ${typeString}, full message: ${message.content}`);
            return undefined;
        }
        if (type == InputControlType.STATE) {
            value = InputControl.parseState(elements[2]);
        }
        else if (type == InputControlType.VALUE) {
            value = InputControl.parseValue(elements[2]);
        }
        else {
            // The other two types don't care about this field 
        }
        return new InputControl(path, type, value);
    }
}
