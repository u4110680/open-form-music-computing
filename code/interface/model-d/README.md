# Websocket client implementation

## Building

### Installation

To build the front end, you will need `make` and the TypeScript transpiler.

To install the typescript transpiler:
```bash
npm install typescript -g
```

### Compiling

The client is developed in `index.html` and `src/<SOURCE_FILE>.ts`.

To transpile please run:

```bash
make
```

## Running

Host the directory and navigate to the website.

You can also find the current client [here](http://bschuetze.xyz/app/).

The current default IP and Port for the server are `203.129.19.254` and `3310`.