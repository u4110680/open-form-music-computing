//const synth = new Tone.Synth().toMaster()
//synth.triggerAttackRelease('C4', '8n')

var power = new Nexus.Add.Toggle("#instrument");

const serverStatusElement = document.getElementById("serverStatus");
serverStatusElement.textContent = "Meme";

var harmDial = Nexus.Add.Dial('#instrument',{
  'size': [75,75],
  'value': 1.0,
  'min': 0,
  'max': 2,
});

var sliderLoopInterval = Nexus.Add.Slider('#instrument',{
  'size': [25,100],
  'value': 0.01,
  'min': 0.1,
  'max': 20
});

var sliderAttack = Nexus.Add.Slider('#instrument',{
  'size': [25,100],
  'value': 0.01,
  'min': 0,
  'max': 10
});

var sliderRelease = Nexus.Add.Slider('#instrument',{
  'size': [25,100],
  'value': 0.01,
  'min': 0,
  'max': 10
});


document.getElementById('button')?.addEventListener('click', async () => {
  setup();
})

async function setup() {
  await Tone.start();
  console.log('audio is ready');
}
  
const synth = new Tone.DuoSynth({
  oscillator: {
    type: "triangle"
  },
  envelope: {
    attack: 0.1
  }
}).toDestination();

var note = "C4";
var pressLength = 1;

const loop = new Tone.Loop(
  time => {
      console.log("Trigger", time)
      synth.triggerAttackRelease(note, pressLength);
  }, 
  "10s"
).start(0);

power.on('change',function(v) {
  v ? Tone.Transport.start() : Tone.Transport.stop();
});

harmDial.on('change',function(v) {
  synth.harmonicity.rampTo(v,.1)
})

sliderLoopInterval.on('change', function(v) {
  console.log("Loop Interval", v);
  loop.interval = v;
})

sliderAttack.on('change',function(v) {
  console.log("Attack", v);
  synth.voice0.envelope.attack = v;
  synth.voice1.envelope.attack = v;
}) 

sliderRelease.on('change',function(v) {
  console.log("Release", v);
  synth.voice0.envelope.release = v;
  synth.voice0.filterEnvelope.release = v;
  synth.voice1.envelope.release = v;
  synth.voice1.filterEnvelope.release = v;
});



