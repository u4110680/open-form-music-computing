from enum import Enum

class Protocol(str, Enum):
    TCP = "tcp"
    UDP = "udp"


def rreplace(source, target, replacement, replacements=None):
    """Same as .replace, except from the end of the string."""
    return replacement.join(source.rsplit(target, replacements))


def get_address_string(addr: tuple[str, int]):
    """Returns a human readable string from an addr returned by a socket connection."""
    if type(addr) == str:
        # Just incase it isn't a tuple for some reason
        return addr
    elif type(addr) == tuple:
        ret = ""
        if len(addr) >= 1:
            ret += f"{addr[0]}"
        if len(addr) >= 2:
            ret += f":{addr[1]}"
        if len(addr) >= 3:
            # Unknown data
            for i, data in enumerate(addr):
                if i < 2:
                    continue
                ret+= f"-{data}"
        return ret
    else:
        # Unknown type
        return f"UnknownAddrType({type(addr)})-{addr}"