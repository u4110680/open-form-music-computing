import logging as log
import typer

from enum import Enum

# Log levels
class Log_Level(Enum):
    DEBUG = "DEBUG"
    INFO  = "INFO"
    WARN  = "WARN"
    ERROR = "ERROR"
    
    def __int__(self):
        if self.value == self.DEBUG._value_:
            return log.DEBUG
        if self.value == self.INFO._value_:
            return log.INFO
        if self.value == self.WARN._value_:
            return log.WARN
        if self.value == self.ERROR._value_:
            return log.ERROR
        raise ValueError(f"{self.value} is an unknown Log_Level value.")


class Logger:
    def __init__(self, enable_file_logging: bool = False, enable_print_logging: bool = False, debug_level: Log_Level = Log_Level.INFO, log_file_path: str = "logger.log"):
        self._ENABLE_LOGGING = enable_file_logging
        self._ENABLE_PRINT_LOGGING = enable_print_logging
        self._DEBUG_LEVEL_TYPE = debug_level
        self._DEBUG_LEVEL = int(debug_level)
        self._LOG_FILENAME = log_file_path


    def update_config(self, enable_file_logging: bool = None, enable_print_logging: bool = None, debug_level: Log_Level = None, log_file_path: str = None):
        """Updates the config of the logger"""
        if enable_file_logging is not None:
            self._ENABLE_LOGGING = enable_file_logging

        if enable_print_logging is not None:
            self._ENABLE_PRINT_LOGGING = enable_print_logging

        if debug_level is not None:
            self._DEBUG_LEVEL_TYPE = debug_level
            self._DEBUG_LEVEL = int(debug_level)

        if log_file_path is not None:
            self._LOG_FILENAME = log_file_path

        self.init_logger()


    def init_logger(self):
        """Set up the logging"""
        if self._ENABLE_LOGGING:
            print(f"Enabling logging: Level = {self._DEBUG_LEVEL_TYPE}, filename = {self._LOG_FILENAME}")
            log.basicConfig(
                filename=self._LOG_FILENAME,
                level=self._DEBUG_LEVEL,
                format="%(levelname)s : %(relativeCreated).0f : %(message)s"
                # datefmt="%Y-%m-%d_%H-%M-%S"
            )


    def sys_out(self, text: str, nl: bool=True):
        """Standard printing function to be used."""
        if self._ENABLE_PRINT_LOGGING:
            typer.echo(text, nl=nl)


    def construct_string(self, *text: any):
        """Constructs a string from an arbitrary amount of arguments."""
        res = ""
        for i, item in enumerate(text):
            if i > 0:
                res = res + " "
            res = res + str(item)
        return res


    def style(self, *text: any, color: str = None, background_colour: str = None):
        """Returns a concatenated styled string from a list of inputs."""
        return typer.style(self.construct_string(*text), fg=color, bg=background_colour)


    def debug(self, name: str, *text: any):
        """Debug info printing function, colour codes the `name` as magenta."""
        output = self.style(name, color=typer.colors.MAGENTA) + ": " + self.construct_string(*text)
        if self._DEBUG_LEVEL <= log.DEBUG:
            self.sys_out(output)
        if self._ENABLE_LOGGING:
            log.debug(output)


    def info(self, name: str, *text: any):
        """Standard info printing function, colour codes the `name` as blue."""
        output = self.style(name, color="blue") + ": " + self.construct_string(*text)
        if self._DEBUG_LEVEL <= log.INFO:
            self.sys_out(output)
        if self._ENABLE_LOGGING:
            log.info(output)


    def success(self, name: str, *text: any):
        """Standard success printing function, colour codes the `name` as green."""
        output = self.style(name, color="green") + ": " + self.construct_string(*text)
        if self._DEBUG_LEVEL <= log.WARN: # Use warning level here
            self.sys_out(output)
        if self._ENABLE_LOGGING:
            log.warn(output)


    def warn(self, name: str, *text: any):
        """Standard warning printing function, colour codes the `name` as yellow."""
        output = self.style(name, color="yellow") + ": " + self.construct_string(*text)
        if self._DEBUG_LEVEL <= log.WARN:
            self.sys_out(output)
        if self._ENABLE_LOGGING:
            log.warn(output)


    def error(self, name: str, *text: any):
        """Standard error printing function, colour codes the `name` as red."""
        output = self.style(name, color="red") + ": " + self.construct_string(*text)
        if self._DEBUG_LEVEL <= log.ERROR:
            self.sys_out(output)
        if self._ENABLE_LOGGING:
            log.error(output)