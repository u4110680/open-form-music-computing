from asyncio import Lock, locks

class AsyncDict:
    """Dict that uses asyncio locks to make it "thread safe"."""
    def __init__(self, initialize: bool = True):
        self._dict = {}
        if initialize:
            self._lock = locks.Lock()
        else:
            self._lock = None

    def initialize(self):
        """Initializes the dictionary lock. Sometimes we want the 
        definition not to be where the dictionary is instantiated
        due to the context of the lock."""
        self._lock = locks.Lock()

    async def keys(self) -> list:
        """Get a list of keys from the dict."""
        async with self._lock:
            try:
                ks = self._dict.keys()
            except Exception as e:
                print("keys", e)
            finally:
                pass
        return ks

    async def put(self, key: any, value: any):
        """Put key:value in dict."""
        async with self._lock:
            try:
                self._dict[key] = value
            except Exception as e:
                print("put", e)
            finally:
                pass

    async def get(self, key, default=None):
        """Get value at key, if it doesn't exist get default."""
        async with self._lock:
            try:
                val = self._dict[key]
            except KeyError:
                val = default
            except Exception as e:
                print("get", e)
            finally:
                pass
        return val
    
    async def delete(self, key):
        """Remove a key from a dictionary."""
        async with self._lock:
            try:
                self._dict.pop(key, "mask_error")
            except Exception as e:
                print("delete", e)
            finally:
                pass

    async def acquire_lock(self):
        """Acquire the lock for this dict. MUST be followed by a release_lock call.
        Useful if you want to do some manual dict modification."""
        await self._lock.acquire()

    async def release_lock(self):
        """Releases the lock, should only be called after an acquire_lock call."""
        await self._lock.release()

    def get_dict(self):
        """Get the underlying dict. Can be used with the manual acquire and release lock calls
        for any manual handling necessary. For example updating a list behind a key without having to create a new copy of the list."""
        return self._dict

    def get_lock(self):
        """Get the underlying lock object. Can be used with the manual get_dict call for methods that aren't suitable for put / get."""
        return self._lock


class Async_Reversible_Dict:
    """Doubly linked dict (keys and values are unique) that uses asyncio locks to make it "thread safe".
    Functions just like a regular dict, however if a value already exists in the dict then the previous k:v
    mapping is removed."""
    def __init__(self, initialize: bool = True):
        self._dictkv = {}
        self._dictvk = {}
        if initialize:
            self._lock = locks.Lock()
        else:
            self._lock = None
    
    def initialize(self):
        """Initializes the dictionary lock. Sometimes we want the 
        definition not to be where the dictionary is instantiated
        due to the context of the lock."""
        self._lock = locks.Lock()

    async def keys(self) -> list:
        """Get a list of keys from the dict."""
        async with self._lock:
            try:
                ks = self._dictkv.keys()
            except Exception as e:
                print("keys", e)
            finally:
                pass
        return ks

    async def put(self, key: any, value: any):
        """Put key:value in dict. If key or value exists in dict already then previous mappings are both removed."""
        async with self._lock:
            try:
                if key in self._dictkv:
                    # Key already exists, need to delete old value from vk dict
                    old_value = self._dictkv[key]
                    self._dictvk.pop(old_value)
                if value in self._dictvk:
                    # Value already exists, need to delete old key
                    old_key = self._dictvk.pop(value)
                    self._dictkv.pop(old_key)
                self._dictkv[key] = value
                self._dictvk[value] = key
                
            except Exception as e:
                print("put", e)
            finally:
                pass

    async def get(self, key, default=None):
        """Get value at key, if it doesn't exist get default."""
        async with self._lock:
            try:
                val = self._dictkv[key]
            except KeyError:
                val = default
            except Exception as e:
                print("get", e)
            finally:
                pass
        return val

    async def get_reverse(self, value, default=None):
        """Get key at value, if it doesn't exist get default."""
        async with self._lock:
            try:
                key = self._dictvk[value]
            except KeyError:
                key = default
            except Exception as e:
                print("get_reverse", e)
            finally:
                pass
        return key
    
    async def delete(self, key):
        """Remove a key from a dictionary."""
        async with self._lock:
            try:
                old_value = self._dictkv.pop(key, "mask_error")
                self._dictvk.pop(old_value, "mask_error")
            except Exception as e:
                print("delete", e)
            finally:
                pass
    
    async def delete_value(self, value):
        """Remove a value from a dictionary."""
        async with self._lock:
            try:
                old_key = self._dictvk.pop(value, "mask_error")
                self._dictkv.pop(old_key, "mask_error")
            except Exception as e:
                print("delete_value", e)
            finally:
                pass

    async def acquire_lock(self):
        """Acquire the lock for this dict. MUST be followed by a release_lock call.
        Useful if you want to do some manual dict modification."""
        await self._lock.acquire()

    async def release_lock(self):
        """Releases the lock, should only be called after an acquire_lock call."""
        await self._lock.release()

    def get_dict(self):
        """Get the underlying dict. Can be used with the manual acquire and release lock calls
        for any manual handling necessary. For example updating a list behind a key without having to create a new copy of the list."""
        return self._dictkv
    
    def get_reversed_dict(self):
        """Get the underlying reversed dict. Can be used with the manual acquire and release lock calls
        for any manual handling necessary. For example updating a list behind a key without having to create a new copy of the list."""
        return self._dictvk

    def get_lock(self):
        """Get the underlying lock object. Can be used with the manual get_dict call for methods that aren't suitable for put / get."""
        return self._lock


class AsyncList:
    """List that uses asyncio locks to make it "thread safe"."""
    def __init__(self, initialize: bool = True):
        self._list: list[any] = []
        self._list.sort
        self._list.extend

        if initialize:
            self._lock = locks.Lock()
        else:
            self._lock = None

    def initialize(self):
        """Initializes the list lock. Sometimes we want the 
        definition not to be where the list is instantiated
        due to the context of the lock."""
        self._lock = locks.Lock()

    async def add(self, value: any, index: int = -1) -> None:
        """Add item to list (at index if specified, otherwise at the end of the list)"""
        async with self._lock:
            try:
                if index < 0:
                    self._list.append(value)
                else:
                    self._list.insert(index, value)
            except Exception as e:
                print("add", e)
            finally:
                pass

    async def extend(self, values: list[any]) -> None:
        """Add items to end of list"""
        async with self._lock:
            try:
                self._list.extend(values)
            except Exception as e:
                print("extend", e)
            finally:
                pass
    
    async def peak(self, index: int = -1) -> any:
        """Returns item at index if specified, otherwise returns element at the start of list"""
        value = None
        async with self._lock:
            try:
                if index < 0:
                    value = self._list[0]
                else:
                    value = self._list[index]
            except Exception as e:
                print("peak", e)
            finally:
                pass
        return value
    
    async def pop(self, index: int = -1) -> any:
        """Returns item at index if specified, otherwise returns element at the start of list. Removes item from list."""
        value = None
        async with self._lock:
            try:
                if index < 0:
                    value = self._list.pop(0)
                else:
                    value = self._list.pop(index)
            except Exception as e:
                print("peak", e)
            finally:
                pass
        return value
    
    async def sort(self, key=None, reverse: bool=False) -> list[any]:
        """Sorts the underlying list. Returns the underlying list after sorting"""
        async with self._lock:
            try:
                self._list.sort(key=key, reverse=reverse)
            except Exception as e:
                print("sort", e)
            finally:
                pass
        return self._list

    async def acquire_lock(self):
        """Acquire the lock for this dict. MUST be followed by a release_lock call.
        Useful if you want to do some manual dict modification."""
        await self._lock.acquire()

    async def release_lock(self):
        """Releases the lock, should only be called after an acquire_lock call."""
        await self._lock.release()

    def get_list(self) -> list[any]:
        """Get the underlying list. Can be used with the manual acquire and release lock calls
        for any manual handling necessary. For example updating a list behind a key without having to create a new copy of the list."""
        return self._list

    def get_lock(self) -> Lock:
        """Get the underlying lock object. Can be used with the manual get_list call for methods that aren't suitable for put / get."""
        return self._lock