from urllib import response
from webbrowser import get
from aioconsole import ainput
from config import ID_TO_NAME, LOG, ROUTES
from message import ForwardRule, ServerControl, ServerControlResponse, ServerControlType

current_rule_id: int = -1

def get_new_rule_id() -> int:
    """Get an ID for a new rule"""
    global current_rule_id
    current_rule_id = current_rule_id + 1
    return current_rule_id


async def get_connected_clients() -> list[str]:
    """All connected clients."""
    async with ID_TO_NAME.get_lock():
        try:
            users = list(ID_TO_NAME.get_reversed_dict().keys())
        except Exception as e:
            LOG.error("RulesEngine", f"unable to get connected clients: {e}")
            users = []
    return users


async def get_connected_players() -> list[str]:
    """All connected clients who are not controlling the server."""
    def user_filter(user: str) -> bool:
        """Helper function ro remove people from the connected list who are not players.
        - Special # starts with a 0
        - Controller # starts with a 1
        - Player # starts with 2 - 9"""
        user_number = user.split("#")[1]
        return (not user_number.startswith("0")) and (not user_number.startswith("1"))

    async with ID_TO_NAME.get_lock():
        try:
            users = list(ID_TO_NAME.get_reversed_dict().keys())
        except Exception as e:
            LOG.error("RulesEngine", f"unable to get connected players: {e}")
            users = []
    return list(filter(user_filter, users))


def get_list_of_commands() -> list[str]:
    """Returns a list of all possible commands."""
    return [
        "? : Display this information",
        "users : Display a list of currently connected users",
        "paths : Display a list of available paths for connection",
        "rules : Display a list of the current rules and their id's",
        "connect... : Connect two endpoints together",
        "disconnect ID : Remove rule with ID"
    ]


def get_list_of_paths() -> list[str]:
    """Returns a list of all possible paths."""
    paths = []
    for i in range(2):
        paths.append(f"/synth/osc{i}/onOff")
        paths.append(f"/synth/osc{i}/volume")
        if i > 0:
            paths.append(f"/synth/osc{i}/detune")
    paths.extend([
        "/synth/noise/onOff",
        "/synth/noise/type",
        "/synth/noise/volume",
        "/synth/filter/cutoff",
        "/synth/filter/emphasis",
        "/synth/filterEnvelope/attack",
        "/synth/filterEnvelope/decay",
        "/synth/filterEnvelope/sustain",
        "/synth/filterEnvelope/release",
        "/synth/amplitudeEnvelope/attack",
        "/synth/amplitudeEnvelope/decay",
        "/synth/amplitudeEnvelope/sustain",
        "/synth/amplitudeEnvelope/release",
        "/synth/output/onOff",
        "/synth/output/volume",
        "/sequencer/onOff",
        "/sequencer/tempo",
        "/sequencer/octave",
    ])
    return paths

async def get_current_rules() -> list[ForwardRule]:
    """Returns a list of the currently set rules."""
    async with ROUTES.get_lock():
        rules_dict = ROUTES.get_dict()
    
    rules: list[ForwardRule] = []
    for origin in rules_dict:
        for rule in rules_dict[origin]:
            rules.append(rule)
    return rules


async def create_rule(control_message: ServerControl, rule_id: int = None) -> ForwardRule:
    """Creates a rule based on the data in a server control message.
    If you don't plan to add this rule or have an existing id, then set the rule_id parameter 
    to avoid getting assigned a new id."""
    connection_parts = control_message.command_args
    if len(connection_parts) < 4:
        LOG.error("RuleEngine", f"server control connect message does not have min required elements: {connection_parts}")
        return None

    source_name = connection_parts[0]
    source_path = connection_parts[1]
    dest_name = connection_parts[2]
    dest_path = connection_parts[3]
    inverse = False
    message_delay = 0
    if len(connection_parts) >= 5:
        if connection_parts[4] == "inverse":
            inverse = True
        elif connection_parts[4] == "normal":
            inverse = False
        else:
            LOG.warn("RulesEngine", f"{connection_parts[4]} does not match 'inverse' or 'normal'")
            return None
    
    if len(connection_parts) >= 6:
        try:
            message_delay = float(connection_parts[5])
        except Exception:
            LOG.warn("RulesEngine", f"unable to parse float for message delay: {connection_parts[5]}")
            return None

    users = await get_connected_players()
    # LOG.debug("RulesEngine", "connected players:\n - " + "\n - ".join(users))
    paths = get_list_of_paths()
    if source_name not in users:
        LOG.warn("RulesEngine", f"source: {source_name} is not a known player")
    elif dest_name not in users:
        LOG.warn("RulesEngine", f"destination: {dest_name} is not a known player")
    elif source_path not in paths:
        LOG.warn("RulesEngine", f"source: {source_path} is not a known address")
    elif dest_path not in paths:
        LOG.warn("RulesEngine", f"destination: {dest_path} is not a known address")
    else:
        if rule_id is None:
            rule_id = get_new_rule_id()

        return ForwardRule(rule_id, source_name, source_path, dest_name, dest_path, inverse, message_delay)


async def add_rule(control_message: ServerControl) -> bool:
    """Adds a rule to the rule engine.
    Returns True if the rule was added successfully, False otherwise"""
    rule: ForwardRule = await create_rule(control_message)
    if rule is None:
        return False

    existing_source_rules = await ROUTES.get(rule.source_name)
    if existing_source_rules is None:
        existing_source_rules = []

    for existing_rule in existing_source_rules:
        if rule.matches(existing_rule):
            LOG.warn("RulesEngine", f"existing rule already present: {existing_rule}")
            return False
            
    existing_source_rules.append(rule)
    await ROUTES.put(rule.source_name, existing_source_rules)
    LOG.success("RulesEngine", f"adding rule {rule.id}: {rule}")
    return True


async def remove_rule(control_message: ServerControl) -> bool:
    """Removes a given rule if it exists"""
    rule_to_remove: ForwardRule = None

    if len(control_message.command_args) == 1:
        # Removal for ID based disconnect
        try:
            rule_id = int(control_message.command_args[0])
        except ValueError:
            LOG.warn("RulesEngine", f"unable to parse id: {control_message.command_args}")
            return False
    
        if rule_id > current_rule_id:
            LOG.warn("RulesEngine", f"rule {rule_id} doesn't exist")
            return False

        async with ROUTES.get_lock():
            routes_dict = ROUTES.get_dict()
            for dest in routes_dict:
                for rule in routes_dict[dest]:
                    if rule.id == rule_id:
                        rule_to_remove = rule
                        break
    else:
        connection_parts = control_message.command_args
        if len(connection_parts) < 4:
            LOG.error("RuleEngine", f"server control disconnect message does not have min required elements: {connection_parts}")
            return None

        source_name = connection_parts[0]
        source_path = connection_parts[1]
        dest_name = connection_parts[2]
        dest_path = connection_parts[3]
        inverse = False
        message_delay = 0
        if len(connection_parts) >= 5:
            if connection_parts[4] == "inverse":
                inverse = True
            elif connection_parts[4] == "normal":
                inverse = False
            else:
                LOG.warn("RulesEngine", f"{connection_parts[4]} does not match 'inverse' or 'normal'")
                return None
        
        if len(connection_parts) >= 6:
            try:
                message_delay = float(connection_parts[5])
            except Exception:
                LOG.warn("RulesEngine", f"unable to parse float for message delay: {connection_parts[5]}")
                return None
        
        disconnection_rule = ForwardRule(0, source_name, source_path, dest_name, dest_path, inverse, message_delay)
        async with ROUTES.get_lock():
            routes_dict = ROUTES.get_dict()
            for dest in routes_dict:
                for rule in routes_dict[dest]:
                    if disconnection_rule.matches(rule):
                        rule_to_remove = rule
                        break
        

    if rule_to_remove is None:
        LOG.warn("RulesEngine", f"unable to create rule from message args: {control_message.command_args}")
        return False

    existing_source_rules: list[ForwardRule] = await ROUTES.get(rule_to_remove.source_name)
    if existing_source_rules is None:
        LOG.warn("RulesEngine", f"no rules for {rule_to_remove.source_name} exist")
        return False

    matching_rule_index: int = -1
    for rule_index, existing_rule in enumerate(existing_source_rules):
        if rule_to_remove.matches(existing_rule) or rule_to_remove.id == existing_rule.id:
            matching_rule_index = rule_index
            break

    if matching_rule_index < 0:
        LOG.warn("RulesEngine", f"no matching rule found for {rule_to_remove}")
        return False

    existing_source_rules.pop(matching_rule_index)
    await ROUTES.put(rule_to_remove.source_name, existing_source_rules)
    LOG.success("RulesEngine", f"removed rule {rule_to_remove}")
    return True


async def handle_command(control_message: ServerControl) -> ServerControlResponse:
    """Handles the given command and returns the response message to be forwarded back to the sender."""
    response_data: list[str] = None
    if control_message.control_type == ServerControlType.HELP:
        response_data = get_list_of_commands()

    elif control_message.control_type == ServerControlType.USERS:
        users = await get_connected_players()
        if len(users) == 0:
            response_data = ["No users present"]
        else:
            response_data = users

    elif control_message.control_type == ServerControlType.PATHS:
        paths = get_list_of_paths()
        if len(paths) == 0:
            response_data = ["No paths present"]
        else:
            response_data = get_list_of_paths()

    elif control_message.control_type == ServerControlType.RULES:
        current_rules = await get_current_rules()
        if len(current_rules) == 0:
            response_data = ["No rules present"]
        else:
            response_data = [x.__str__() for x in current_rules]

    elif control_message.control_type == ServerControlType.CONNECT:
        add_result: bool = await add_rule(control_message)
        if add_result:
            response_data = ["Rule added successfully"]
        else:
            response_data = ["Rule failed to add"]

    elif control_message.control_type == ServerControlType.DISCONNECT:
        remove_result: bool = await remove_rule(control_message)
        if remove_result:
            response_data = ["Rule removed successfully"]
        else:
            response_data = ["Rule failed to remove"]

    else:
        LOG.error(f"Unknown command type: {control_message.control_type}")
        response_data = f"Unknown command type: {control_message.control_type}"

    if response_data is not None:
        return ServerControlResponse(control_message.id, control_message.control_type, response_data)
    else:
        return None

async def command_interface():
    current_rules: list[ForwardRule] = []
    connect_message_structure = "connect command has structure 'connect SOURCE ADDRESS DESTINATION ADDRESS [normal | inverse]'"
    while True:
        try:
            command: str = await ainput("Enter Command ('?' for a list of commands): ")
            print()
            if command == "?" or command == "help":
                print("Commands:\n  - " + "\n  - ".join(get_list_of_commands()))

            elif command == "users":
                users = await get_connected_clients()
                if len(users) == 0:
                    print("No connected users")
                else:
                    print("Users:\n  - " + "\n  - ".join(users))

            elif command == "paths":
                paths = get_list_of_paths()
                print("Paths:\n  - " + "\n  - ".join(paths))

            elif command == "rules":
                if len(current_rules) == 0:
                    print("No current rules")
                else:
                    print("Rules:")
                    for rule_id, rule in enumerate(current_rules):
                        print(f"{rule_id}: {rule}")

            elif command.startswith("connect"):
                connection_parts = command.split(" ")
                if len(connection_parts) < 5:
                    print(connect_message_structure)
                else:
                    source_name = connection_parts[1]
                    source_path = connection_parts[2]
                    dest_name = connection_parts[3]
                    dest_path = connection_parts[4]
                    inverse = False
                    if len(connection_parts) >= 6:
                        if connection_parts[5] == "inverse":
                            inverse = True
                        elif connection_parts[5] == "normal":
                            inverse = False
                        else:
                            print(f"{connection_parts[5]} does not match 'inverse' or 'normal'\n")
                            continue
                    users = await get_connected_clients()
                    paths = get_list_of_paths()
                    if source_name not in users:
                        print(f"{source_name} is not a known user")
                    elif dest_name not in users:
                        print(f"{dest_name} is not a known user")
                    elif source_path not in paths:
                        print(f"{source_path} is not a known address")
                    elif dest_path not in paths:
                        print(f"{dest_path} is not a known address")
                    else:
                        rule_id = len(current_rules)
                        rule = ForwardRule(rule_id, source_name, source_path, dest_name, dest_path, inverse)
                        existing_source_rules = await ROUTES.get(source_name)
                        if existing_source_rules is None:
                            existing_source_rules = []
                        matching_rule_exists = False
                        for existing_rule in existing_source_rules:
                            if rule.matches(existing_rule):
                                print(f"Existing rule already present: {existing_rule}")
                                matching_rule_exists = True
                                break
                        if not matching_rule_exists:
                            # Actually need to add the rule
                            current_rules.append(rule)
                            existing_source_rules.append(rule)
                            await ROUTES.put(source_name, existing_source_rules)
                            print(f"Adding rule {rule_id}: {rule}")

            elif command.startswith("disconnect"):
                try:
                    rule_id = int(command.split(" ")[-1])
                except ValueError:
                    print(f"Unable to parse id: {command.split(' ')[-1]}")
                    continue
                if rule_id >= len(current_rules):
                    print(f"Rule {rule_id} doesn't exist")
                else:
                    rule_to_remove = current_rules[rule_id]
                    existing_source_rules = await ROUTES.get(rule_to_remove.source_name)
                    if existing_source_rules is None:
                        existing_source_rules = []
                    matching_rule_index = -1
                    for rule_index, existing_rule in enumerate(existing_source_rules):
                        if rule_to_remove.matches(existing_rule):
                            matching_rule_index = rule_index
                            break
                    if matching_rule_index >= 0:
                        print(f"Removing rule {rule_id}: {rule_to_remove}")
                        current_rules.pop(rule_id)
                        existing_source_rules.pop(matching_rule_index)
                        await ROUTES.put(rule_to_remove.source_name, existing_source_rules)        

            else:
                print(f"Unknown command: '{command}'")
        except KeyboardInterrupt as e:
            exit: str = await ainput("Exit? (y/n) ")
            if exit.lower == "y":
                break
        except Exception as e:
            print(f"Error during command handling {e}")
        print() # Line Buffer