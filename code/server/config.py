import asyncio
import datetime as dt

from async_dict import AsyncDict, Async_Reversible_Dict, AsyncList
from logger import Logger, Log_Level

# Config file, houses nasty global variables

### GLOBAL VARIABLES
START_DT = dt.datetime.now()
START_TIMESTAMP = START_DT.strftime("%Y-%m-%d_%H-%M-%S")
SERVER_NAME = "SERVER#0000"

def configure_logger(enable_logging: bool, enable_print_logging: bool, log_level: Log_Level, log_path: str):
    """Inits the global logger variable"""
    global LOG
    LOG.update_config(enable_logging, enable_print_logging, log_level, log_path)

LOG: Logger = Logger()

RECEIVE_PIPE: asyncio.Queue = asyncio.Queue(0)
SENDER_PIPES: AsyncDict = AsyncDict(False)
CONNECTION_STATUS: AsyncDict = AsyncDict(False)
ID_TO_NAME: Async_Reversible_Dict = Async_Reversible_Dict(False)

# Delay pipe, stores messages that need to be delayed.
# Messages are stored as a 3 tuple [release_time, destination, message]
DELAY_PIPE: AsyncList = AsyncList(False)

# Takes the form of 
# key: name
# value: list(ForwardRule)
ROUTES: AsyncDict = AsyncDict(False)

HEARTBEAT_DELAY: int = 10
