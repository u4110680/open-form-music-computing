# Websocket server implementation

## Installation

Requires poetry

```bash
poetry install
```

## Running

```bash
python websock_server.py
```

View options with:

```bash
python websock_server.py --help
```