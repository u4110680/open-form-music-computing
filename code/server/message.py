from __future__ import annotations
from enum import Enum
from config import LOG

from utils import rreplace

class Message:
    def __init__(self, sender: str, destination: str, content: str):
        self.sender = sender
        self.destination = destination
        self.content = content


class ForwardRule:
    def __init__(self, rule_id: int, source_name: str, source_path: str, dest_name: str, dest_path: str, inverse: bool = False, message_delay: float = 0):
        self.id = rule_id
        self.source_name = source_name
        self.source_path = source_path
        self.dest_name = dest_name
        self.dest_path = dest_path
        self.inverse = inverse
        self.message_delay = message_delay
    
    def matches(self, rule: ForwardRule) -> bool:
        """Checks whether the given rule is equivalent to the current rule."""
        # NOTE: Doesn't consider inverse, its purely just the input output maps
        return (self.source_name == rule.source_name and 
                self.source_path == rule.source_path and
                self.dest_name == rule.dest_name and
                self.dest_path == rule.dest_path)
            
    def __str__(self):
        return f"ForwardRule({self.id}, {self.source_name}, {self.source_path}, {self.dest_name}, {self.dest_path}, {'inverse' if self.inverse else 'normal'}, {self.message_delay})"


class ControlState(Enum):
    DISABLED = "disabled"
    DISCONNECTED = "disconnected"
    LOCAL = "local"
    NETWORK = "network"
    BOTH = "both"


class InputControlType(Enum):
    VALUE = "value"
    STATE = "state"
    REQUEST_VALUE = "request_value"
    REQUEST_STATE = "request_state"


class InputControl:
    def __init__(self, path: str, control_type: InputControlType, value: bool | float, state: ControlState):
        self.path: str = path
        self.control_type: InputControlType = control_type 
        self.value: bool | float = value
        self.state: ControlState = state
    
    def matches_rule(self, forward_rule: ForwardRule) -> bool:
        return self.path == forward_rule.source_path
    
    def get_transformation(self, forward_rule: ForwardRule) -> InputControl:
        """Transforms the current InputControl based on the given forward rule."""
        transformed_control: InputControl = InputControl(forward_rule.dest_path, self.control_type, self.value, self.state)
        if self.control_type == InputControlType.STATE:
            if forward_rule.inverse:
                transformed_control.value = not transformed_control.value
        elif self.control_type == InputControlType.VALUE:
            if forward_rule.inverse:
                transformed_control.value = 1 - transformed_control.value
        return transformed_control

    @staticmethod
    def construct_value_message(path: str, value: bool | float) -> str:
        return f"{path}/{InputControlType.VALUE.value}/{value}"
    
    @staticmethod
    def construct_state_message(path: str, state: ControlState) -> str:
        return f"{path}/{InputControlType.STATE.value}/{state}"

    def construct_message(self) -> str:
        if self.value == None:
            return InputControl.construct_state_message(self.path, self.state)
        else:
            return InputControl.construct_value_message(self.path, self.value)
    
    def update_path(self, new_path: str):
        self.path = new_path

    @staticmethod
    def parse_value(raw_value: str) -> bool | float:
        if raw_value.lower() == "true":
            return True
        elif raw_value.lower() == "false":
            return False
        elif "." in raw_value:
            return float(raw_value)
        else:
            return int(raw_value)
    
    @staticmethod
    def parse_state(raw_value: str) -> ControlState:
        return ControlState(raw_value)

    @staticmethod
    def parse_message(message: CTMessage) -> InputControl:
        if message == None:
            LOG.warn("InputControl", "asked to parse a null / undefined message")
            return None

        if message.get_message_type() != CTType.INPUT_CONTROL:
            LOG.warn("InputControl", "asked to parse a non-input control type message")
            return None

        if message.get_message_content() == None or len(message.get_message_content()) == 0:
            LOG.warn("InputControl", "received an input control message with no content")
            return None

        elements: list[str] = message.get_message_content().rsplit(CTMessage._INPUT_CONTROL_SEPARATOR, 2)
        if len(elements) <= 2:
            LOG.warn("InputControl", f"received an input control message with poorly formed content: {message.get_message_content()}")
            return None
        
        message_path: str = elements[0]
        type_string: str = elements[1]
        value: bool | float = None
        state: ControlState = None
        control_type: InputControlType

        try:
            control_type = InputControlType(type_string)
        except TypeError as e:
            LOG.error("InputControl", f"received an input control message with unknown type string: {type_string}, full message: {message.get_message_content()}")
            return None

        if control_type == InputControlType.STATE:
            state = InputControl.parse_state(elements[2])
        elif control_type == InputControlType.VALUE:
            value = InputControl.parse_value(elements[2])
        else:
            # TODO: Huh?
            pass

        return InputControl(message_path, control_type, value, state)


class ServerControlType(Enum):
    HELP = "help"
    USERS = "users"
    PATHS = "paths"
    RULES = "rules"
    CONNECT = "connect"
    DISCONNECT = "disconnect"


class ServerControl:

    def __init__(self, sender: str, control_id: str, control_type: ServerControlType, command_args: list[str]):
        self.sender: str = sender
        self.id = control_id
        self.control_type: ServerControlType = control_type
        self.command_args: list[str] = command_args

    @staticmethod
    def parse_message(message: CTMessage) -> ServerControl:
        if message == None:
            LOG.warn("ServerControl", "asked to parse a null / undefined message")
            return None

        if message.get_message_type() != CTType.SERVER_CONTROL:
            LOG.warn("ServerControl", "asked to parse a non-server control type message")
            return None

        if message.get_message_content() == None or len(message.get_message_content()) == 0:
            LOG.warn("ServerControl", "received a server control message with no content")
            return None

        elements: list[str] = message.get_message_content().split(CTMessage._SERVER_CONTROL_SEPARATOR)

        if len(elements) < 2:
            LOG.warn("ServerControl", f"received a poorly formed server control message: {message.get_message_content()}")
            return None

        control_id = elements[0]
        type_string = elements[1]
        try:
            control_type = ServerControlType(type_string)
        except TypeError as e:
            LOG.error("ServerControl", f"received a server control message with unknown type string: {type_string}, full message: {message.get_message_content()}")
            return None
        
        if len(elements) > 2:
            command_args = elements[2:]
        else:
            command_args = None

        return ServerControl(message.get_sender(), control_id, control_type, command_args)


class ServerControlResponse:

    def __init__(self, control_id: str, control_type: ServerControlType, response: list[str]):
        self.id: str = control_id
        self.control_type: ServerControlType = control_type
        self.response: list[str] = response

    def to_message_string(self) -> str:
        if self.control_type == None:
            LOG.warn("ServerControlResponse", "asked to construct a null / undefined response type")
            return None

        if self.response == None:
            LOG.warn("ServerControlResponse", "asked to construct null / undefined response data")
            return None

        return f"{self.id}{CTMessage._SERVER_CONTROL_SEPARATOR}{self.control_type.value}{CTMessage._SERVER_CONTROL_SEPARATOR}" + CTMessage._SERVER_CONTROL_SEPARATOR.join(self.response)


class CTType(Enum):
    NEW_CONNECTION  = "NEW_CONNECTION"
    END_CONNECTION  = "END_CONNECTION"
    NEW_PLAYER      = "NEW_PLAYER"
    REMOVE_PLAYER   = "REMOVE_PLAYER"
    INPUT_CONTROL   = "INPUT_CONTROL"
    SERVER_CONTROL  = "SERVER_CONTROL"
    SERVER_CONTROL_RESPONSE  = "SERVER_CONTROL_RESPONSE"
    GLOBAL_MESSAGE  = "GLOBAL_MESSAGE"
    PRIVATE_MESSAGE = "PRIVATE_MESSAGE"
    HEARTBEAT       = "HEARTBEAT"
    OTHER           = "OTHER"


class CTMessage:
    _MESSAGE_START: str = "[["
    _MESSAGE_SEPARATOR: str = "%"
    _MESSAGE_END: str = "]]"
    _INPUT_CONTROL_SEPARATOR: str = "/"
    _SERVER_CONTROL_SEPARATOR: str = "|"

    def __init__(self, message: Message = None):
        self._raw_message: Message = message

        self.sender: str = None
        self.message_type: CTType = None
        self.message_content: str = None

        if self._raw_message is not None:
            self._interpret_message()

    def get_sender(self):
        if self.sender is None:
            try:
                # Need to figure it out
                message_str: str = self._get_stripped_message()
                split_message = message_str.split(self._MESSAGE_SEPARATOR)
                self.sender = split_message[0]
            except TypeError as e:
                # Encountered None when parsing
                raise e
        
        return self.sender

    def get_message_type(self):
        if self.message_type is None:
            # Need to figure it out
            try:
                message_str: str = self._get_stripped_message()
                split_message = message_str.split(self._MESSAGE_SEPARATOR)
                self.message_type = CTType(split_message[1])
            except TypeError as e:
                # Encountered None when parsing
                raise e

        return self.message_type

    def get_message_content(self):
        if self.message_content is None:
            if self._raw_message is None:
                return None
            # Need to figure it out
            message_str: str = self._get_stripped_message()
            try:
                split_message = message_str.split(self._MESSAGE_SEPARATOR)
                if len(split_message) > 2:
                    self.message_content = split_message[2]
                else:
                    self.message_content = None
            except TypeError as e:
                # Encountered None when parsing
                raise e

        return self.message_content
    
    def create_message(self, sender: str, type: CTType, content: str=None):
        self.sender = sender
        self.message_type = type
        self.message_content = content
        self._raw_message = self._construct_message()
    
    def get_raw_message(self) -> Message:
        return self._raw_message
    
    def _construct_message(self) -> Message:
        rm: str = f"{self._MESSAGE_START}{self.sender}{self._MESSAGE_SEPARATOR}{self.message_type.value}"
        if self.message_content is not None:
            rm += f"{self._MESSAGE_SEPARATOR}{self.message_content}"
        rm += self._MESSAGE_END
        return Message(self.sender, None, rm)
    
    def _get_stripped_message(self) -> str:
        message_str: str = "" + self._raw_message.content
        if (not message_str.startswith(self._MESSAGE_START) or 
            not message_str.endswith(self._MESSAGE_END)):
            raise ValueError("Message doesn't contain both start and stop data.")
        message_str = message_str.replace(self._MESSAGE_START, "", 1)
        message_str = rreplace(message_str, self._MESSAGE_END, "", 1)
        return message_str

    def _interpret_message(self):
        self.get_sender()
        self.get_message_type()
        self.get_message_content()