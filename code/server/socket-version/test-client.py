import datetime as dt
import logging as log
import multiprocessing as mp
import socket
import sys
import time
import typer
import uuid


SERVER_IP = "127.0.0.1"
SERVER_PORT = 4001


def getsocket(prot):
    if prot == "udp":
        return socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # INET, UDP
    else: # prot == "tcp"
        return socket.socket(socket.AF_INET, socket.SOCK_STREAM) # INET, TCP


sock = getsocket("tcp")
sock.connect((SERVER_IP, SERVER_PORT))
print("connected")
while True:
    sock.sendall("HB".encode("utf-8"))
    print("sent HB")
    message = sock.recv(4096)
    print("got", message)
    time.sleep(10)
    sock.sendall("meme".encode("utf-8"))
    print("sent meme")