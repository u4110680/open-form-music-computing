### SOCKETS
import multiprocessing as mp
import select
import socket

from utils import *

class MP_socket:
    """
    Multiprocessing safe socket wrapper for a sender / receiver socket 
    (socket returned from a new connection).
    """
    def __init__(self, host: str, port: int, protocol: Protocol, socket: socket.socket, address: socket.AddressInfo):
        self.host = host
        self.port = port
        self.protocol = protocol
        self.addr = address

        self.__socket = socket
        self.__lock = None
    
    def register_lock(self, lock: mp.Lock):
        """Registers the lock instance with the socket.
        This can't be done automatically as the locks need to be passed through thread 
        constructors explicitly."""
        self.__lock = lock

    def send_message(self, message: str, timeout: float = None):
        """Passes a message down a connection in utf-8 encoding."""
        select.select([], [self.__socket], [], timeout)
        # Wait until the socket is ready to write and then get the lock and send message
        self.__lock.acquire(block=True)
        message = message + "\n"
        if self.protocol == Protocol.UDP:
            self.__socket.sendto(message.encode("utf-8"), (self.host, self.port))
        else: # protocol == Protocol.TCP
            self.__socket.send(message.encode("utf-8"))
        self.__lock.release()
    
    def receive_message(self, buffer_size: int = 4096, timeout: float = None):
        """Receive a message from the pipe in `buffer_size` byte chunks."""
        message = b""
        # Wait until there is actually something to read, once there is we acquire the lock and read
        select.select([self.__socket], [], [], timeout)
        self.__lock.acquire(block=True)
        while True:
            bytes_received = self.__socket.recv(buffer_size)
            message += bytes_received
            if len(bytes_received) < buffer_size:
                # Got everything there is to offer
                break
        self.__lock.release()
        # message = message.decode("utf-8")
        return message

    def set_timeout(self, timeout: float):
        """Set the timeout value on the socket."""
        self.__socket.settimeout(timeout)
    
    def connect(self):
        """Connects the socket to the previously set address."""
        self.__socket.connect(self.addr)
    
    def close(self):
        """Close the socket connection."""
        self.__lock.acquire(block=True)
        self.__socket.shutdown(0)
        self.__socket.close()
        self.__lock.release()
