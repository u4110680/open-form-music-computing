import datetime as dt
import base64
import email
import logging as log
import multiprocessing as mp
import select
import socket
import string
import sys
import time
import typer
import uuid

from io import StringIO
from mp_socket import *
from hashlib import sha1
from utils import *

### GLOBAL VARIABLES
START_DT = dt.datetime.now()
START_TIMESTAMP = START_DT.strftime("%Y-%m-%d_%H-%M-%S")
LOG_FILENAME = f"logs/server-logs_{START_TIMESTAMP}.log"
HOST = ""
ENABLE_LOGGING = False
DEBUG_LEVEL = log.INFO


def main(
    port: int = typer.Option(4001, help="Port for the server to listen to incoming connections."),
    protocol: Protocol = typer.Option(Protocol.TCP, help="Protocol to use for server connections."),
    enable_logging: bool = typer.Option(False, help="Enable / Disable logging to file")
):
    global ENABLE_LOGGING

    # Sort logging
    ENABLE_LOGGING = enable_logging
    if ENABLE_LOGGING:
        info("SERVER", f"writing to log file '{LOG_FILENAME}'")
        init_logger()

    # Start server
    try:
        if protocol == Protocol.UDP:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        else:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.bind((HOST, port))
        sock.listen(10)
    except socket.error as e:
        error("SERVER", e)
        sys.exit(1)
    
    # Each connection will have its own listener
    threads = {}
    receive_queue = mp.Queue()
    new_connections = mp.Queue()
    sender_queues = {}

    info("SERVER", f"starting up at {START_TIMESTAMP}, listening to port {port} for new connections")
    
    # MAIN WELCOME LOOP
    welcomer = mp.Process(target=welcome, args=(sock, port, protocol, new_connections))
    welcomer.start()

    while True:
        try:
            # Main loop, here we want to initialize new connections that have been welcomed by the welcomer 
            # and also pass messages that we have received
            r_list, _, _ = select.select([new_connections._reader, receive_queue._reader], [], [])
            for ent in r_list:
                if ent is new_connections._reader:
                    # New connection to add
                    send_queue = mp.Queue()
                    id, new_socket = new_connections.get()
                    new_lock = mp.Lock()
                    sender_queues[id] = send_queue
                    threads.setdefault(id, [])
                    threads[id].append(mp.Process(target=sender, args=(id, new_socket, new_lock, send_queue)))
                    threads[id].append(mp.Process(target=receiver, args=(id, new_socket, new_lock, receive_queue)))
                    threads[id].append(mp.Process(target=heartbeat, args=(id, new_socket, new_lock)))
                    threads[id][0].start()
                    threads[id][1].start()
                    threads[id][2].start()
                if ent is receive_queue._reader:
                    message: Message = receive_queue.get()
                    # Pass message to anyone who isn't the sender
                    for id in sender_queues:
                        if id == message.sender:
                            # Don't send it back to yourself
                            continue
                        sender_queues[id].put(message, block=True)
        except KeyboardInterrupt as e:
            break

    # Clean up
    for id in threads:
        threads[id][0].kill()
        threads[id][1].kill()
        threads[id][2].kill()
    
    welcomer.kill()


### Welcomer
def welcome(sock: socket.socket, port: int, protocol: Protocol, new_connections: mp.Queue):
    """Listens for new connections on the main port, performs the necessary handshake and passes those along to the backend."""
    # MAIN WELCOME LOOP
    while True:
        # Main listen loop
        try:
            conn, addr = sock.accept()
            # handshake(conn, addr)
            new_socket = MP_socket(HOST, port, protocol, conn, addr)
        except KeyboardInterrupt:
            info("SERVER", "exiting due to keyboard interrupt")
            sock.shutdown(0)
            sock.close()
            break

        id = uuid.uuid1()
        new_connections.put([id, new_socket])

        info("SERVER", f"received connection from {addr}")


# GET / HTTP/1.1
# Host: 127.0.0.1:4001
# Connection: Upgrade
# Pragma: no-cache
# Cache-Control: no-cache
# User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36
# Upgrade: websocket
# Origin: http://127.0.0.1:5500
# Sec-WebSocket-Version: 13
# Accept-Encoding: gzip, deflate, br
# Accept-Language: en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7
# Sec-WebSocket-Key: yf2u9VxBYCEUkJgNanbqNA==
# Sec-WebSocket-Extensions: permessage-deflate; client_max_window_bits

# {
#   'Request': 'GET', 
#   'Page': '/', 
#   'Type': 'HTTP/1.1'
#   'Host': '127.0.0.1:4001', 
#   'Connection': 'Upgrade', 
#   'Pragma': 'no-cache', 
#   'Cache-Control': 'no-cache', 
#   'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.0.0 Safari/537.36', 
#   'Upgrade': 'websocket', 
#   'Origin': 'http://127.0.0.1:5500', 
#   'Sec-WebSocket-Version': '13', 
#   'Accept-Encoding': 'gzip, deflate, br', 
#   'Accept-Language': 'en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7', 
#   'Sec-WebSocket-Key': '3lqgVgXqCCMZtv8000EkZw==', 
#   'Sec-WebSocket-Extensions': 'permessage-deflate; client_max_window_bits', 
# }


def handshake(conn: socket.socket, addr: tuple[str, int]):
    """Performs the necessary handshake for welcoming web clients"""
    message = receive_message(conn, addr)
    info("HANDSHAKE", f"From {get_address_string(addr)}, Got:\n{message}")
    # Convert request to an easier to parse dict
    request, headers = message.split('\r\n', 1)
    message = email.message_from_file(StringIO(headers))
    headers = dict(message.items())
    request_params = request.strip("\n").strip("\r").split(" ")
    headers["Request"] = request_params[0]
    headers["Page"] = request_params[1]
    headers["Type"] = request_params[2]
    accept_str = get_websocket_key(headers["Sec-WebSocket-Key"])
    resp = f"HTTP/1.1 101 Switching Protocols\r\nUpgrade: websocket\r\nConnection: Upgrade\r\nSec-WebSocket-Accept: {accept_str}\r\nSec-WebSocket-Protocol: tcp\r\n\r\n"
    info("HANDSHAKE", f"Sending to {get_address_string(addr)}:\n{resp}")
    send_message(conn, addr, resp)
    info("HANDSHAKE", f"Completed handshake with {get_address_string(addr)}")


def get_websocket_key(input_key: str):
    """Creates the encoded response string for replying to web requests."""
    magic_string = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
    combined = input_key + magic_string
    sha = sha1(combined.encode("utf-8")).digest()
    return base64.b64encode(sha).decode("utf-8")


### Backend

class Message:
    def __init__(self, sender, destination, content):
        self.sender = sender
        self.destination = destination
        self.content = content


def backend(receive_queue: mp.Queue, new_contacts: mp.Queue):
    """Takes messages from the receive queue and gets them to where they need to go.
    Takes in new destinations from the new_contacts queue."""
    sender_queues = {}

    while True:
        message: Message = receive_queue.get()

        while True:
            # Get any new id queues
            try:
                new_entry = new_contacts.get(block=False)
                id = new_entry[0]
                sender_queues[id] = new_entry[1]
            except Exception as e: # TODO: what exception should this actually be?
                break
        
        # Pass message to anyone who isn't the sender
        for id in sender_queues:
            if id == message.sender:
                # Don't send it back to yourself
                continue
            sender_queues[id].put(message, block=True)


### Listener

def receive_message(conn: socket.socket, addr: tuple[str, int], buffer_size: int = 4096):
    """Receive a message from the pipe in `buffer_size` byte chunks."""
    message = b""
    while True:
        bytes_received = conn.recv(4096)
        message += bytes_received
        if len(bytes_received) < buffer_size:
            # Got everything there is to offer
            break
    message = message.decode("utf-8")
    return message


def receiver(id, mp_socket: MP_socket, socket_lock: mp.Lock, queue: mp.Queue):
    mp_socket.set_timeout(60)
    mp_socket.register_lock(socket_lock)
    while True:
        try:
            message = mp_socket.receive_message()
        except socket.timeout as e:
            info("RECEIVER", f"timed out waiting for a message from {id}, exiting...")
            mp_socket.close()
            break

        # debug("RECEIVER", f"{id} - {message}")
        if message == b"":
            continue
            # Close
            info("RECEIVER", f"ending connection with client-{id} - {mp_socket.addr}")
            mp_socket.close()
            break
        elif message == "HB":
            # Hearbeat message, don't disconnect but don't do anything
            info("RECEIVER", f"got heartbeat from {id}")
        else:
            parsed = parse_ws_message(message)
            m = Message(id, id, message)
            queue.put(m, block=True)
            debug("RECEIVER", f"got '{message}' from client-{id}")


def parse_ws_message(data: bytes):
    ddata = base64.b64decode(data)
    print(ddata)
    frame = bytearray(ddata)
    print(frame)


### Sender

def send_message(conn: socket.socket, addr: tuple[str, int], message: str):
    """Passes a message down a connection in utf-8 encoding."""
    conn.sendall(message.encode("utf-8"))
    debug("SEND", f"Sent to {get_address_string(addr)}:\n{message}")


def sender(id, mp_socket: MP_socket, socket_lock: mp.Lock, queue: mp.Queue):
    """Reads messages from a sending queue and passes them to the client."""
    connected = True
    mp_socket.set_timeout(60)
    mp_socket.register_lock(socket_lock)
    mp_socket.send_message("HELLO")
    while True:
        if connected:
            message: Message = queue.get(block=True)
            try:
                # send_message(conn, addr, message)
                mp_socket.send_message(message.content)
            except socket.error as e:
                warn("SENDER", f"connection to client-{id} is dead,\n{e}\nreconnecting...")
                connected = False
                continue
            except Exception as e:
                error("SENDER", f"error with client-{id}\n{e}")

            debug("SENDER", f"sent '{message}' to {id}")
        else:
            # not connected
            info("SENDER", f"attempting reconnect with client-{id}")
            try:
                mp_socket.connect()
                connected = True
                success("SENDER", f"reconnected with client-{id}")
            except socket.timeout as e:
                warn("SENDER", f"timed out waiting for client-{id}, exiting...")
                break


### Heartbeat

def heartbeat(id, mp_socket: MP_socket, socket_lock: mp.Lock):
    """Heartbeat sender for clients."""
    connected = True
    error_count = 0
    message = "HB"
    mp_socket.register_lock(socket_lock)
    while True:
        if connected:
            try:
                # send_message(conn, addr, message)
                mp_socket.send_message(message)
            except Exception as e:
                error("HEARTBEAT", f"client-{id} got error: {e}")
                error_count += 1
                if error_count > 10:
                    error("HEARTBEAT", f"too many errors for {id} detected, disconnecting...")
                    connected = False
                    break
            time.sleep(20)
        else:
            break
    
    info("HEARTBEAT", f"for client-{id} exiting")


### LOGGER

def init_logger():
    """Set up the logging"""
    log.basicConfig(
        filename=LOG_FILENAME,
        level=log.DEBUG,
        format="%(levelname)s : %(relativeCreated).0f : %(message)s"
        # datefmt="%Y-%m-%d_%H-%M-%S"
    )
    log.info(f"Starting at {START_TIMESTAMP}")


### UTILS

def sys_out(text: str, nl: bool=True):
    """Standard printing function to be used."""
    typer.echo(text, nl=nl)


def construct_string(*text: any):
    """Constructs a string from an arbitrary amount of arguments."""
    res = ""
    for i, item in enumerate(text):
        if i > 0:
            res = res + " "
        res = res + str(item)
    return res


def style(*text: any, color: str = None, background_colour: str = None):
    """Returns a concatenated styled string from a list of inputs."""
    return typer.style(construct_string(*text), fg=color, bg=background_colour)


def debug(name: str, *text: any):
    """Debug info printing function, colour codes the `name` as magenta."""
    output = style(name, color=typer.colors.MAGENTA) + ": " + construct_string(*text)
    if DEBUG_LEVEL >= log.DEBUG:
        sys_out(output)
    if ENABLE_LOGGING:
        log.debug(output)


def info(name: str, *text: any):
    """Standard info printing function, colour codes the `name` as blue."""
    output = style(name, color="blue") + ": " + construct_string(*text)
    if DEBUG_LEVEL >= log.INFO:
        sys_out(output)
    if ENABLE_LOGGING:
        log.info(output)


def success(name: str, *text: any):
    """Standard success printing function, colour codes the `name` as green."""
    output = style(name, color="green") + ": " + construct_string(*text)
    if DEBUG_LEVEL >= log.WARN: # Use warning level here
        sys_out(output)
    if ENABLE_LOGGING:
        log.warn(output)


def warn(name: str, *text: any):
    """Standard warning printing function, colour codes the `name` as yellow."""
    output = style(name, color="yellow") + ": " + construct_string(*text)
    if DEBUG_LEVEL >= log.WARN:
        sys_out(output)
    if ENABLE_LOGGING:
        log.warn(output)


def error(name: str, *text: any):
    """Standard error printing function, colour codes the `name` as red."""
    output = style(name, color="red") + ": " + construct_string(*text)
    if DEBUG_LEVEL >= log.ERROR:
        sys_out(output)
    if ENABLE_LOGGING:
        log.error(output)


### STARTUP

if __name__ == "__main__":
    typer.run(main)