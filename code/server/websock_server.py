import asyncio
import time
from aioconsole import ainput
import datetime as dt
import rules_engine as RE
import typer
import websockets

from async_dict import AsyncDict, Async_Reversible_Dict
from config import CONNECTION_STATUS, DELAY_PIPE, HEARTBEAT_DELAY, ID_TO_NAME, LOG, RECEIVE_PIPE, ROUTES, SERVER_NAME, SENDER_PIPES, START_TIMESTAMP, configure_logger
from logger import Logger, Log_Level
from message import ForwardRule, InputControl, Message, CTMessage, CTType, ServerControl, ServerControlResponse, ServerControlType
from utils import *

# SENDING

async def send_CTMessage(websocket, message: CTMessage):
    """Attempts to send a CTMessage to a websocket. If the connection fails then it updates the connection status."""
    await send_message(websocket, message._raw_message)


async def send_message(websocket, message: Message):
    """Attempts to send a message to a websocket. If the connection fails then it updates the connection status."""
    try:
        await websocket.send(message.content)
        LOG.debug("SEND", f"sent '{message.content}' to {websocket.id}")
    except websockets.exceptions.ConnectionClosed:
        LOG.error("SEND", f"got connection closed trying to send to {websocket.id}")
        await CONNECTION_STATUS.put(websocket.id, "disconnected")


async def sender(websocket):
    """Waits on the corresponding SENDER_PIPE for the given connection and then sends it on to the intended recipient."""
    connected = await CONNECTION_STATUS.get(websocket.id) == "connected"
    while connected:
        pipe = await SENDER_PIPES.get(websocket.id)
        message: Message = await pipe.get()
        await send_message(websocket, message)
        connected = await CONNECTION_STATUS.get(websocket.id) == "connected" 


async def heartbeat(websocket):
    """Heartbeat sender for clients."""
    connected = await CONNECTION_STATUS.get(websocket.id) == "connected"
    message: CTMessage = CTMessage()
    message.create_message(SERVER_NAME, CTType.HEARTBEAT)
    while connected:
        await send_CTMessage(websocket, message)
        await asyncio.sleep(HEARTBEAT_DELAY)
        connected = await CONNECTION_STATUS.get(websocket.id) == "connected" 


# RECEIVING

async def receive_message(websocket) -> Message: 
    """Receive a raw message from the websocket and wrap it in a Message class."""
    message: Message = None
    try:
        raw_message = await websocket.recv()
        LOG.debug("RECEIVE", f"got '{raw_message}' from {websocket.id}")
        message = Message(websocket.id, None, raw_message)
    except websockets.exceptions.ConnectionClosed:
        LOG.error("RECEIVE", f"got connection closed trying to receive from {websocket.id}")
        await CONNECTION_STATUS.put(websocket.id, "disconnected")
    return message


async def receiver(websocket):
    """Waits on the receive call for the websocket and then queues the messages in the RECEIVE_PIPE."""
    connected = await CONNECTION_STATUS.get(websocket.id) == "connected" 
    while connected:
        message: Message = await receive_message(websocket)
        await RECEIVE_PIPE.put(message)
        connected = await CONNECTION_STATUS.get(websocket.id) == "connected" 
    LOG.info("RECEIVER", f"exiting for {websocket.id}")
    # Delete their entry
    await ID_TO_NAME.delete(websocket.id)


# New Connections

async def handle_new_connection(websocket):
    """Handles any new connections by initializing variables and starting a sender, receiver and heartbeat task for the new connection."""
    LOG.info("NEW_CONNECTION", f"received new connection from {websocket.remote_address}")
    await SENDER_PIPES.put(websocket.id, asyncio.Queue(0))
    await CONNECTION_STATUS.put(websocket.id, "connected")
    await ID_TO_NAME.put(websocket.id, f"{websocket.id}_TEMP")
    await asyncio.gather(sender(websocket), receiver(websocket), heartbeat(websocket))
    await SENDER_PIPES.delete(websocket.id)
    await CONNECTION_STATUS.delete(websocket.id)
    LOG.info("NEW_CONNECTION", f"exiting connection from {websocket.remote_address}")


async def welcomer(address: str, port: int):
    """Listens for new connections on the main port and spawns a receiver, heartbeat and sender for each."""
    # MAIN WELCOME LOOP
    while True:
        try:
            # Main listen loop
            async with websockets.serve(handle_new_connection, host=address, port=port) as s:
                LOG.info("WELCOMER", f"listening for new connections on {address}:{port}")
                await s.wait_closed()
        except Exception as e:
            print("-----------------")
            print(e)
            print("-----------------")
        except GeneratorExit as ge:
            print("-----------------")
            print("gen", ge)
            print("-----------------")
            break


# Main / Backend logic

async def backend(address, port, local_command_interface: bool = False):
    """Takes messages from the receive queue and gets them to where they need to go."""
    LOG.info("SERVER", f"starting up at {START_TIMESTAMP}")
    # Init the async dicts in the correct loop
    # global SENDER_PIPES, CONNECTION_STATUS, ID_TO_NAME, ROUTES
    SENDER_PIPES.initialize()
    CONNECTION_STATUS.initialize()
    ID_TO_NAME.initialize()
    # Setup forwarding
    ROUTES.initialize()
    DELAY_PIPE.initialize()

    if local_command_interface:
        # Start the command interface
        asyncio.get_event_loop().create_task(RE.command_interface())

    # Start a welcomer task to listen for new connections
    asyncio.get_event_loop().create_task(welcomer(address, port))
    asyncio.get_event_loop().create_task(delayer())

    # Start a parser loop for new messages
    while True:
        # Get the message from the receive queue
        raw_message: Message = await RECEIVE_PIPE.get()
        if raw_message is None:
            LOG.warn("BACKEND", f"got a blank message")
            continue
        else:
            try:
                message: CTMessage = CTMessage(raw_message)
            except TypeError as e:
                LOG.warn("BACKEND", f"Unable to convert to CTMessage - {raw_message.content}\n{e}")
                continue
            try:
                if message.get_message_type() == CTType.NEW_CONNECTION:
                    LOG.success("BACKEND", f"registering new link of {raw_message.sender} <-> {message.get_sender()}")
                    await ID_TO_NAME.put(raw_message.sender, message.get_sender())
                    # TODO: GENERATE NEW_PLAYER message here
                elif message.get_message_type() == CTType.END_CONNECTION:
                    # TODO: Logic for nice shutdown here
                    # TODO: Generate REMOVE_PLAYER message here
                    pass
                elif message.get_message_type() == CTType.HEARTBEAT:
                    # TODO: Any heartbeat logic here?
                    LOG.info("BACKEND", f"got heartbeat from {message.get_sender()}")
                    pass
                elif message.get_message_type() == CTType.INPUT_CONTROL:
                    LOG.debug("BACKEND", "Got an InputControl message")
                    # TODO: Pass input control message along
                    route_keys = await ROUTES.keys()
                    if message.get_sender() in route_keys:
                        # Possibility of forwarding this message
                        # Transform the message
                        control_data = InputControl.parse_message(message)
                        forward_rules: list[ForwardRule] = await ROUTES.get(message.get_sender())
                        for rule in forward_rules:
                            dest_id = await ID_TO_NAME.get_reverse(rule.dest_name)
                            if dest_id is not None:
                                if control_data.matches_rule(rule):
                                    transformed_control_data = control_data.get_transformation(rule)
                                    out_content = transformed_control_data.construct_message()
                                    out_message = CTMessage()
                                    out_message.create_message(SERVER_NAME, CTType.INPUT_CONTROL, out_content)
                                    if rule.message_delay > 0:
                                        # Message needs to be delayed
                                        out_time = time.time() + rule.message_delay
                                        await DELAY_PIPE.add([out_time, dest_id, out_message])
                                    else:
                                        async with SENDER_PIPES.get_lock():
                                            try:
                                                await SENDER_PIPES.get_dict()[dest_id].put(out_message.get_raw_message())
                                            except Exception as e:
                                                LOG.error("BACKEND", f"Unable to forward message to {dest_id}: {e}")
                                                pass
                elif message.get_message_type() == CTType.SERVER_CONTROL:
                    LOG.debug("BACKEND", f"Got a ServerControl message: {message.message_content}")
                    control_message: ServerControl = ServerControl.parse_message(message)
                    if control_message is None:
                        LOG.warn("BACKEND", f"Got a ServerControl message but was unable to parse: {message.get_message_content()}")
                        continue
                    # Have a valid control message now
                    result: ServerControlResponse = await RE.handle_command(control_message)
                    if result is not None:
                        dest_name = control_message.sender
                        dest_id = await ID_TO_NAME.get_reverse(dest_name)
                        response_test = result.to_message_string()
                        LOG.debug("BACKEND", f"Sending response to {dest_name}\n  - '{response_test}'")
                        out_message = CTMessage()
                        out_message.create_message(SERVER_NAME, CTType.SERVER_CONTROL_RESPONSE, response_test)
                        async with SENDER_PIPES.get_lock():
                            try:
                                await SENDER_PIPES.get_dict()[dest_id].put(out_message.get_raw_message())
                            except Exception as e:
                                LOG.error("BACKEND", f"Unable to forward message to {dest_name}: {e}")
                                pass
                elif message.get_message_type() == CTType.GLOBAL_MESSAGE:
                    # TODO: Some kinda logic for sending a chat message here
                    pass
                elif message.get_message_type() == CTType.PRIVATE_MESSAGE:
                    # TODO: Some kinda logic for sending a private chat message here
                    pass
                else:
                    # TODO: Unknown CTMessage received, do what?
                    LOG.warn("BACKEND", f"got a CTMessage of unknown type - {message.get_message_type()} - {raw_message.content}")
                    pass
                
            except TypeError as e:
                LOG.warn("BACKEND", f"Got a type error handling message - {raw_message.content}\n{e}")


async def delayer():
    """Forwards messages once the delay has expired"""
    while True:
        try:
            await asyncio.sleep(0.01) # Sleep 10ms
            messages = await DELAY_PIPE.sort(key=lambda x: x[0])
            if len(messages) > 0 and messages[0][0] < time.time():
                while True:
                    delay_tuple = await DELAY_PIPE.peak(0)
                    if delay_tuple is None:
                        break
                    if delay_tuple[0] < time.time():
                        # Remove item from delay pipe, time to send
                        await DELAY_PIPE.pop(0)
                        dest_id: str = delay_tuple[1]
                        out_message: CTMessage = delay_tuple[2]
                        async with SENDER_PIPES.get_lock():
                            try:
                                await SENDER_PIPES.get_dict()[dest_id].put(out_message.get_raw_message())
                            except Exception as e:
                                LOG.error("DELAYER", f"Unable to forward message to {dest_id}: {e}")
                    else:
                        # No more messages to forward yet
                        break

        except Exception as e:
            print("-----------------")
            print(e)
            print("-----------------")
        except GeneratorExit as ge:
            print("-----------------")
            print("gen", ge)
            print("-----------------")
            break


def main(
    address: str = typer.Option("127.0.0.1", help="Address for the proxy to listen to for incoming connections."),
    port: int = typer.Option(4001, help="Port for the proxy to listen to for incoming connections."),
    enable_logging: bool = typer.Option(True, help="Enable / Disable logging to file"),
    enable_print_logging: bool = typer.Option(False, "--print-logs/--no-print-logs", "-pl/-npl", help="Enable / Disable logging to the command line"),
    log_level: Log_Level = typer.Option("DEBUG", "--log-level", "-ll", help="Configure the level of output info in the logs."),
    log_path: str = typer.Option("logs/server-logs_{{}}.log", "--log-path", "-lp", help="File path for log files to be stored at. {{}} is replaced with the program start time.")
):
    # Enable logger
    log_path = log_path.replace("{{}}", START_TIMESTAMP)
    configure_logger(enable_logging, enable_print_logging, log_level, log_path)

    # Run main program
    try:
        LOG.info("MAIN", f"server starting on {address}:{port}")
        asyncio.run(backend(address, port))
    except KeyboardInterrupt:
        LOG.info("MAIN", "server exiting due to keyboard interrupt")


if __name__ == "__main__":
    typer.run(main)
