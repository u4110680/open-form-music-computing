#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

cd /home/wisp/open-form-music-computing/

# Check to see if we actually need to pull
git fetch
BRANCH=$(git rev-parse --abbrev-ref HEAD)
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse origin/${BRANCH})
BASE=$(git merge-base @ origin/${BRANCH})

echo $LOCAL
echo $REMOTE
echo $BASE

if [ $LOCAL = $REMOTE ]; then
    echo "Up-to-date"
elif [ $LOCAL = $BASE ]; then
    # Only do the thing if we actually have an update
    echo "Need to pull"
    git pull "https://gitlab.anu.edu.au/u4110680/open-form-music-computing.git" ${BRANCH}
    cd code/interface
    cd model-d
    make
    cd ../control
    make
    cd ..
    rsync -a -u model-d/ /drives/green-240gb/nginx-reverse/data/site/app/
    rsync -a -u control/ /drives/green-240gb/nginx-reverse/data/site/control/
    git reset --hard
    echo "Done"
elif [ $REMOTE = $BASE ]; then
    echo "Need to push"
else
    echo "Diverged"
fi